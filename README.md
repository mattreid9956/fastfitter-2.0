# *Fast Fitter*
********************************************************************************

* **Project**: Multipurpose fitting analysis framework.
* **Package**: FastFitter
* **File**: $Id: README,md,v 1.25 2014/01/04 13:29:20 mreid Exp $
* **Authors**:  Thomas Latham (T.Latham@warwick.ac.uk), Edmund Smith (University of Oxford) and Matthew M Reid (Matthew.Reid@warwick.ac.uk).
* **Description**: This file contains package information and usage instructions
* **Contributors**:, Daniel O'Hanlon (D.O-Hanlon@warwick.ac.uk), Rafael Silva Coutinho (R.Silva-Coutinho@warwick.ac.uk)   


********************************************************************************

## What is fastfitter v2.0?

* **FastFitter**: Allows the user to fit data models using simultaneous extended maximum likelihood techniques. Ultimately allowing one to fit standalone distributions or simultaneous pdfs which share parameters across different distributions.
The framework allows complex models to be fit behind a simple interface for increased efficiency.
Comes packaged with many model pdfs as standard. Users can create composite models
from those included or add their own. Constraints on parameters can be created in two ways: as a function of other parameters and/or through the use of an external Gaussian constraint. Formatted plots are produced with the ability to blind parameters.
Users can also build profile likelihoods and use these to combine measurements.

********************************************************************************

********************************************************************************

## What does this package depend on?

********************************************************************************

1. cmake -- see http://www.cmake.org/ for more information.
2. CERN's ROOT Analysis Framework -- In fact this packages is basically a 
front-end for performing some of functions that RooFit allows.
3. If you want to use all ROOT features including fast fourier transforms, gsl, make sure you have all dependencies installed first.


Ideologies:
  * Models should be easy configurable and simple enough to add or import from another RooWorkspace.
  * After every operation the fitter should be returned to a neutral state, ready for its next use. As in, if I perform a toy setting some specific parameters constant, I should make them float afterward thus restoring order.


TODO: 
  One problem with RooWorkspace is that once something is imported it remains there.
  It would be nice to be able to override a pdf with a different model but keep the same
  name in a convenient way so that you could for instance change the shape for
  a systematic cross check i.e. DCB <=> DG. This is of course possible in the framework,
  by writing out the original model and importing all but the signal pdf.

  Various add-ons for ToyStudies class mainly from CharmlessFitter
   1. Add RooStats::ModelConfig interface for more involved likelihood and limit calculations.
   2. Add the FitterLikesRatioPlot stuff as this was a neat feature of CharmlessFitter.
   3. ToyStudy::localMinimumChecker it would be neat to have some more detailed
   output here comparing any new minima found with the default fit.
   4. add a method to set a string search such that "*Yield*=x" parameters to some value 'x' in the ToyStudy class.


********************************************************************************

## How do I build the package?

********************************************************************************

You can access this repo with SSH or HTTPS;
Only tested on Linux, please do report any issues. 

Either you have got the source or got it from the repo, you must then

```
git clone https://mattreid9956@bitbucket.org/mattreid9956/fastfitter-2.0.git
cd fastfitter-2.0
```

We include a bash script, build.sh that essentially
makes a directory called build in the top directory
and puts all the libraries and example executables in there.

./build.sh 

you can run the examples locally or you can modify the default install path,
/usr/local/fitter to what ever you like by changing the path in build.sh. After
that you should simply be able install it by running

make install

from within the build directory (this is where cmake will put the Makefile). 
Remember to set up your LD_LIBRARY_PATH or create a ".conf" file
in your /etc/ld.so.conf.d/ area that links to the install location. Also remember to
add the install location

THATS IT!! You can test that everything is working ok by running one of the examples.
If you are using another shell environment such as .csh, 
view the build.sh script and adapt it as you require. 


********************************************************************************

## Can I run the FastFitter via python?

********************************************************************************

Simply put yes! It is not well tested at the moment but uses ROOT's PyCintex 
to produce python dictionaries of all the classes in the package. There is also an 
installer for creating a python module of the fastfitter class. This is in the module 
directory and the user can find information on how to install this there. It should 
be as simple as

python setup.py build
python setup.py install


********************************************************************************

## How do I use FastFitter in an interactive ROOT session?

********************************************************************************

1. Edit your RooLogon.C to load the libraries

```
gSystem->Load("xxx/libFitter.so");
using namespace RooFit;
```

2. Start ROOT from workdir.

```
root -l
```
You should see something like this banner printed out:
```
RooFit v3.56 -- Developed by Wouter Verkerke and David Kirkby 
                Copyright (C) 2000-2013 NIKHEF, University of California & Stanford University
                All rights reserved, please read http://roofit.sourceforge.net/license.txt
```

3. Run your macros.


********************************************************************************

## Where are the examples?

********************************************************************************

There are line-by-line commented example scripts under the examples directory.
These are compiled programs, not ROOT macros.
They exist in the build/examples directory
They will be compiled when you run the ./build.sh script:

They can be executed locally from the local build of the fastfitter package

./build/examples/simple-model/fitter-simple-model

OR if you have the PATH envirnment setup you can run

fitter-simple-model

directly from the command line.

Examples currently test and in a working state are:
    * fitter-simple-model - Fits two data samples containing the same invariant mass spectra, but with one
    reconstrtucted under a worse resolution. Simultaneous fit is performed to extract the signal and 
    background shapes.
        *Other features* - chi2 and prob of fit, plotting, checking for local minima, pull study performed.
    * fitter-model -
    * fitter-analysis - reading in of an external RooWorkspace and how you can set the environment.
    * fitter-likelihood - combines two likelihood profiles and merges them with systematic convolution.


WARNINGS: - Some of the example programs take a long time to execute - you may wish to edit them e.g. by shortening a loop.
          - The examples should pick up the data from where they were installed automatically. Report if this is not
          the case.

********************************************************************************

## Credits

********************************************************************************

Thanks to Nicole for many of the PDF classes, the expansion of the pull 
plotting capabilities and for the asymmetry implementation.
Thanks to Jim for testing things on the fly and for the likelihood ratio
plot stuff.