import os
# Locate this module on the filesystem
__packagename_path = os.path.abspath( os.path.dirname( __file__ ) )
__packagename_so = os.path.join( __packagename_path, "/usr/local/fastfitter/lib/libFitter.so" )


from ctypes import *
cdll.LoadLibrary( "/usr/local/fitter/lib/x86_64-linux-gnu/libFitter.so" )

# Always need to import the shared object and extract useful namespace
import PyCintex
PyCintex.Cintex.Enable()
PyCintex.loadDictionary( "/usr/local/fastfitter/lib/libFitter.so" )

FitterBase = PyCintex.gbl.Fast.FitterBase # useful namespace
Fitter = PyCintex.gbl.Fast.Fitter # useful namespace
SimultaneousFitter = PyCintex.gbl.Fast.SimultaneousFitter # useful namespace
ModelBase = PyCintex.gbl.Fast.ModelBase # useful namespace
FitterPulls = PyCintex.gbl.Fast.FitterPulls # useful namespace
ClientTree = PyCintex.gbl.Fast.ClientTree # useful namespace
FitterLikesRatioPlot = PyCintex.gbl.Fast.FitterLikesRatioPlot # useful namespace
LHCbStyle = PyCintex.gbl.Fast.LHCbStyle
ToyStudy = PyCintex.gbl.Fast.ToyStudy
string_tools = PyCintex.gbl.string_tools
ConvolutionBase = PyCintex.gbl.Fast.ConvolutionBase
Exceptions = PyCintex.gbl.Fast.Exceptions
Fitter1DLikeProfile = PyCintex.gbl.Fast.Fitter1DLikeProfile
Fitter1DLikeProfileFFTConv = PyCintex.gbl.Fast.Fitter1DLikeProfileFFTConv
FitterUpperLimits = PyCintex.gbl.Fast.FitterUpperLimits


# RooFit pdfs
RooBifurCB = PyCintex.gdb.RooBifurCB
RooDataPdf = PyCintex.gdb.RooDataPdf
RooGeneralisedHyperbolic = PyCintex.gdb.RooGeneralisedHyperbolic
RooHypatia2 = PyCintex.gdb.RooHypatia2
RooHypatia = PyCintex.gdb.RooHypatia
RooAmorosoPdf = PyCintex.gdb.RooAmorosoPdf
RooApollonios = PyCintex.gdb.RooApollonios
RooCruijff = PyCintex.gdb.RooCruijff
RooCruijffSimple = PyCintex.gdb.RooCruijffSimple

