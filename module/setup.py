from setuptools import setup

readme = open('README.txt').read()

setup(name='FastFitter',
      version='1.0',
      author='Matthew Reid',
      author_email='Matthew.Reid@Warwick.ac.uk',
      license='MIT',
      package_dir={'': 'FastFitter'},
      description='Fast Analysis Simultaneous Toys',
      long_description=readme,
      py_modules=['fastfitter']
     )
