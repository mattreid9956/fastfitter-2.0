
// -- CLASS DESCRIPTION [PDF] --
// A Crystal Ball with different widths on each side.


#include "RooFit.h"
#include <iostream>
#include <cmath>

// Local
#include "RooBifurCB.hpp"

// RooFit
#include "RooAbsReal.h"
#include "RooRealVar.h"

ClassImp(RooBifurCB);

static const char rcsid[] =
"$Id: RooBifurCB.cc,v 1.3 2005/06/27 18:57:21 denardo Exp $";

RooBifurCB::RooBifurCB(const char *name, const char *title,
        RooAbsReal& _m, RooAbsReal& _m0, 
        RooAbsReal& _sigmaL, RooAbsReal& _sigmaR,
        RooAbsReal& _alpha, RooAbsReal& _n) :
    RooAbsPdf(name, title),
    m("m", "Dependent", this, _m),
    m0("m0", "M0", this, _m0),
    sigmaL("sigmaL", "SigmaL", this, _sigmaL),
    sigmaR("sigmaR", "SigmaR", this, _sigmaR),
    alpha("alpha", "Alpha", this, _alpha),
    n("n", "Order", this, _n)
{}

RooBifurCB::RooBifurCB(const RooBifurCB& other, const char* name) :
    RooAbsPdf(other, name), m("m", this, other.m), m0("m0", this, other.m0),
    sigmaL("sigmaL", this, other.sigmaL),  sigmaR("sigmaR", this, other.sigmaR), 
    alpha("alpha", this, other.alpha),  n("n", this, other.n)
{}

Double_t RooBifurCB::evaluate() const {

    Double_t t = (m-m0);
    if( t > 0 ) {
        t = t / sigmaR;
    } else {
        t = t / sigmaL;
    }

    if ( alpha < 0 ) {
        t = -t;
    }

    Double_t absAlpha = std::fabs(alpha);

    if ( t > -absAlpha ) {
        return std::exp( -0.5 * t * t );
    }
    else {
        Double_t a =  std::pow( n / absAlpha, n ) *
            std::exp( -0.5 * absAlpha * absAlpha );

        Double_t b= n / absAlpha - absAlpha; 
        return a / std::pow( b - t, n );
    }
}

