#ifndef ROO_BIFURCB
#define ROO_BIFURCB

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

class RooBifurCB : public RooAbsPdf {
    public:
        RooBifurCB(const char *name, const char *title, RooAbsReal& _m,
                RooAbsReal& _m0, RooAbsReal& _sigmaL,  RooAbsReal& _sigmaR,
                RooAbsReal& _alpha, RooAbsReal& _n);

        RooBifurCB(const RooBifurCB& other, const char* name = 0);
        virtual TObject* clone(const char* newname) const { return new RooBifurCB(*this,newname); }

        inline virtual ~RooBifurCB() { }

    protected:

        RooRealProxy m;
        RooRealProxy m0;
        RooRealProxy sigmaL;
        RooRealProxy sigmaR;
        RooRealProxy alpha;
        RooRealProxy n;

        Double_t evaluate() const;

    private:

        ClassDef(RooBifurCB,0) // Bifurcated Crystal Ball lineshape PDF
};

#endif
