#ifndef FAST_TOYSTUDY_H
#define FAST_TOYSTUDY_H 1


// STL include
#include <iosfwd>
#include <algorithm>
#include <functional>

// ROOT
#include "TString.h"


// RooFit
#include "RooFitResult.h"

// local
#include "string_tools.hpp"
#include "Fitter.hpp"
#include "FitterPulls.hpp"


/** @class ToyStudy Fitter/ToyStudy.hpp
 *  
 *
 *  @author Matthew M Reid
 *  @date   2014-01-08
 */

/* Class to allow easy setup of toy studies given a few input parameters.
 *
 */

// This should load in the fitResult, modify it by changing yields/params then
// save a new snapshot. This should then be reloaded each time the MCstudy loops,
// wiggle parameters. Then the pulls calculated.

namespace Fast {

    class ToyStudy {

        public:
            
            ClassDef(ToyStudy,1);
            explicit ToyStudy( Fitter* fitter, Int_t nSamples = 500,
                    Int_t mSystematics = 500, Int_t seed = 0 );

            virtual ~ToyStudy();

            //=============================================================================
            // Does a MC study on the pdf. 
            // Sets seed for random generator (to enable many small independent toy studies
            // to be done in parallel).
            // Allows individual yield parameters to be set explicitly
            //=============================================================================
            virtual void runToyStudy( const char* fitName = "fitResults", 
                    std::string setyields = "", 
                    Fitter::RandomMethod randomiser = Fitter::GAUSSIAN );
          
            
            //virtual void runSystematics( const char* fitName = "fitResults", const char* varyFixedParams = "" );

            void fillVectors();
            void fillVectors( const RooArgSet* genParams );
            void fillVectors_SystII(RooFitResult* res_official = 0,  RooFitResult* res_toyMC = 0);
            void dataMinNll();

            //=============================================================================
            // Perform Systematics studies due to fixing MC parameters
            //=============================================================================
            virtual void runSystI(  const char* fitName = "fitResults", 
                    Fitter* fitterMC = 0, std::string setparameters = "", 
                    Fitter::RandomMethod randomiser = Fitter::CORRELATION );

            //=============================================================================
            // Perform Systematics studies due to the choice of the parametrisation
            //=============================================================================
            virtual void runSystII(  const char* fitName = "fitResults", 
                    Fitter* fitter_official = 0, 
                    Fitter::RandomMethod randomiser = Fitter::CORRELATION );


            //virtual void runToyStudyAndSystematics(, Int_t seed = 0, 
            //        const char* varyFixedParams = "" );
            //=============================================================================
            // MIGRAD is able to find the minimum of your parameter space, however it can 
            // be subject to localised minima. A cross-check for this is to change the 
            // initial values of the fit and perform the fit again. If the true global 
            // minimum has been found then the function returns true.
            //=============================================================================
            virtual bool localMinimumChecker( const char* fitName = "fitResults", Int_t n = 100,
                    Fitter::RandomMethod method = Fitter::GAUSSIAN,
                    Double_t edmTol = 1.e-3, Bool_t extendedMode = kTRUE, Bool_t useMinos = kTRUE, Bool_t useSumW2Errors = kFALSE, 
                    Bool_t printResults = kFALSE );


            //=============================================================================
            virtual void plotValues(TString fileName) const;
            //=============================================================================
            virtual void plotPulls(TString fileName) const;
            //=============================================================================
            virtual void plotSystI(TString fileName, std::string paramName) const;
            //=============================================================================
            virtual void plotSystII(TString fileName) const;
            //=============================================================================
            virtual void writePulls(TString fileName) const;
            //=============================================================================
            virtual void plotNlls(TString fileName) const;


            // getters and setters
            virtual void setNSamples( Int_t nSamples ) { m_nSamples = nSamples; }
            virtual void setMSystematics( Int_t mSystematics ) { m_mSystematics = mSystematics; }


            virtual Int_t getSeed() const { return m_seed; }

        protected:

        private:


            void setSeed( Int_t seed ) ;

            //=============================================================================
            // set parameters to the values listed in args, used for toys
            //=============================================================================
            void setToyInitialValues( const std::vector<std::string>& args,
                    RooArgSet& params );

            //=============================================================================
            // Function call to randomise all non-constant parameters within their ranges.
            // If the range value is true, the initial value is chosen by sampling a uniform
            // distribution over the allowed range of the variable, if it is false then 
            // the initial value is sampled from a Gaussian based on G( val, err ).
            //=============================================================================
            //void randomiseFitParams( Fitter::RandomMethod range = ALL );

            //=============================================================================
            // set systematic parameters to the values listed in args, used for systematic
            // studies such as varying a parameter that was fixed due to MC.
            //=============================================================================
            //void setSystematicInitialValues( std::vector<std::string> args );

            string_tools m_stringHelper;
            Fitter* m_fitter;
            Int_t m_nSamples;
            Int_t m_mSystematics;
            RooFitResult* m_dataResult;
            RooFitResult* m_result;
            Int_t m_seed;
            const char* m_datafitResultName;
            const char* m_toyfitParamsName;
            const char* m_systematicParamsName;

            FitterPulls m_pulls;

            RooArgSet m_wiggleCorrelation;
    };

    class CompareFitResultFunctor : public std::binary_function<RooFitResult*, RooFitResult*, bool> {
        public:
            bool operator()( RooFitResult* lhs, RooFitResult* rhs)
            {
                return ( lhs->minNll() < rhs->minNll() );
            }
    };

}

#endif // FAST_TOYSTUDY_H
