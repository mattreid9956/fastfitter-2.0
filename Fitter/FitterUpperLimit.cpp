// $Id: $
// Include files
// std libs
#include <iostream>
#include <cmath>
#include <ctime>
#include <sstream>
#include <limits>
#include <algorithm>    // std::find
#include <iomanip>      // std::setprecision

// ROOT
#include "TStyle.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLine.h"
#include "TROOT.h"
#include "TMath.h"
#include "TSystemFile.h"

// RooFit
#include "RooPlot.h"
#include "RooAbsPdf.h"
#include "RooArgSet.h"
#include "RooCmdArg.h"

// RooStats

// local
#include "FitterUpperLimit.hpp"
#include "Exceptions.hpp"
#include "string_tools.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : FitterUpperLimit
//
// 2014-01-08 : Matthew M Reid
//-----------------------------------------------------------------------------


using namespace Fast;
using Fast::GeneralException;


//-----------------------------------------------------------------------------
// Standard constructor, initializes variables
//-----------------------------------------------------------------------------
FitterUpperLimit::FitterUpperLimit(  ) 
    : m_profile(),
    m_maxIterations( 1E+7 ), // 10 million
    m_confidenceLevel(0.95), 
    m_lowerBound( 0.0 ),
    m_upperLimits(),
    m_fillColour(),
    m_fillStyle()
{}


//-----------------------------------------------------------------------------
// overloaded constructor
//-----------------------------------------------------------------------------
FitterUpperLimit::FitterUpperLimit(  const Fitter1DLikeProfile& profile, 
                    Double_t cl )
    : m_profile( profile ), 
    m_maxIterations( 1E+7 ), // 10 million
    m_confidenceLevel( cl ),
    m_lowerBound( 0.0 ),
    m_upperLimits(),
    m_fillColour(),
    m_fillStyle()
{
    if( !this->isCLValueOK( m_confidenceLevel ) ) {
        std::stringstream msg;
        msg << "Confidence level value("<< m_confidenceLevel << ") is not defined in range [0,1].";
        throw GeneralException("FitterUpperLimit::FitterUpperLimit",
                msg.str() );        
    }

    // we need afterall a likelihood to set an upper limit.
    if( !m_profile.isLikelihood() ) {
        m_profile.convertToLikelihood();
    }
}


//-----------------------------------------------------------------------------
// set the confidence level (private member)
//-----------------------------------------------------------------------------
void FitterUpperLimit::setConfidenceLevel( Double_t cl )
{
    if( !this->isCLValueOK( cl ) ) {
        std::stringstream msg;
        msg << "Confidence level value("<< cl << ") is not defined in range [0,1].";
        throw GeneralException("FitterUpperLimit::setConfidenceLevel",
                msg.str() );        
    }
    m_confidenceLevel = cl;
}


//-----------------------------------------------------------------------------
// Set the fill style for specified confidence level
//-----------------------------------------------------------------------------
void FitterUpperLimit::setFillStyle( Double_t cl, Style_t style )
{
    if( !this->isCLValueOK( cl ) ) {
        std::stringstream msg;
        msg << "Confidence level value("<< cl << ") is not defined in range [0,1].";
        throw GeneralException("FitterUpperLimit::setFillStyle",
                msg.str() );        
    }
    m_fillStyle[cl] = style;
}


//-----------------------------------------------------------------------------
// Set the fill colour for specified confidence level
//-----------------------------------------------------------------------------
void FitterUpperLimit::setFillColour( Double_t cl, Color_t colour )
{
    if( !this->isCLValueOK( cl ) ) {
        std::stringstream msg;
        msg << "Confidence level value("<< cl << ") is not defined in range [0,1].";
        throw GeneralException("FitterUpperLimit::setFillColour",
                msg.str() );        
    }
    m_fillColour[cl] = colour;
}


//-----------------------------------------------------------------------------
// checks if a given confidence value is defined in the range [0,1].
//-----------------------------------------------------------------------------
Bool_t FitterUpperLimit::isCLValueOK( Double_t cl )
{
    return ( cl < 0.0 || cl > 1.0 ) ? false : true;
}


//-----------------------------------------------------------------------------
// Add the plot confidence interval to the list of confidence levels to plot
//-----------------------------------------------------------------------------
void FitterUpperLimit::addConfidenceAttributes( Double_t cl, 
        Color_t colour, Style_t style )
{
    this->setFillStyle( cl, style );
    this->setFillColour( cl, colour );
}


//-----------------------------------------------------------------------------
// calculates the value for the poi for the CL.
//-----------------------------------------------------------------------------
void FitterUpperLimit::setUniformPrior( Double_t x ) 
{
    m_lowerBound = x;
}


//-----------------------------------------------------------------------------
// plots the values for the poi for the CL specified on a TCanvas in different
// formats pathname.{eps,pdf,png,C}
//-----------------------------------------------------------------------------
void FitterUpperLimit::plotLimits( const TString& pathname, 
        const TString& xtitle ) 
{
    // Annoyingly we need to open a file to
    TString dummyFileName = "dummy_file_121.root";

    TFile *f = TFile::Open( dummyFileName, "RECREATE" );
    if ( !f || f->IsZombie() ) {
        throw IOFailure("Fitter1DLikeProfile::plotGraph", 
                dummyFileName.Data(), "READ" );
    }
    f->cd();

    TCanvas* canvas = 0;
    try {
        canvas = new TCanvas( "Graph", "Graph" );
    }
    catch ( std::bad_alloc& e ) {
        throw GeneralException( "Fitter1DLikeProfile::plotGraph",
                "Got std::bad_alloc when creating new TCanvas" );
    } 

    gStyle->SetOptStat(0);
    m_profile.pGraph()->Draw( "al" );
    m_profile.pGraph()->SetTitle( "" );
    m_profile.pGraph()->GetXaxis()->SetTitle( xtitle );
    m_profile.pGraph()->GetYaxis()->SetTitle( "Likelihood" );
    canvas->Update();

    // We use copies of the nominal Fitter1DLikeProfile to create a series
    // of reduced TGraph's internally so that they can be plotted in the 
    // desired range. This is ok as we join the ends of the TGraph together
    // so it forms a useful close solid which we can then fill in with a 
    // different colour and style for a given confidence level.

    std::vector<Fitter1DLikeProfile*> limits( m_upperLimits.size() );

    std::map<Double_t,Double_t>::const_iterator it = m_upperLimits.begin();
    const std::map<Double_t,Double_t>::const_iterator endit = m_upperLimits.end();
    Double_t cl(0.0), lowerBound(m_lowerBound), 
             upperLimit(0.0), canvasYmin( canvas->GetUymin() );

    Int_t np(0), colour(2), style1(3353), style2(3335), style(style1);
    for ( Int_t i(0); it != endit; ++it, ++i ) {

        cl = it->first;
        upperLimit = it->second;

        // Reset the Fitter1DLikeProfile
        limits[i] = new Fitter1DLikeProfile( m_profile );
        limits[i]->SetName( ("copy" + string_tools::to_string<Double_t>( cl ) ).c_str() );

        // Change the range of the TGraph for plotting purposes.
        limits[i]->setRange( lowerBound, upperLimit );
        np = limits[i]->npoints();
        
        // now close the plot by adding 3 more points to the graph
        // first point.
        limits[i]->pGraph()->SetPoint( np, 
                limits[i]->pGraph()->GetX()[ np - 1 ], 
                canvasYmin );

        // second point
        limits[i]->pGraph()->SetPoint( np + 1, 
                limits[i]->pGraph()->GetX()[ 0 ],
                limits[i]->pGraph()->GetY()[ np ] );

        // third and final points
        limits[i]->pGraph()->SetPoint( np + 2, 
                limits[i]->pGraph()->GetX()[ 0 ], 
                limits[i]->pGraph()->GetY()[ 0 ] );

        // now on to the plotting styles
        if( colour == 10 || colour == 0 || colour == 18 || colour == 19 ) {
            colour++;
            if(colour==19) colour++;
            // we do this just to avoid plotting with any dodgy colours such
            // as white, user should specify what it is that they want if 
            // they do not want default plotting styles.
        }

        limits[i]->pGraph()->SetFillStyle( 
                ( m_fillStyle.find( cl ) != m_fillStyle.end() ) ? m_fillStyle[cl] : style 
                );
        std::swap( style, style1 ); // switch the default style
        std::swap( style, style2 );
        
        limits[i]->pGraph()->SetFillColor( 
                ( m_fillColour.find( cl ) != m_fillColour.end() ) ? m_fillColour[cl] : colour++ 
                );
        limits[i]->pGraph()->SetLineColor( 0 );
        limits[i]->pGraph()->Draw("f same");    

        // move along the min range value
        lowerBound = upperLimit;
    }

    // save the plots
    canvas->SaveAs( pathname + ".pdf" );
    canvas->SaveAs( pathname + ".png" );
    canvas->SaveAs( pathname + ".eps" );
    canvas->SaveAs( pathname + ".C" );

    // clean up
    delete canvas; canvas=0;
    f->Close();
    delete f; f=0;

    std::vector<Fitter1DLikeProfile*>::iterator iter = limits.begin();
    const std::vector<Fitter1DLikeProfile*>::const_iterator end = limits.end();
    for(; iter != end; ++iter ) {
        delete *iter; *iter=0;
    }

    // remove the temp datafile permanently.
    TSystemFile sysFile( dummyFileName, "." );
    sysFile.Delete();
}


//-----------------------------------------------------------------------------
// get upper limit for specified limit.
//-----------------------------------------------------------------------------
Double_t FitterUpperLimit::getUpperLimit( Double_t atCL ) const 
{ 
    if (atCL<0.0) {
        return m_upperLimits.find(m_confidenceLevel)->second;
    } else {
        return m_upperLimits.find(atCL)->second; 
    }
}


//-----------------------------------------------------------------------------
// sets the maximum number of interations to find the upper limit of the poi.
//-----------------------------------------------------------------------------
void FitterUpperLimit::setMaxIterations( UInt_t niter )
{
    m_maxIterations = niter;
}


//-----------------------------------------------------------------------------
// calculates the value for the poi for the CL.
//-----------------------------------------------------------------------------
void FitterUpperLimit::calculate( Double_t cl )
{
    // should have a check as no point doing this if we have already
    // found the CL upper limit for a given cl.

    // Just check we're not being stupid
    if( cl == 0.0 ) {
        m_upperLimits[0] = 0.;
    }
    if( cl == 1.0 ) {
        m_upperLimits[1] = m_profile.getXmax();
    }

    Double_t confidenceLevel = cl;
    if(confidenceLevel < 0) {
        std::cerr << "INFO: FitterUpperLimit::calculate - Using internally cached CL: " 
            << m_confidenceLevel << std::endl;
        confidenceLevel = m_confidenceLevel;
    }

    // now we want sensible confidence range
    if( confidenceLevel < 1 && confidenceLevel > 0)
    {
        // reset the upper limit
        m_upperLimits[confidenceLevel] = 0.;

        // first need a likelihood function
        if( !m_profile.isLikelihood() ) {
            m_profile.convertToLikelihood();
        }

        // find the total area from the bound to infinity
        Double_t maxR = m_profile.getXmax();
        Double_t total_integral = m_profile.integrate( m_lowerBound, maxR );

        // recursively loop over untill we get the right value to some 
        // precision 1.e-5.
        Double_t integral_x(0.), ratio(0.), minR( m_lowerBound ), epsilon(1.E-5) ;
        UInt_t counter(1);
        while( TMath::Abs( confidenceLevel - ratio ) > epsilon ) {
            m_upperLimits[confidenceLevel] = 0.5*(maxR + minR);
            integral_x = m_profile.integrate( m_lowerBound, m_upperLimits[confidenceLevel] );
            ratio = integral_x / total_integral;
            if( ratio > confidenceLevel ) {
                maxR = m_upperLimits[confidenceLevel];
            }
            else {
                minR = m_upperLimits[confidenceLevel];
            }
            
            // safety check to make sure nothing absolutely crazy is going on...
            if( counter > m_maxIterations ) {
                std::stringstream msg;
                msg << "Maximum number of iterations " << m_maxIterations 
                    << " has been exceeded.";
                throw GeneralException("FitterUpperLimit::calculate",
                        msg.str() );   
            }
        }
    }
}



//=============================================================================
// Destructor
//=============================================================================
FitterUpperLimit::~FitterUpperLimit()
{}
