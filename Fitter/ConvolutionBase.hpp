#ifndef FAST_FITTER_CONVOLUTIONBASE
#define FAST_FITTER_CONVOLUTIONBASE 1

/***************************
 * ConvolutionBase       *
 *                         *
 * � Matthew M Reid        *
 ***************************/

// STL include
#include <iosfwd>

// ROOT 
#include "Rtypes.h"
#include "TGraph.h"

// RooFit

/** @class Fitter ConvolutionBase.hpp Fitter/ConvolutionBase.hpp

  Base class to apply a systematic convolution to a set of points in a 
  provided TGraph. 

  @author Matthew M Reid
  @date   2014-01-08
  */

namespace Fast {

    class ConvolutionBase {

        public:

            ClassDef( ConvolutionBase, 1 ); // ConvolutionBase Class	

            virtual TGraph convolution( Double_t syst_err ) const = 0;
    };
}

#endif // FAST_FITTER_CONVOLUTIONBASE

