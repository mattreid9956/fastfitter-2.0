// $Id: $
#ifndef FAST_FITTERUPPERLIMIT_H
#define FAST_FITTERUPPERLIMIT_H 1

// Include files
// STL include
#include <string>
#include <map>

// ROOT
#include "Rtypes.h"
#include "TNamed.h"
#include "TAttLine.h"

// RooFit
#include "RooCmdArg.h"
#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooMsgService.h"
#include "RooGlobalFunc.h"

// Local includes
#include "Fitter1DLikeProfile.hpp"



/** @class Fitter FitterUpperLimit.hpp Fitter/FitterUpperLimit.hpp

  Class to perform the upper limit calculation given a Fitter1DlikeProfile 
  and a baysian prior.
  physical parameters (e.g. reconstructed mass of a particle) to a model PDF,
  constructing sWeights, creating reduced datasets and plotting fit results.
  Note that the model PDF must be of type RooAddPdf (or inherit from it),
  since several methods (include the construction of the sPlot and the 
  plotting methods) require a RooArgList of coefficients and PDFs.

  @author Matthew M Reid
  @date   2014-01-08
  */

namespace Fast // Fiting Analysis Simulataneous Toys
{
    class FitterUpperLimit //: public FitterLimitsBase
    {
        public:

            /// Standard constructor.
            FitterUpperLimit( );

            /** Overloaded constructor.
              @param profile to be used to calculate the limit.
              @param cl confidence level to determine value.
              */
            FitterUpperLimit( const Fitter1DLikeProfile& profile, 
                    Double_t cl = .95 );

            virtual ~FitterUpperLimit( ); ///< Destructor


            /// calculate - determines the parameter of interest at the CL%.
            virtual void calculate( Double_t cl = -1.0 );


            /** setConfidenceLevel - sets the confidence level which will be
             * determined for the parameter of interest.
             @param cl the confidence level.
             */
            virtual void setConfidenceLevel( Double_t cl );


            /** setUniformPrior - sets the lower bound for the integration
             * this is rubbish that it will be hard coded, RooStats would
             * be soooo much cooler here...
             @param x lower bound prior value.
             */
            virtual void setUniformPrior( Double_t x = 0.0 );


            /** setConfidenceLevel - sets the confidence level which will be
             * determined for the parameter of interest.
             @param cl the confidence level.
             @param style the TGraphPainter fill stype.
             */
            virtual void setFillStyle( Double_t cl, Style_t style );


            /** setConfidenceLevel - sets the confidence level which will be
             * determined for the parameter of interest.
             @param cl the confidence level.
             @param colour the TGraphPainter fill colour.
             */
            virtual void setFillColour( Double_t cl, Color_t colour );


            /** getUpperLimit - returns the upper limit.
              @param cl the confidence level.
              */
            virtual Double_t getUpperLimit( Double_t atCL = -1. ) const ;


            /** setMaxIterations - sets the maximum number of loops used to find 
             * the CL limit. Will throw is niter is exceeded. Will default to
             * 1E+7.
             @param niter the number of maximum iterations used to calculate CL
             */
            virtual void setMaxIterations( UInt_t niter ) ;


            /** isCLValueOK - checks if a given confidence value is defined in
             * the range [0,1].
             @param cl the confidence level.
             */
            static Bool_t isCLValueOK( Double_t cl ) ;


            /** setConfidenceAttributes - add the plot confidence interval 
             * to the list of confidence levels to plot
             @param cl the confidence level.
             @param colour the TGraphPainter fill colour.
             @param style the TGraphPainter fill stype.
             */
            virtual void addConfidenceAttributes( Double_t cl, 
                    Color_t colour, Style_t style );


            /// getConfidenceLevel - returns the current confidence level.
            virtual Double_t getConfidenceLevel() const { return m_confidenceLevel; }


            /** plotLimits - plots all the limits that have been calculated so far
             * in order of increasing CL level.
             @param pathname the file name path to be stored.
             @param xtitle the name of the poi.
             */ 
            virtual void plotLimits( const TString& pathname,
                    const TString& xtitle );

        private:

            Fitter1DLikeProfile m_profile;
            Double_t m_maxIterations;
            Double_t m_confidenceLevel;
            Double_t m_lowerBound;
            std::map<Double_t, Double_t> m_upperLimits;
            std::map<Double_t, Double_t> m_fillColour;
            std::map<Double_t, Double_t> m_fillStyle;
            std::vector<Fitter1DLikeProfile*> m_limits;

            /// createGraphs - produces the new'd Fitter1DLikeProfile's in range.
            void createPlots();

            ClassDef( FitterUpperLimit, 1 );
    };
}

#endif // FAST_FITTERUPPERLIMIT_H
