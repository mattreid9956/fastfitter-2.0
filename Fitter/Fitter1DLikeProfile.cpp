/***************************
 * Fitter1DLikeProfile       *
 ***************************/

// STL includes
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>        // std::floor
#include <sstream>
#include <algorithm>    // std::min_element, std::max_element
#include <functional>   // std::bind1st and std::mem_fun

// ROOT
#include "TCanvas.h"
#include "TArrow.h"
#include "TFile.h"
#include "TLine.h"
#include "TSystemFile.h"
#include "TStyle.h"
#include "Math/Functor.h"
#include "Math/RootFinder.h"
#include "Math/RootFinderAlgorithms.h" // for finding the root
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/IFunction.h"
#include "Math/Integrator.h"
#include "Math/GaussLegendreIntegrator.h"
#include "Math/ProbFuncMathCore.h" // Both for confidence level interpretation
#include "Math/QuantFuncMathCore.h"

// RooFit

// local include
#include "Fitter1DLikeProfile.hpp"
#include "Exceptions.hpp"
#include "Fitter1DLikeProfileFFTConv.hpp"

using namespace Fast;
using Fast::IOFailure;
using Fast::TDirectoryError;
using Fast::GeneralException;


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::m_precision = 1.E-4;


//-----------------------------------------------------------------------------
Fitter1DLikeProfile::Fitter1DLikeProfile()
    : TNamed(),
    m_isLikelihood( kFALSE ), 
    m_minimum( -std::numeric_limits<Double_t>::max() ), 
    m_hilowerr( std::make_pair( -std::numeric_limits<Double_t>::max(), 
                std::numeric_limits<Double_t>::max() ) ),
    m_sigmaerr( 1.0 )
{}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile::Fitter1DLikeProfile( const char* name, const char* title )
    : TNamed( name, title ),
    m_isLikelihood( kFALSE ), 
    m_minimum( -std::numeric_limits<Double_t>::max() ), 
    m_hilowerr( std::make_pair( -std::numeric_limits<Double_t>::max(), 
                std::numeric_limits<Double_t>::max() ) ),
    m_sigmaerr( 1.0 )
{}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile::Fitter1DLikeProfile( const char* name, const char* title,
        TString filename, TString graphname, Double_t sigma  )
    : TNamed( name, title ),
    m_isLikelihood( kFALSE ), 
    m_minimum( -std::numeric_limits<Double_t>::max() ), 
    m_sigmaerr(sigma)
{
    this->loadProfile( filename, graphname );
}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile::Fitter1DLikeProfile( const char* name, const char* title,
        TGraph dllgraph, Double_t sigma )
    : TNamed( name, title ),
    m_isLikelihood( kFALSE ), 
    m_minimum( -std::numeric_limits<Double_t>::max() ), 
    m_sigmaerr( sigma )
{
    this->setGraph( dllgraph );
}

//-----------------------------------------------------------------------------
Fitter1DLikeProfile::Fitter1DLikeProfile( const Fitter1DLikeProfile& other )
    : TNamed( other ),
    m_graph( other.graph() ), 
    m_spline( other.spline() ), m_isLikelihood( other.isLikelihood() ),
    m_minimum( other.minimum() ), m_hilowerr( other.getErrorHiLow() ),
    m_sigmaerr( other.getSigmaLevelError() )
{}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile::~Fitter1DLikeProfile()
{
    /*if ( m_spline ) {
        delete m_spline; m_spline=0;
    }
    if ( m_graph ) {
        delete m_graph; m_graph=0;
    }*/
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::loadProfile( const TString& filename, 
        const TString& graphname )
{
    TFile *f = TFile::Open( filename, "READ" );
    if ( !f || f->IsZombie() ) {
        throw IOFailure("Fitter1DLikeProfile::loadProfile", 
                filename.Data(), "READ" );
    }

    const TGraph* graph = dynamic_cast<TGraph*>( f->Get( graphname ) );
    if (!graph) {
        throw TDirectoryError( "Fitter1DLikeProfile::loadProfile", 
                *f, graphname.Data() );
    }

    // Make a copy so we can close the file.
    TGraph *clone = dynamic_cast<TGraph*>( graph->Clone() );
    m_graph = *clone;
    delete clone; clone=0;

    // close the file and tidy up
    f->Close();
    
    // Create the approximation to the likelihood
    this->createSpline();

    // find minimum x-value
    this->findMinimum();

    // calc error bands
    this->calcErrors();
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::minimisefunction( const Double_t* xx ) 
{
    return function( xx[0] );
}


//-----------------------------------------------------------------------------
/*
 * Calculates the approximate x-value based on the minimum dll value. We do this
 * by simply finding the x position in the array for the smallest y element.
 * This should be the fitted value for the parameter of interest that was 
 * scanned to create the dll.
 */
void Fitter1DLikeProfile::findApproxMinimum() 
{
    // FIRST ESTIMATE... Find the minimum point from the data.
    std::vector<Double_t> y;
    y.assign( m_graph.GetY(), m_graph.GetY() + m_graph.GetN() ) ;
    size_t pos = std::distance( y.begin(), std::min_element( y.begin(), y.end() ) );
    m_minimum = m_graph.GetX()[ pos ];
}


//-----------------------------------------------------------------------------
/*
 * Calculates the x-value based on the minimum dll value. This should be the
 * fitted value for the parameter of interest that was scanned to create the
 * dll.
 */
void Fitter1DLikeProfile::findMinimum() 
{
    bool likelihood = m_isLikelihood;
    if( m_isLikelihood ) {
        this->convertToLogLikelihood();
    }

    // find the x position where the dll is a minimum from TGraph.
    this->findApproxMinimum();

    // Not interpolate using the spline fit.
    // set default minimiser and algo. These are HARD coded for now but it 
    // is a simple minima search so not a big deal.
    std::string minName("Minuit2"), algoName("Migrad");
    ROOT::Math::Minimizer* min = 
        ROOT::Math::Factory::CreateMinimizer( minName, algoName );

    // set tolerance , etc...
    min->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2 
    min->SetTolerance( 1.e-5 );

    ROOT::Math::Functor f( std::bind1st( 
                std::mem_fun( &Fitter1DLikeProfile::minimisefunction ), this 
                ), 1 );

    // set the minimisation function.
    min->SetFunction( f );

    // Set the free but bounded variable to be minimized!
    min->SetLimitedVariable( 0,      // var number
            "x",              // name
            m_minimum,        // initial value
            1.E-5,            // step size
            this->getXmin(),  // min range
            this->getXmax()   // max range
            );

    // do the minimization
    min->Minimize();  

    // expected minimum is 0, but due to precision and fact we didnt scan
    // every point in the likelihood function, slightly >= 0.
    if ( min->MinValue() < 0. ) {
        // as below, it might be good to have some tolerance here, we should be
        // near 0 and not massively negative.
        //this->transformY( min->MinValue(), "-" );
        //this->findMinimum();
        // if it is only slightly negative -1.E-3 then shift up the dll.
        if( min->MinValue() > - 1.E-3 ) {
            std::stringstream msg;
            msg <<  "Negative delta log likelihood minimum found: "
                << min->MinValue() << ", shifting datapoints.";
            std::cerr << "WARNING: Fitter1DLikeProfile::findMinimum - "
                << msg.str() << std::endl;
            //this->renormalise();
            this->transformY( min->MinValue(), "-" );
            this->findMinimum();
        } else {
            Error( "NumericalMinimisation", "failed to converge" );
            std::stringstream msg;
            msg << "Minimiser " << minName << " - " << algoName 
                << "   failed to converge! Evaluated at f(" << min->X()[0] << "): " 
                << min->MinValue() << ".";
            throw GeneralException("Fitter1DLikeProfile::findMinimum",
                    msg.str() ) ;     
        }
    }

    // set the minimum 
    m_minimum = min->X()[0];

    // this should't cause a problem as max element should be be 1 
    // in any case for the likelihood.
    this->renormalise();

    // revert to original state, shift back up and set to previous 
    // likelihood state.
    if( likelihood != m_isLikelihood ) {
        this->convertToLikelihood();
    }
}


//-----------------------------------------------------------------------------
/*
 * Removes any points that lie outside the specified range [min, max].
 */
void Fitter1DLikeProfile::setRange( Double_t min, 
        Double_t max ) {

    this->setXmin( min );
    this->setXmax( max );
}


//-----------------------------------------------------------------------------
/*
 * Returns the integral of the spline on the bounded interval [xmin, xmax].
 */
Double_t Fitter1DLikeProfile::integrate( Double_t xmin, 
        Double_t xmax ) const {

    if ( xmin < m_graph.GetX()[0] ) {
        xmin = m_graph.GetX()[0];
    }

    if ( xmax > m_graph.GetX()[ m_graph.GetN() -1 ] ) {
        xmax = m_graph.GetX()[ m_graph.GetN() -1 ];
    }

    // check that xmin is not larger than xmax, is so then swap.
    if ( xmin > xmax ) {
        std::swap ( xmin, xmax );
    }

    // Defind the functor binding to the TSpline Eval call.
    ROOT::Math::Functor1D f1D( 
            std::bind1st( std::mem_fun( &Fitter1DLikeProfile::function ), this ) 
            );

    // Create the Integrator
    ROOT::Math::GaussLegendreIntegrator ig;

    // Set parameters of the integration
    ig.SetFunction( f1D );
    ig.SetRelTolerance( m_precision );
    ig.SetNumberPoints( 5 * m_graph.GetN() );

    return ig.Integral( xmin, xmax );
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::calcErrors( ) 
{
    bool likelihood = m_isLikelihood;
    if( m_isLikelihood ) {
        this->convertToLogLikelihood();
    }

    // Via Wilks theorem transfrom the desired sigma reading
    //      sigma = sqrt( dll ).
    double readSigmaAt = 0.5 * m_sigmaerr * m_sigmaerr;

    // Now shift the negative log likelihood function down so that we
    // can find the roots. It should be shifted consistent with the expected
    // number of sigma.
    //     i.e for 1sigma --> -0.5, 2sigma --> -2, 3sigma --> -4.5! 
    this->transformY( readSigmaAt, "-" );

    // Create RootFinder with derivative functions.
    // WOULD BE WAY COOLER TO USE C++11 here and std::bind or std::function.
    // Unfortunately because of the way the MemFunctions are implemented in ROOT
    // we cannot pass by &Fitter1DLikeProfile::function and so must wrap them using
    // std::mem_fun and std::bind1st;
    ROOT::Math::GradFunctor1D f1DG( 
            std::bind1st( std::mem_fun( &Fitter1DLikeProfile::function ), this ),
            std::bind1st( std::mem_fun( &Fitter1DLikeProfile::derivative ), this )
            );

    // We use the Brent method here, this could become a configurable...
    ROOT::Math::RootFinder rfb( ROOT::Math::RootFinder::kBRENT );

    // Obtain the first x-axis root.
    Double_t start = this->getXmin();
    // set and solve within the desired range
    rfb.SetFunction( f1DG, start, m_minimum );
    rfb.Solve();

    //  std::cout << "Range (start,minimum) --> (" << start << "," << m_minimum << ")" << std::endl;
    Double_t lower = -std::numeric_limits<Double_t>::max();
    if( rfb.Status() == 0 ) {
        lower = rfb.Root();
    } else {
        std::stringstream msg;
        msg << "Failed to find lower error at the " << m_sigmaerr << " sigma level.";
        throw GeneralException("Fitter1DLikeProfile::calcErrors",
                msg.str() ) ;
    }

    // obtain the second x-axis root.
    Double_t end = this->getXmax();
    rfb.SetFunction( f1DG, m_minimum, end );
    rfb.Solve();
    //  std::cout << "Range (minimum,end) --> (" << m_minimum << "," << end << ")" << std::endl;

    Double_t upper = std::numeric_limits<Double_t>::max();
    if( rfb.Status() ==0 ) {
        upper = rfb.Root();
    } else {
        std::stringstream msg;
        msg << "Failed to find upper error at the " << m_sigmaerr << " sigma level.";
        throw GeneralException("Fitter1DLikeProfile::calcErrors",
                msg.str() ) ;
    }

    // revert to original state, shift back up and set to previous 
    // likelihood state.
    this->transformY( readSigmaAt, "+" );
    if( likelihood != m_isLikelihood ) {
        this->convertToLikelihood();
    }

    // return the difference between the bounds and the minimum
    // to provide the +/- asymmetric error estimate.
    m_hilowerr = std::make_pair( upper - m_minimum, lower - m_minimum );
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::function( Double_t x ) const
{
    // Evaluate the likelihood function at a given x-value.
    return m_spline.Eval( x );
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::derivative( Double_t x ) const
{
    // Evaluate the derivative of the likelihood function at a given x-value.
    return m_spline.Derivative( x );
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::createSpline() 
{
    // points must be sorted before using a TSpline
    m_graph.Sort();

    //try {
    // spline interpolation creating a new spline
    m_spline = SPLINETYPE( "", m_graph.GetX(), m_graph.GetY(), m_graph.GetN() );
    //}
    /*catch ( std::bad_alloc& e ) {
      throw GeneralException( "Fitter1DLikeProfile::createSpline",
      "Got std::bad_alloc when creating new TSpline5" );
      }*/    
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::significance( Double_t x ) const {

    Double_t val0 = this->function( x );
    // Now need to decide if data is -dll or likelihood
    if( !m_isLikelihood ) {
        return TMath::Sqrt( 2.0 * val0 );
    } else {
        return TMath::Sqrt( 2.0 * -1.0 * TMath::Log( val0 ) );
    }
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::transformX( Double_t f, std::string op ) 
{
    // Transformation of the coordinates for a given operation
    Double_t x(0.0), y(0.0);
    for( Int_t i(0); i < m_graph.GetN(); ++i ) {

        m_graph.GetPoint( i, x, y );

        if( op == "+" ) {
            x += f;
        } else if ( op == "-" ) {
            x -= f;
        } else if ( op == "/" ) {
            x /= f;
        }  else if ( op == "*" ) {
            x *= f;
        } else {
            std::stringstream msg;
            msg << "Failed to apply x operation type " << op;
            throw GeneralException("Fitter1DLikeProfile::transformX",
                    msg.str() ) ;
        }
        m_graph.SetPoint( i, x, y );
    }

    // Create the approximation to the likelihood
    this->createSpline();

    // find the minimum x value
    this->findMinimum();

    // calc errors
    this->calcErrors();
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::transformY( Double_t f, std::string op ) 
{
    // Transformation of the coordinates for a given operation
    Double_t x(0.0), y(0.0);
    for( Int_t i(0); i < m_graph.GetN(); ++i ) {

        m_graph.GetPoint( i, x, y );

        if( op == "+" ) {
            y += f;
        } else if ( op == "-" ) {
            y -= f;
        } else if ( op == "/" ) {
            y /= f;
        }  else if ( op == "*" ) {
            y *= f;
        } else {
            std::stringstream msg;
            msg << "Failed to apply y operation type " << op;
            throw GeneralException("Fitter1DLikeProfile::transformY",
                    msg.str() ) ;
        }

        m_graph.SetPoint( i, x, y );
    }

    // Create the approximation to the likelihood
    this->createSpline();
}


//-----------------------------------------------------------------------------
/*Fitter1DLikeProfile::Fitter1DLikeProfile( TH1* hist, Double_t xscale,
  Double_t yscale ) 
  : m_npoints( hist->GetNbinsX() )
  {
// Will use a TGraph for ease... Maybe we should just use that in the first place... 
TGraph* gr = 0;

try {
gr = new TGraph( nbins );
}
catch ( std::bad_alloc& e ) {
throw GeneralException( "Fitter1DLikeProfile::Fitter1DLikeProfile",
"Got std::bad_alloc when creating new TGraph" );
}

// Fill the graph with the histogram dataset.
for( Int_t bin(1); bin <= hist->GetNbinsX(); ++bin ) {
gr->SetPoint( bin-1, hist->GetBinCenter( bin ), hist->GetBinContent( bin ) );
}

// This is just lazy but fine...
m_graph = dynamic_cast<TGraph*>(gr->Clone());
delete gr; gr=0;
createSpline();
//    m_x.assign( gr->GetX(), gr->GetX() + nbins ) ;
//    m_y.assign( gr->GetY(), gr->GetY() + nbins ) ;
}
*/


//-----------------------------------------------------------------------------
/*
 * Calculates the numeric convolution of a point over a given range
 */
/*
   double Fitter1DLikeProfile::convolutionPoint( double p, double sigma, int min, int max )
   {
   double result = 0.;

   for ( int i = min; i < max; ++i ) {
   result += m_graph.GetY()[i] * TMath::Gaus( p - m_graph.GetX()[i], 0.0, sigma );
   }
   result /= ( max - min );
   return result;
   }
   */


//-----------------------------------------------------------------------------
/*
 * Will remove points that lie below the min  value provided.
 */
void Fitter1DLikeProfile::setXmin( Double_t min )
{
    if( min < this->getXmax() ) {
        Int_t count(0);
        Double_t x(0.0), y(0.0);
        for (Int_t i( this->npoints()-1 ); i >= 0; i--) {
            m_graph.GetPoint( i, x, y );
            if ( x < min ) {
                ++count;
                m_graph.RemovePoint(i); 
            }
        }
        // now set the end point to be the maximum,
        Double_t y_at_min = this->function( min );
        m_graph.SetPoint( this->npoints() - 1, min, y_at_min);

        // update the spline
        this->createSpline();
        std::cerr << "INFO: Fitter1DLikeProfile::setXmin - removed " 
            << count << " points from likelihood." << std::endl;
    }
    else {
        // throw an error if trying to set the minimum for the maximum.
        std::stringstream msg;
        msg << "Cannot cut at a minimum("<< min << ") value that is greater than the maximum(" 
            << this->getXmax() << ") bound.";
        throw GeneralException("Fitter1DLikeProfile::setXmin",
                msg.str() ) ; 
    }
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::systConvolution( Double_t syst_error, TString type ) 
{
    bool isLikelihood( m_isLikelihood );

    // Convert -loglikelihood to likelihood
    this->convertToLikelihood();

    ConvolutionBase* convolver = 0;

    // store the minimum
    Double_t min = m_minimum;
    // at the moment use the default fftBins scheme maybe we should change
    // this though?...
    if( type == "FFT" ) {

        try {
            convolver = new Fitter1DLikeProfileFFTConv( *this );
        }
        catch ( std::bad_alloc& e ) {
            throw GeneralException( "Fitter1DLikeProfile::systConvolution",
                    "Got std::bad_alloc when creating new Fitter1DLikeProfileFFTConv" );
        }         

        // update the internal graoh with the convolved one.
        this->setGraph( convolver->convolution( syst_error ) );
        //this->setGraph( convolver->convolution( syst_error / absmin ) );
        //this->scaleX( absmin );
    } else {
        // throw an error if the type is not defined.
        std::stringstream msg;
        msg << "Convolution not valid for type requested: " << type;
        throw GeneralException("Fitter1DLikeProfile::systConvolution",
                msg.str() ) ;          
    }

    // cleanup.
    delete convolver; convolver=0;

    if( !isLikelihood ) { // convert back to -dll
        this->convertToLogLikelihood();
    }

    Double_t diffshift = min - m_minimum;
    if( diffshift <= 0 ) {
        this->transformX( TMath::Abs( diffshift ), "-" );
    } else {
        this->transformX( TMath::Abs( diffshift ), "+" );
    }

    // Create spline
    this->createSpline();
    // find minimum x-value and errors is already done in the setGraph.
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::convertToLikelihood()
{
    if( !m_isLikelihood ) { // no point if it is alread a likelihood.
        Double_t x(0.0), y(0.0);
        for( Int_t i = 0; i < m_graph.GetN(); ++i ) {
            m_graph.GetPoint( i, x, y );
            y = TMath::Exp( -y );
            m_graph.SetPoint( i, x, y );
        }
        // Set the likelihood flag to true since we have transformed.
        m_isLikelihood = kTRUE;
    }
    else {
        std::cerr << "INFO: Fitter1DLikeProfile - Already in Likelihood frame.\n";
    }

    // Create spline
    this->createSpline();
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::convertToLogLikelihood( )
{
    if( m_isLikelihood ) { // no point is it is already -dll.
        Double_t x(0.0), y(0.0);
        for ( Int_t i = 0; i < m_graph.GetN(); ++i ) {
            m_graph.GetPoint( i, x, y );
            y = -TMath::Log( y );
            m_graph.SetPoint( i, x, y );
        }
        // Set the likelihood flag to false after this transfrom.
        m_isLikelihood = kFALSE;
    }
    else {
        std::cerr << "INFO: Fitter1DLikeProfile - Already in Negative Log Likelihood frame.\n";
    }

    // Create spline
    this->createSpline();
}


//-----------------------------------------------------------------------------
/*
 * Returns the maximum x-axis range.
 */
Double_t Fitter1DLikeProfile::getXmax( ) const 
{
    return m_graph.GetX()[ this->npoints() - 1 ] ;
}


//-----------------------------------------------------------------------------
/*
 * Returns the minimum x-axis range.
 */
Double_t Fitter1DLikeProfile::getXmin( ) const
{
    return m_graph.GetX()[ 0 ];
}


//-----------------------------------------------------------------------------
/*
 * Will remove points that lie above the max value provided.
 */
void Fitter1DLikeProfile::setXmax( Double_t max )
{
    if( max > this->getXmin() ) {
        Int_t count(0);
        Double_t x(0.0), y(0.0);
        for (Int_t i( this->npoints()-1 ); i >= 0; i--) {
            m_graph.GetPoint( i, x, y );
            if ( x > max ) {
                ++count;
                m_graph.RemovePoint(i); 
            }
        }
        // now set the end point to be the maximum,
        Double_t y_at_max = this->function( max );
        m_graph.SetPoint( this->npoints() - 1, max, y_at_max);
       
        // update the spline.
        this->createSpline();
        std::cerr << "INFO: Fitter1DLikeProfile::setXmax - removed " 
            << count << " points from likelihood." << std::endl;
    }
    else {
        // throw an error if trying to set the minimum for the maximum.
        std::stringstream msg;
        msg << "Cannot cut at a maximum("<< max << ") value that is less than the minimum(" 
            << this->getXmin() << ") bound.";
        throw GeneralException("Fitter1DLikeProfile::setXmax",
                msg.str() ) ; 
    }
}


//-----------------------------------------------------------------------------
/*
 * Code below should not be trusted at the edges as we do not check we have
 * points outside of range.
 */
/*
   void Fitter1DLikeProfile::convolution( double sigma, int nS )
   {

   Double_t* nbs = m_graph.GetX();
   Double_t* like = m_graph.GetY();
   Double_t xi(0.0), conv(0.0);
   for( Int_t i(0); i < m_graph.GetN(); ++i ) {

   m_graph.GetPoint( i, xi, conv );
// Determine borders for given point
Double_t nbsMin = xi - nS * sigma;
Double_t nbsMax = xi + nS * sigma;
if( nbsMin < nbs[0] ) {
nbsMin = nbs[0];
}
if( nbsMax > nbs[m_graph.GetN() - 1] ) {
nbsMax = nbs[m_graph.GetN() - 1];
}
Int_t min = i;
Int_t max = m_graph.GetN() - 1;
while ( nbs[min] > nbsMin ) {
--min;
}
while ( nbs[max] > nbsMax) {
--max;
}

// Now do convolution integral
conv = convolutionPoint( xi, sigma, min, max );
m_graph.SetPoint( i, xi, conv ); // set the new point
}
}
*/


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::renormalise( )
{
    // should only work for likelihood profiles, not dlls.
    bool likelihood = m_isLikelihood;
    if( !m_isLikelihood ) {
        this->convertToLikelihood();
    }

    // find maximum
    //Double_t max = TMath::MaxElement( m_graph.GetN(), m_graph.GetY() );
    // renormalise
    transformY( this->function( m_minimum ), "/" );

    if( !likelihood ) {
        this->convertToLogLikelihood();
    }
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::scaleY( Double_t factor )
{
    // scales the y-coordinates with respect to the factor in the argument
    Double_t x(0.0), y(0.0);
    for ( Int_t i(0); i < m_graph.GetN(); ++i ) {
        m_graph.GetPoint( i, x, y );
        y *= factor;
        m_graph.SetPoint( i, x, y );
    }

    // Create the approximation to the likelihood
    this->createSpline();

    // calc errors
    this->calcErrors();
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::scaleX( Double_t factor )
{
    // scales the x-coordinates with respect to the factor in the argument
    Double_t x(0.0), y(0.0);
    for ( Int_t i(0); i < m_graph.GetN(); ++i ) {
        m_graph.GetPoint( i, x, y );
        x *= factor;
        m_graph.SetPoint( i, x, y );
    }

    // Create the approximation to the likelihood
    this->createSpline();

    // find the minimum x value
    this->findMinimum();

    // calc errors
    this->calcErrors();
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::save( const TString& filename, const TString& name, 
        Bool_t update )
{
    // Set the name of the graoh and spline
    TString n = GetName();
    if( n == "" ) {
        n = "Fitter1DLikeProfile";
    }

    if( name != "" ) {
        n = name + "_" + n;
    }

    TString type = "dllhood";
    if( m_isLikelihood ) {
        type = "lhood";
    }

    n = n + "_" + type;

    m_graph.SetName( n );
    m_spline.SetName( n + "_spline" );

    TString mode = ( update ) ? "UPDATE" : "RECREATE";

    TFile* f = TFile::Open( filename, mode );
    if ( !f || f->IsZombie() )
    {
        throw IOFailure("Fitter1DLikeProfile::save", filename.Data(), 
                mode.Data() );
    }

    f->cd();

    // Write the graph and spline
    m_graph.Write(); 
    m_spline.Write();    

    // Close the file
    f->Close();
    delete f; f=0;
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfile::getConfidenceLevelError() const 
{ 
    return ( ROOT::Math::normal_cdf( m_sigmaerr ) 
            - ROOT::Math::normal_cdf( -1.0 * m_sigmaerr ) );
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::setConfidenceLevelError( Double_t confidence ) 
{
    Double_t alpha = (1.0 - confidence)/2.0;
    Double_t sigma = ROOT::Math::normal_quantile( 1.0 - alpha, 1.0 );
    this->setSigmaLevelError( sigma );
}



//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::plotGraph( const TString& pathname, 
        const TString& xtitle, Color_t grColor,
        bool visErrs, Color_t lineColor, 
        bool withSpline, Color_t colour, Style_t style )
{
    // Set the name of the graoh and spline
    TString n = GetName();
    if( n == "" ) {
        n = "Fitter1DLikeProfile";
    }

    // remove path.
    Int_t slash = pathname.Last('/');
    TString path = pathname( 0, slash );
    if( path != "" ) {  path += "/"; }
    TString name = pathname( slash + 1, pathname.Length() - slash - 1 );
    if( name != "" ) {
        n = path + name + "_" + n;
    } else {
        n = path + n;
    }

    TString mode( "#Delta ln(L)" );
    TString type = "dllhood";
    if( m_isLikelihood ) {
        type = "lhood";
        mode = "L";
    }

    n = n + "_" + type;

    // Annoyingly we need to open a file to
    TString dummyFileName = "dummy_file_121.root";
    TFile *f = TFile::Open( dummyFileName, "RECREATE" );
    if ( !f || f->IsZombie() ) {
        throw IOFailure("Fitter1DLikeProfile::plotGraph", 
                dummyFileName.Data(), "READ" );
    }
    f->cd();

    TCanvas* canvas = 0;
    try {
        canvas = new TCanvas( "Graph", "Graph" );
    }
    catch ( std::bad_alloc& e ) {
        throw GeneralException( "Fitter1DLikeProfile::plotGraph",
                "Got std::bad_alloc when creating new TCanvas" );
    } 

    gStyle->SetOptStat(0);

    // Draw the stuff then.
    m_graph.SetLineColor( grColor );
    m_graph.Draw("AC");
    if( withSpline ) {
        m_spline.SetLineColor( colour );
        m_spline.SetLineStyle( style );
        m_spline.Draw("CSAME");
    }
    canvas->Draw();

    m_graph.GetYaxis()->SetTitle( mode );
    m_graph.GetXaxis()->SetTitle( xtitle );

    // If requested add the TLines for the errors.
    TLine *Yline_cutoff=0, *Yline_min=0, *Yline_max=0;
    if( !m_isLikelihood && visErrs ) {
        // we wish to visualise the error bands
        Double_t x1 = m_graph.GetXaxis()->GetXmin();
        Double_t x2 = m_graph.GetXaxis()->GetXmax();

        Double_t xcont_max = m_minimum + this->getErrorHiLow().first ;
        Double_t xcont_min = m_minimum + this->getErrorHiLow().second ;
        Double_t Yat_Xmax = 0.5 * ROOT::Math::chisquared_quantile(
                this->getConfidenceLevelError(), 
                1 ); // 1 degree freedom.

        try {
            Yline_cutoff = new TLine(x1,Yat_Xmax,x2,Yat_Xmax);
            Yline_min = new TLine(xcont_min,0.,xcont_min,Yat_Xmax);
            Yline_max = new TLine(xcont_max,0.,xcont_max,Yat_Xmax);
        }
        catch ( std::bad_alloc& e ) {
            throw GeneralException( "Fitter1DLikeProfile::plotGraph",
                    "Got std::bad_alloc when creating new TLine" );
        } 

        Yline_cutoff->SetLineColor( lineColor );
        Yline_cutoff->SetLineStyle( kDashed );
        Yline_min->SetLineColor( lineColor );
        Yline_max->SetLineColor( lineColor );

        Yline_cutoff->Draw();
        Yline_min->Draw();
        Yline_max->Draw();
    } 

    canvas->Update();

    canvas->SaveAs( n + ".pdf" );
    canvas->SaveAs( n + ".eps" );
    canvas->SaveAs( n + ".png" );
    canvas->SaveAs( n + ".C" );

    // clean up
    delete canvas; canvas=0;
    f->Close();
    delete f; f=0;

    // cleanup error band lines
    if( !m_isLikelihood && visErrs ) {
        delete Yline_cutoff; Yline_cutoff=0;
        delete Yline_min; Yline_min=0;
        delete Yline_max; Yline_max=0;
    }

    // remove the temp datafile permanently.
    TSystemFile sysFile( dummyFileName, "." );
    sysFile.Delete();
}

//-----------------------------------------------------------------------------
/// Returns the higher error at the default 1 sigma level.
Double_t Fitter1DLikeProfile::getErrorHi( Double_t sigma ) 
{
    this->setSigmaLevelError( sigma );
    return m_hilowerr.first; 
} 


//-----------------------------------------------------------------------------
/// Returns the lower error at the default 1 sigma level.
Double_t Fitter1DLikeProfile::getErrorLow( Double_t sigma ) 
{ 
    this->setSigmaLevelError( sigma );
    return m_hilowerr.second; 
} 


//-----------------------------------------------------------------------------
std::pair<Double_t,Double_t> Fitter1DLikeProfile::getErrorHiLow( Double_t sigma ) 
{ 
    this->setSigmaLevelError( sigma );
    return m_hilowerr;
}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile Fitter1DLikeProfile::operator-=(
        const Fitter1DLikeProfile& rhs)
{
    // check that both likelihood profiles are in the same format.
    if( rhs.isLikelihood() ) {
        throw GeneralException( "Fitter1DLikeProfile::operator-=()",
                "Passed in likelihoo profile needs to be a [-log(likelihood)]."\
                " transformation. Use rhs.convertToLogLikelihood();");
    }

    // need to convert the current profile to a dll for the addition.
    bool likelihood = m_isLikelihood;
    if( m_isLikelihood ) {
        this->convertToLogLikelihood();
    }

    // We dont need to check the points are the same as we would for a 
    // histogram. We have a TSpline for this very reason DERR!! Just 
    // check that the graphs are defined in the same range that is all
    // that needs to be done as the TSpline can go nuts outside of its
    // designed boundary. So trim the internal graph boundaries.
    if( !this->checkConsistency( *this, rhs ) ) {
        std::cerr << "WARNING: Fitter1DLikeProfile::operator-= - checkConsistency failure."\
            " Will change internal boundaries of graph before merging." << std::endl;
        this->setXmin( rhs.getXmin() );
        this->setXmax( rhs.getXmax() );
    }

    // check that the pruning didnt do anything crazy like remove the minimum.
    // Should probably do something else too but there you go...
    if( m_minimum < this->getXmin() 
            || m_minimum > this->getXmax() 
            || this->npoints() == 0 ) {
        std::stringstream msg;
        msg << "Merging two very different profiles. Something has gone wrong as" 
            << " minimum(" << m_minimum << ") is no longer in the graph range."; 
        throw GeneralException("Fitter1DLikeProfile::operator-=",
                msg.str() ) ;   
    }

    // Cheat to get access to the spline an this was we dynamic_cast to a 
    // TSpline* so it doesnt ever matter what the Fitter1DLikeProfile was 
    // created using a TSpline3, TSpline5, etc... We only need to use the
    // Eval function anyway.
    Fitter1DLikeProfile copyrhs = rhs;
    const TSpline* spline2 = copyrhs.pSpline();

    Double_t x(0), y(0);
    for ( Int_t i(0); i < m_graph.GetN(); ++i ) {
        m_graph.GetPoint( i, x, y );
        y -= spline2->Eval( x );
        m_graph.SetPoint( i, x, y );
    }

    // create spline
    this->createSpline();

    // find minimum x-value
    this->findMinimum();

    // find the error bands
    this->calcErrors();

    // Finally revert to original state.
    if( likelihood ) {
        this->convertToLikelihood();
    }
    return *this;
}


//-----------------------------------------------------------------------------
std::vector<Double_t> Fitter1DLikeProfile::y() const
{
    std::vector<Double_t> y;
    y.assign( m_graph.GetY(), m_graph.GetY() + m_graph.GetN() );  
    return y;
}


//-----------------------------------------------------------------------------
std::vector<Double_t> Fitter1DLikeProfile::x() const 
{
    std::vector<Double_t> x;
    x.assign( m_graph.GetX(), m_graph.GetX() + m_graph.GetN() );  
    return x;
}


//-----------------------------------------------------------------------------
Bool_t Fitter1DLikeProfile::checkConsistency( const Fitter1DLikeProfile& lhs, 
        const Fitter1DLikeProfile& rhs ) 
{
    return ( rhs.getXmin() > lhs.getXmin() && rhs.getXmax() < lhs.getXmax() );
}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile Fitter1DLikeProfile::operator+=(
        const Fitter1DLikeProfile& rhs)
{
    // check that both likelihood profiles are in the same format.
    if( rhs.isLikelihood() ) {
        throw GeneralException( "Fitter1DLikeProfile::operator+=()",
                "Passed in likelihoo profile needs to be a [-log(likelihood)]."\
                " transformation. Use rhs.convertToLogLikelihood();");
    }

    // need to convert the current profile to a dll for the addition.
    bool likelihood = m_isLikelihood;
    if( m_isLikelihood ) {
        this->convertToLogLikelihood();
    }

    // We dont need to check the points are the same as we would for a 
    // histogram. We have a TSpline for this very reason DERR!! Just 
    // check that the graphs are defined in the same range that is all
    // that needs to be done as the TSpline can go nuts outside of its
    // designed boundary. So trim the internal graph boundaries.

    if( !this->checkConsistency( *this, rhs ) ) {
        std::cerr << "WARNING: Fitter1DLikeProfile::operator+= - checkConsistency failure."\
            " Will change internal boundaries of graph before merging." << std::endl;
        this->setXmin( rhs.getXmin() );
        this->setXmax( rhs.getXmax() );
    }

    // check that the pruning didnt do anything crazy like remove the minimum.
    // Should probably do something else too but there you go...
    if( m_minimum < this->getXmin() 
            || m_minimum > this->getXmax() 
            || this->npoints() == 0 ) {
        std::stringstream msg;
        msg << "Merging two very different profiles. Something has gone wrong as" 
            << " minimum(" << m_minimum << ") is no longer in the graph range."; 
        throw GeneralException("Fitter1DLikeProfile::operator+=",
                msg.str() ) ;   
    }

    // Cheat to get access to the spline an this was we dynamic_cast to a 
    // TSpline* so it doesnt ever matter what the Fitter1DLikeProfile was 
    // created using a TSpline3, TSpline5, etc... We only need to use the
    // Eval function anyway.
    Fitter1DLikeProfile copyrhs = rhs;
    const TSpline* spline2 = copyrhs.pSpline();

    Double_t x(0), y(0);
    for ( Int_t i(0); i < m_graph.GetN(); ++i ) {
        m_graph.GetPoint( i, x, y );
        y += spline2->Eval( x );
        m_graph.SetPoint( i, x, y );
    }

    // create spline
    this->createSpline();

    // find minimum x-value
    this->findMinimum();

    // find the error bands
    this->calcErrors();

    // Finally revert to original state.
    if( likelihood ) {
        this->convertToLikelihood();
    }
    return *this;
}


//-----------------------------------------------------------------------------
Fitter1DLikeProfile& Fitter1DLikeProfile::operator=(const Fitter1DLikeProfile& other)
{
    if ( this != &other ) // Ignore attepmts at self-assignment
    {
        TNamed::operator=(other);
        m_spline = other.spline();
        m_isLikelihood = other.isLikelihood();
        m_minimum = other.minimum();
        m_hilowerr = other.getErrorHiLow();
        m_sigmaerr = other.getSigmaLevelError();
        //        this->createSpline();
    }
    return *this;
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::setGraph( const TGraph& graph ) 
{
    // set the new graph then update the internals.
    m_graph = graph;

    m_graph.Sort();

    // Create the approximation to the likelihood
    this->createSpline();

    // find minimum x-value
    this->findMinimum();

    // find the error bands
    this->calcErrors();
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::setSigmaLevelError( double sigma ) 
{
    m_sigmaerr = sigma;
    this->calcErrors();
}


//-----------------------------------------------------------------------------
Fast::Fitter1DLikeProfile Fast::operator+(const Fast::Fitter1DLikeProfile& lhs, 
        const Fast::Fitter1DLikeProfile& rhs)
{
    Fast::Fitter1DLikeProfile temp(lhs);
    temp += rhs;
    return temp;
}


//-----------------------------------------------------------------------------
Fast::Fitter1DLikeProfile Fast::operator-(const Fast::Fitter1DLikeProfile& lhs, 
        const Fast::Fitter1DLikeProfile& rhs)
{
    Fast::Fitter1DLikeProfile temp(lhs);
    temp -= rhs;
    return temp;
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfile::setGlobalKillBelowError( RooFit::MsgLevel msg ) {

    RooMsgService::instance().setGlobalKillBelow( msg );
}
