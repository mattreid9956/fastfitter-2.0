#ifndef FAST_CLIENTTREE_H 
#define FAST_CLIENTTREE_H 1

// STL include
#include <map>
//#include <unordered_map>
#include <map>

// RooFit include
#include "RooAbsArg.h"

// Local include
#include "Fitter.hpp"

/** @class ClientTree ClientTree.hpp Fitter/ClientTree.hpp
 *  
 *
 *  @author Matthew M Reid
 *  @date   2014-01-08
 */

namespace Fast {

    class ClientTree {
        
        public: 
    
            typedef std::map<std::string,std::pair<int,std::string> > Map;
            typedef std::map<std::string,std::pair<int,std::string> >::iterator MapIter;
            typedef std::map<std::string,std::pair<int,std::string> >::const_iterator ConstMapIter;
            /// Standard constructor
            ClientTree( );

            // Constructor with pointer to RooAbsArg
            ClientTree( RooAbsArg* thearg,
                    std::vector<std::string> oldobjects );


            //=============================================================================
            //recursively places clients of the "var" given in m_TreeMap
            //goes all the way up the tree until the RooAbsArg has no more clients.
            //=============================================================================
            void recursiveClientCheck(RooAbsArg* var, 
                    std::vector<std::string> oldobjects,
                    int currentdisp=0);

            //=============================================================================
            //looks in the map and removes servers of the clients that are not amongst the 
            //keys of the map and are therefore not affected by the change in dependency on
            //the new var
            //=============================================================================
            void removeOrphanServers();

            //=============================================================================
            //iterates over the tree of clients defined in the map altering all the 
            //dependencies in turn such that dependency is transferred from var to newvar 
            //and is propagated all the way up the tree
            //returns a vector of strings to use as factory commands in the RooWorkspace
            //=============================================================================
            std::vector<std::string> clientReplace(RooAbsArg* var,
                    RooAbsArg* newvar,
                    Fitter& rsf);

            //=============================================================================
            //The versions of the last 3 functions that use TStrings internally.
            //=============================================================================
            void recursiveClientCheckTS(RooAbsArg* var, int currentdisp=0);
            void removeOrphanServersTS();
            std::vector<std::string> clientReplaceTS(RooAbsArg* var,
                    RooAbsArg* newvar);


            //=============================================================================
            // Get the vector of modified object names.
            //=============================================================================
            std::vector<std::string> getModifiedObjectNames();


            //=============================================================================
            // Split a string by some delimiter.
            //=============================================================================
            void split( std::vector<std::string>& splitstring, std::string stringvalue, std::string c );
            
           
            //=============================================================================
            // remove all right-side trailing characters labelled c
            //=============================================================================
            void trim_right( std::string& stringvalue, std::string c );

            
            //=============================================================================
            // remove all left-side leading characters labelled c
            //=============================================================================
            void trim_left( std::string& stringvalue, std::string c );
            
            virtual ~ClientTree( ); ///< Destructor

        protected:

            // A map containing names of clients in the tree (keys)
            // and pairs of ints and strings
            // the int is the displacement of the client from the 
            // variable which this is a ClientTree of.
            // std::string is a comma separated list of servers of the 
            // client.
            Map m_TreeMap;

            // Names of objects that this ClientTree has modified, i.e. 
            // old objects to put in trash...
            std::vector<std::string> m_ModObjects;

        private:

            /// Print the client tree map.
            void printTreeMap( const ClientTree::Map& treeMap );

    };


}

/// Compare two strings.
bool compareSubComponents( std::string l, std::string r );

#endif // FAST_CLIENTTREE_H
