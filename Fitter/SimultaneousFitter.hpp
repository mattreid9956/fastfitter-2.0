// $Id: $
#ifndef FAST_SIMULTANEOUSFITTER_H 
#define FAST_SIMULTANEOUSFITTER_H 1

// STL include
#include <string>
#include <map>

// Local include
#include "ModelBase.hpp"


// forward declarations
class TTree;

/** @class Simultaneousfitter SimultaneousFitter/SimultaneousFitter.hpp
 *  
 *
 *  @author Matthew M Reid
 *  @date   2014-01-08
 */

/* Class to extend the functionality of RooPhysFitter to provide helper functions
   to create a model and unbinned data set for a single descriminating variable. 
   In principle, the variable can be any physical parameter.
 */

namespace Fast {

    class SimultaneousFitter : public ModelBase {

        public:

            typedef Double_t(*DoubleFun)(Double_t a);
    	    // the following is a dummy variable that seems to be necessary for rootcint
            // to understand the DoubleFun typedef.
		    DoubleFun m_currentFunc;


            //=============================================================================
            /// Standard constructor
            SimultaneousFitter( ); 


            //=============================================================================
            // Constructor with name and title.
            SimultaneousFitter( const char* name, const char* title);


            //=============================================================================
            /// Virtual destructor.
            virtual ~SimultaneousFitter( ); ///< Destructor
            
            
            //=============================================================================
            /** combineDataSets - combines all datasets already in the workspace into one 
             * with a RooCategory as an index that has the names of the individual 
             * datasets as types. name and title are of the new combined dataset.
            */
            virtual void combineDataSets();    


            //=============================================================================
            /** importPDFFromFile - Import any RooAbsPdf from a file and put it in the 
             * workspace, can be be saved within a RooWorkspace in the input file, in which
             * case specify the name in the input parameter. Optionally set all parameters
             * of this pdf constant with setConstant flag.
             @param filename the root file name.
             @param pdfname the name of the pdf to import.
             @param newname specify a new name for the pdf after importing.
             @param wsname the name
            */
            virtual void importPDFFromFile(const char* filename, const char* pdfname,
                    const char* newname, const char* wsname = "", 
                    Bool_t setConstant = kFALSE );


            //=============================================================================
            /** exportAllPDFsToFile - Export all pdfs in the current fitter workspace to a 
             * workspace in a file.
             @param filename root file to export to.
             @param wsname specify a RooWorkspace name to export pdf's to.
             */ 
            virtual void exportAllPDFsToFile(const char* filename, const char* wsname);


            //=============================================================================
            /** importVarFromFile - Import any RooRealvar from a file and put it in the 
             * workspace, must be saved within a RooWorkspace in in the input file.
             @param fileName root file to import from.
             @param varName RooRealVar name.
             @param newName rename the variable.
             @param wsName the name of the workspace in the import file.
            */
            virtual void importVarFromFile(const char* fileName, const char* varName,
                    const char* newName, const char* wsName = "");


            //=============================================================================
            /** importVarValueFromFile - Get value of a RooRealvar from a file  within the
             * RooWorkspace.
             @param fileName root file to import from.
             @param varName RooRealVar name.
             @param newName rename the variable.
             @param wsName the name of the workspace in the import file.
             */ 
            virtual std::vector<Double_t> importVarValueFromFile( const char* fileName, 
                    const char* varName, const char* wsName );

            
            //=============================================================================
            /** exportPDFToFile - Export a named pdf in the current fitter workspace to
             * another workspace in a file.
             @param filename root file to import from.
             @param wsname create or add to the RooWorkspace in the file.
             @param pdfname pdf name.
             @param newname rename the variable.
             */ 
            virtual void exportPDFToFile(const char* filename, const char* wsname,
                                         const char* pdfname, const char* newname = "");

            
            //=============================================================================
            /** createPDFSets - creates the necessary named sets in the workspace for each 
             * pdf category.
            */ 
            virtual void createPDFSets();

            
            //=============================================================================
            /** addPDFs - Adds the comma separate list of pdfs to the named set 
             * corresponding to that category.
             @param categoryName the name of the fit variable slice.
             @param pdfs comma-separated list of the pdf names to be add together.
             */
            virtual void addPDFs(const char* categoryName, const char* pdfs);

            
            //=============================================================================
            /** addYields - Add the variables to the workspace that will be used as yields
             * for the given category. Puts them in a named set.
             @param categoryName the name of the fit variable slice.
             */
            virtual void addYields(const char* categoryName);

            
            //=============================================================================
            /** buildAddPdfs - Picks up the relevant named sets of pdfs and yields in each 
             * category and builds RooAddPdfs out of them.
            */
            virtual void buildAddPdfs();

            
            //=============================================================================
            /** buildModel - Creates a RooSimultaneous object from all the RooAddPdfs that
             * are in the workspace and have names that correspond to those in the category.
            */ 
            virtual void buildModel();

            
            //=============================================================================
            /** calculateSWeights - calculate SWeights (Note that newName is only used when 
             * the RooDataSet is cloned). To use the current values for the fit parameters
             * (instead of loading a snapshot), specify an empty stringIf no newName is
             * specified, then the dataset with wrights will be called {origName}_withWeights,
             * where {origName} is the name of the input dataset.
             @param fitName the name of the fit as saved during performFit() method.
             @param sliceName the slice of the data to calculate the weights from.
            */
            virtual void calculateSWeights( const char* fitName,
                    const char* sliceName );


            //=============================================================================
            /** plotFitResults - Plot the observable with name 'name', showing the fit results
            * NB. The variable must be an observable in the model PDF
            *
            * If sumWErrors is true, then plot sum-of-weights-squared errors (default is
            * Poissonian errors). RooFit will force sum-of-weights-squared errors if the
            * dataset is weighted.
            *
            * excludedComponents is a comma separated list of pdf components on the slice
            * that are not to be plotted
            *
            * The function returns a RooPlot pointer. Note that the user is responsible
            * for deleting this object.
             @param name the name of the observable to be plotted.
             @param slicename the name of the slice of data to project and plot
             @param fitName the stored fit cached in performFit("fitName").
             @param excludeComponents do not plot this comma-separated list.
             @param sumW2Errors for plotting the sum-of-weights-squared errors.
            */
            virtual RooPlot* plotFitResults(const char* name,
                    const char* slicename,
                    const char* fitName,
                    const char* excludedComponents="",
                    Bool_t sumW2Errors=kTRUE
                    );


            //=============================================================================
            /** plotPrettyMassAndPull - Saves the plots of the mass distribution and the
             * pull distrubution  will also apply a title axis.
             @param saveName the output file name.
             @param xtitle x-axis title name.
             @param slicename the name of the slice of data to project and plot
             @param fitName the stored fit cached in performFit("fitName").
             @param scale normal or log scales can be defined. 
            */ 
            void plotPrettyMassAndPull( TString saveName, std::string xtitle, 
                    TString slicename, std::string fitName = "fitResults", 
                    std::string scale = "normal" ) ;


            //=============================================================================
            /** plotMassAndPull - Saves the plots of the mass distribution and the 
             * pull distrubution (e.g. MC).
             @param saveName the output file name.
             @param xtitle x-axis title name.
             @param slicename the name of the slice of data to project and plot
             @param fitName the stored fit cached in performFit("fitName").
             @param scale normal or log scales can be defined. 
            */ 
            virtual void plotMassAndPull( TString saveName, std::string xtitle, 
                    TString slicename, std::string fitName = "fitResults", 
                    std::string scale = "normal" ) ;


            //=============================================================================
            /** plotMassAndPull - Saves the plots of the mass distribution (e.g. data fit)
             @param saveName the output file name.
             @param xtitle x-axis title name.
             @param slicename the name of the slice of data to project and plot.
             @param fitName the stored fit cached in performFit("fitName").
             @param scale normal or log scales can be defined. 
            */ 
            virtual void plotMass( TString saveName, std::string xtitle, 
                    TString slicename, std::string fitName = "fitResults", 
                    std::string scale = "normal" ) ;


            //=============================================================================
            /** plotMassAndPull - Simple function to plot a variable with given weight from
             * given dataset. Written primarily to plot vars from sWeighted datasets. May be 
             * updated. NOT TESTED!
             @param varName the output file name.
             @param datasetName the name of the dataset to plot.
             @param wgtVarName the sWeight variable name.
             @param sumW2Errors for plotting the sum-of-weights-squared errors.
            */ 
            virtual RooPlot* makePlot(const char* varName,
                    const char* datasetName,
                    const char* wgtVarName="",
                    Bool_t sumW2Errors=kFALSE
                    );

            
            /// sumYields - Returns the total yield over all fits. Over-ride
            virtual double sumYields();


            /** plotFitPulls - Plots the pulls between the observable with name "name" 
              for the model PDF.
              NB. The variable must be an observable in the model PDF.
              If a "fitName" is given, then this fit snapshot be loaded,
              else the current fit values are used.
              The function returns a RooPlot pointer. Note that the user is 
              responsible for deleting this object.
              @param name observable name.
              @param slicename the name of the slice of data to project and plot.
              @param fitName the stored fit cached in performFit("fitName").
              @param pmsigma sets the limits of the pull range.
              @param pmline sets the +/- level for plotting a line.
             */
            virtual RooPlot* plotFitPulls(const char* name, const char* slicename,
                    const char* fitName="", double pmsigma = 5.0, double pmline = 2.0 );


            //=============================================================================
            /** getYield - Gets the yield of the given pdf in the range given
              @param name observable name.
              @param slicename the name of the slice of data to project and plot.
              @param fitName the stored fit cached in performFit("fitName").
            */
              virtual double getYield(const char* pdfname, const char* yieldname,
                    double start, double stop, Bool_t getError = kFALSE);



            //=============================================================================
            /** chi2FitMeasure - Returns the chi2 goodness of fit measure for a given slice
             * of data and the projected pdf
              @param name observable name.
              @param slicename the name of the slice of data to project and plot.
              @param fitName the stored fit cached in performFit("fitName").
            */
            double chi2FitMeasure( const char* name, const char* slicename,
                    const char* fitName );


            //=============================================================================
            /** probFitMeasure - Returns the probability goodness of fit measure for a given 
             * slice of data and the projected pdf. 
              @param name observable name.
              @param slicename the name of the slice of data to project and plot.
              @param fitName the stored fit cached in performFit("fitName").
            */
              double probFitMeasure( const char* name, const char* slicename,
                    const char* fitName );


            //=============================================================================
            /** doRooMCStudy - Does a simple RooMCStudy on the pdf. 
             * Sets seed for random generator (to enable many small independent toy studies
             * to be done in parallel).
             * Allows statistics to be scaled by an overall factor (multiplies all yields
             * by this number before performing the study.
             * Set binnedMode flag to kFALSE to do unbinned fits in all the toys, default
             * is binned.
              @param nSameples number of toys.
              @param fname output file name.
              @param seed initial seed for random number generator.
              @param nEvtPerSampleScaleFactor scales all yields by the factor.
            */
            virtual void doRooMCStudy(Int_t nSamples, const char* fname, UInt_t seed, 
                    Double_t nEvtPerSampleScaleFactor=1.0,
                    Bool_t binnedMode = kTRUE);



            //=============================================================================
            /** setNegativeYields - Set the option of allowing yields to fluctuate to 
             * negative values
             @param neg the boolean flag to turn on/off (true/false) negative yeilds.
            */
            virtual void setNegativeYields(bool neg);


        protected:

            //-----------------------------------------------------------------------------
            // Get the name of the data histogram in a RooPlot of the specified
            // fit variable.
            // This function is used to get the pull plot.
            // NB. You may need to be override this method in a derived class.
            //-----------------------------------------------------------------------------
            std::string getDataHistName( const char* slicename ) const ;

            //-----------------------------------------------------------------------------
            // Get the name of the model PDF curve in a RooPlot of the specified
            // fit variable.
            // This function is used to get the pull plot.
            // NB. You may need to be override this method in a derived class.
            //-----------------------------------------------------------------------------
            std::string getModelCurveName(const char* name) const ;


            //=============================================================================
            //flag to set negative freedom for the yields
            //defaults to true
            //=============================================================================
            bool m_NegYields;

        private:
            ClassDef(SimultaneousFitter,1);

    };

}
#endif // FAST_SIMULTANEOUSFITTER_H
