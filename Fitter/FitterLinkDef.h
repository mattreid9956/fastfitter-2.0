// Include files

/** @file B2JpsiKsHHSFitterLinkDef B2JpsiKsHHSFitterLinkDef.h
 *  
 *
 *  @author Matthew Reid
 *  @date   2013-08-29
 *
 */
//#include <boost/tokenizer.hpp>
//#include <boost/algorithm/string.hpp>
#include <map>
#include <utility>
#include <vector>
#include <list>
#include <TString.h>


#ifdef __CINT__

/************************************
 * Copyright(c) 2013, SFitter   *
 * All rights reserved.             *
 * Matthew M Reid                   *
************************************/

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
// Class definitions (ACliC)

#pragma link C++ namespace RooFit;

#pragma link C++ defined_in "ConvolutionBase.hpp";
#pragma link C++ defined_in "FitterBase.hpp";
#pragma link C++ defined_in "Fitter.hpp";
#pragma link C++ defined_in "ToyStudy.hpp";
#pragma link C++ defined_in "FitterPulls.hpp";
#pragma link C++ defined_in "FitterUpperLimit.hpp";
#pragma link C++ defined_in "Fitter1DLikeProfile.hpp";
#pragma link C++ defined_in "Fitter1DLikeProfileFFTConv.hpp";
#pragma link C++ defined_in "FitterLikesRatioPlot.hpp";
#pragma link C++ defined_in "ModelBase.hpp";
#pragma link C++ defined_in "SimultaneousFitter.hpp";
#pragma link C++ defined_in "ClientTree.hpp";
#pragma link C++ defined_in "LHCbStyle.hpp";
#pragma link C++ defined_in "string_tools.hpp";
//#pragma link C++ defined_in "MWorkspace.hpp";

#pragma link C++ namespace Fast;

#pragma link C++ class Fast::TDirectoryError;
#pragma link C++ class Fast::WSImportCodeFailure;
#pragma link C++ class Fast::WSImportFailure;
#pragma link C++ class Fast::WSRetrievalFailure;
#pragma link C++ class Fast::IOFailure;
#pragma link C++ class Fast::GeneralException;

#pragma link C++ class Splitter;

#pragma link C++ defined_in "RooCruijff.hpp";
#pragma link C++ defined_in "RooCruijffSimple.hpp";
#pragma link C++ defined_in "RooApollonios.hpp";
#pragma link C++ defined_in "RooAmorosoPdf.hpp";
#pragma link C++ defined_in "RooDataPdf.hpp";
#pragma link C++ defined_in "RooBifurCB.hpp";
#pragma link C++ defined_in "RooHypatia.hpp";
#pragma link C++ defined_in "RooHypatia2.hpp";
#pragma link C++ defined_in "RooGeneralisedHyperbolic.hpp";

#pragma link C++ enum Fast::Fitter::RandomMethod;
#pragma link C++ function Fast::operator+(const Fast::Fitter1DLikeProfile&, const Fast::Fitter1DLikeProfile& );
#pragma link C++ function Fast::operator-(const Fast::Fitter1DLikeProfile&, const Fast::Fitter1DLikeProfile& );

/*
#pragma link C++ defined_in "RooLLRatioVar.hpp";
#pragma link C++ defined_in "RooPearsonsChi2Var.hpp";
#pragma link C++ defined_in "RooYatesChi2Var.hpp";
#pragma link C++ defined_in "RooGaussianChi2Var.hpp";
*/

#endif // FITTER_FITTERLINKDEF_H
