#ifndef FAST_FITTER_1DLIKEPROFILE
#define FAST_FITTER_1DLIKEPROFILE 1

/***************************
 * Fitter1DLikeProfile       *
 *                         *
 * � Matthew M Reid        *
 ***************************/

// STL include
#include <vector>
#include <limits>
#include <utility>

// ROOT 
#include "Riostream.h"
#include "Rtypes.h"
#include "TString.h"
#include "TGraph.h"
#include "TNamed.h"
#include "TSpline.h"
#include "TH1.h"
#include "TMath.h"
#include "TColor.h"

// RooFit
#include "RooMsgService.h"
#include "RooGlobalFunc.h"

// Local

/** @class Fitter Fitter1DLikeProfile.hpp Fitter/Fitter1DLikeProfile.hpp

  Class to handle delta log likelihood profiles and manipulation. One must
  have a dll profile for the parameter of interest stored as a TGraph. This
  can be created using the Fitter::nllScan functions, e.g:

    std::vector<double> scanpoints;
    // ... fill scan points for the parameter of interest, obviously after fit!
    fitter.nllScan( "paramName", scanpoints, "fitResults", "nllScan.root" );
    // output of this will be a graph whose name takes the form 
    // "nll_scan_paramName". This can be then used as well as the file to read 
    // into the Fitter1DLikeProfile class.

    Fitter1DLikeProfile profile( "nllScan.root", "nll_scan_paramName" );
    profile.

  Such as applying a
  convolution with a systematic uncertainty, find the roots for the X sigma
  errors. Store the data in terms of the -Ln(likelihood) remember that!!

  ToDo -- Be good to add this for multi-dimensional analysis or at least 2d!!
  @author Matthew M Reid
  @date   2014-01-08
  */

namespace Fast {

    class Fitter1DLikeProfile : public TNamed {

        public:
            // use a qubic or quintic spline to smooth the graph.
            typedef TSpline5 SPLINETYPE;

            /// Default constructor.
            Fitter1DLikeProfile(); 

            /// Constructor to set the name and title of profile.
            Fitter1DLikeProfile( const char* name, const char* title );

            /// Constructur
            Fitter1DLikeProfile( const char* name, const char* title,
                    TString filename, TString graphname, 
                    Double_t sigma = 1.0 ); 

            /// Constructor to store a delta log(likelihood) graph.
            Fitter1DLikeProfile( const char* name, const char* title,
                    TGraph dllgraph, Double_t sigma = 1.0 ); 

            // Copy constructor
            Fitter1DLikeProfile( const Fitter1DLikeProfile& other );

            /// virtual destructor
            virtual ~Fitter1DLikeProfile();

            // Copy assignment operator
            Fitter1DLikeProfile& operator=( const Fitter1DLikeProfile& other );

            // Member operators to allow addition and subtraction of two
            // Fitter1DLikeProfile's.
            Fitter1DLikeProfile operator+=( const Fitter1DLikeProfile& rhs );
            Fitter1DLikeProfile operator-=( const Fitter1DLikeProfile& rhs );
            friend Fitter1DLikeProfile operator+( const Fitter1DLikeProfile& lhs, 
                    const Fitter1DLikeProfile& rhs );
            friend Fitter1DLikeProfile operator-( const Fitter1DLikeProfile& lhs, 
                    const Fitter1DLikeProfile& rhs );

            
            /** setGlobalKillBelowError - sets the error message verbosity.
             @param msg specifies the verbosity output level from RooFit.
             */
            virtual void setGlobalKillBelowError( RooFit::MsgLevel msg = RooFit::FATAL );


            /** loadProfile - load in the log likelihood profile scan.
            @param filename the filename.root to be read in
            @param graphname the name of the parameter of interest scan (name of TGraph).
            @param sigma sets the error sigma level to read off from dll.
            */
            virtual void loadProfile( const TString& filename, const TString& graphname );
            

            /** significance - determine significance where dll is evaluated at
            // the x position specified and Wilks theorem is applied.
            @param x the value at which to determine the significance, for yields
            the default value of 0 is used as its the difference is the signal
            and signal+background hypothesis.
            */
            virtual Double_t significance( Double_t x = 0.0 ) const ;

            /** evaluate - evaluates the currently stored dll or likelihood at 
             * the specified x position.
             @param x the value at which to evaluate the likelihood function.
             */
            virtual Double_t function( Double_t x ) const;

            /** derivative - evaluates the derivative of the currently stored 
             * dll or likelihood at the specified x position.
             @param x the value at which to evaluate the derivative of the
             likelihood function.
             */
            virtual Double_t derivative( Double_t x ) const;

            /** integrate - integrates the currently stored spline within the 
             * specified range.
             @param xmin the lower integral bound, defaults to min in TGraph.
             @param xmax the upper integral bound, defaults to max in TGraph.
             */
            virtual Double_t integrate( 
                    Double_t xmin = -std::numeric_limits<Double_t>::max(),
                    Double_t xmax = std::numeric_limits<Double_t>::max() )
                const;


            /** checkConsistency - tests whether two Fitter1DLikeProfiles can be
             * merged without changing the x-axis range. This is important as if
             * this is not the case, a call to the TSpline Eval function can lead to
             * undefined behaviour far outside the range it was created.
             @param lhs the profile we will merge with.
             @param rhs the test profile to see if its boundaries are in a suitable
             range to be merged efficiency and safely.
             */
            static Bool_t checkConsistency( const Fitter1DLikeProfile& lhs, 
                    const Fitter1DLikeProfile& rhs );

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //virtual double confidenceUpperLimit();

            /// Tranform the internal store to a likelihood function.
            virtual void convertToLikelihood();

            /// Inverse tranformation to get back delta log likelihood function.
            virtual void convertToLogLikelihood();

            /** systConvolution - applys a Gaussian N~(0,sigma_{syst}) with the 
             * likelihood profile. This uses a numerical convolution and works for large 
             * significances > 10 sigma. RooFFTConv can breakdown at this precision.
             @param syst_error the width of the Gaussian, it is the resolution by which we
             will smear the profile likelihood.
             @param nS sets the sigma interval in which the integral will be calculated
             for each point [default = 4].
             */
            void systConvolution( Double_t syst_error, TString type = "FFT" );

            /** setXmax - will remove any points that lie outside the specified upper 
             * limit.
             @param  max the upper range of the likelihood.
             */
            void setXmax( Double_t max = std::numeric_limits<Double_t>::max() ) ;

            /// returns the maximum x-range of the likelihood.
            Double_t getXmax( ) const ;

            
            /** setRange - will remove any points that lie outside the specified 
             * range [min, max].
             @param  min the upper range of the likelihood.
             @param  max the upper range of the likelihood.
             */
            void setRange( Double_t min = -std::numeric_limits<Double_t>::max(),
                    Double_t max = std::numeric_limits<Double_t>::max() ) ;


            /** setXmin - will remove any points that lie below the specified lower 
             * limit.
             @param  min the upper range of the likelihood.
             */
            void setXmin( Double_t min = -std::numeric_limits<Double_t>::max() ) ;

            /// returns the minimum x-range of the likelihood.
            Double_t getXmin( ) const ;


            /** plotGraph - saves name.{eps,png,pdf,C} versions of the graph.
              @param  name file name to store image.
              @param  xtitle the x-axis title name.
              @param withSpline boolean to specify whether to overlay the spline fit.
              @param  colour sets the plotted line color of the spline model.
              @param  style sets the plotted line style of the spline model.
             */
            virtual void plotGraph( const TString& pathname, 
                    const TString& xtitle, Color_t grColor = kBlue,
                    Bool_t visErrs = kTRUE, 
                    Color_t lineColor = kRed,
                    Bool_t withSpline = kFALSE, 
                    Color_t colour = kCyan, Style_t style = kDashed );


            /** save - will save the current profile to the filename specified with
             * the name specified. The file can be opened in recreate or update
             * modes. Objects written to file with format: "
             * {lhood/dllhood}_{name}_{GetName()}.{pdf,png,epx,C}
             @param filename name of the root file. 
             @param name sets the ROOT name of the graph "name",  as well as the 
             prefix for the spline which takes the form "name_spline". 
             */
            virtual void save( const TString& filename, const TString& name, 
                    Bool_t update = kFALSE );

            /** transformX - scales the x-coordinates with respect to the operation
             * specified [+,-,*,/]
             @param f value to be applied to the x-coordinate. 
             @param op operation to be applied to the x-coordinate. 
             */
            void transformX( Double_t f, std::string op );

            /** transformY - scales the y-coordinates with respect to the operation
             * specified [+,-,*,/]
             @param f value to be applied to the y-coordinate. 
             @param op operation to be applied to the y-coordinate. 
             */
            void transformY( Double_t f, std::string op );                

            /** scaleY - scales the y-coordinates with respect to the factor 
             * provided.
             @param factor scale to mulitply the y-coordinates by. 
             */
            void scaleY( Double_t factor );

            /** scaleX - scales the x-coordinates with respect to the factor 
             * provided.
             @param factor scale to mulitply the x-coordinates by. 
             */
            void scaleX( Double_t factor );

            
            /// public setter methods
            /** setSigmaLevelError - sets the sigma level for which to read off asymmetric
             * error bands from the scanned likelihood function.
             @param sigma scale . 
             */
            void setSigmaLevelError( double sigma ); 
               
            /** setGraph - assign a nll scan graph to the Fitter1DLikeProfile
             * class.
             @param graph this is the TGraph created by the dll scan. 
             */
            void setGraph( const TGraph& graph ); 

            // public getter methods
            virtual std::vector<Double_t> x() const;
            virtual std::vector<Double_t> y() const;
            
            virtual TGraph* pGraph() { return dynamic_cast<TGraph*>( &m_graph ); }
            virtual TGraph graph() const { return m_graph; }
            virtual const TGraph& graph() { return m_graph; }
            virtual TSpline* pSpline() { return dynamic_cast<TSpline*>( &m_spline ); }
            virtual SPLINETYPE spline() const { return m_spline; }
            virtual const SPLINETYPE& spline() { return m_spline; }
            virtual Bool_t isLikelihood() const { return m_isLikelihood; }
            virtual Int_t npoints() const { return m_graph.GetN(); }
            virtual Double_t minimum() const { return m_minimum; } 
            virtual Double_t getSigmaLevelError() const { return m_sigmaerr; }


            /** getErrorHi - Returns the higher error at the default 1 sigma level.
             @param sigma numbers of "sigmas" used to determine the errors from dll scan.
             */
            virtual Double_t getErrorHi( Double_t sigma = 1.0 ) ;

            /** getErrorLow - Returns the lower error at the default 1 sigma level.
             @param sigma numbers of "sigmas" used to determine the errors from dll scan.
             */
            virtual Double_t getErrorLow( Double_t sigma = 1.0 ) ;

            /// getConfidenceLevelError get the current confidence level error.
            virtual Double_t getConfidenceLevelError() const ;

            /** setConfidenceLevelError - sets the internal .
             @param graph this is the TGraph created by the dll scan. 
             */
            virtual void setConfidenceLevelError( Double_t confidence ); 

            /// returns a pair storing the high and low error values
            virtual std::pair<Double_t,Double_t> getErrorHiLow() const { return m_hilowerr; }
        
            /** getErrorHighLow - returns a std::pair containing the asymmetric errors. The
             * upper(high) as the first element and the lower(low) error level as the second 
             * element in the std::pair.
             * @param sigma numbers of "sigmas" used to determine the errors from dll scan. 
             */
            virtual std::pair<Double_t,Double_t> getErrorHiLow( Double_t sigma ) ;

        protected:

            TGraph m_graph;
            SPLINETYPE m_spline;
            Bool_t m_isLikelihood; // flag to determine -dll or normal likelihood.
            Double_t m_minimum;
            std::pair< Double_t, Double_t > m_hilowerr;
            Double_t m_sigmaerr;


        private:
   
            static Double_t m_precision;

            /// find first approximation to the minimum.
            void findApproxMinimum();

            /// find minimum of the function
            void findMinimum();

            /// createSpline - creates the approximation to the graph data.
            void createSpline();

            /// minimisedfunction
            Double_t minimisefunction( const Double_t* xx ) ;

            /// Do the convolution
            //double convolutionPoint( Double_t p, Double_t sigma, 
            //        Int_t min, Int_t max );
            void renormalise();
            //void convolution( Double_t sigma, Int_t nS = 4 );

            /** getErrors - reads off the asymetric errors for the given sigma
             * level of the delta log likelikelihood using Wilkes theorem.
             @param sigma - used to find the errors at the specified level.
             */
            virtual void calcErrors( );

            ClassDef( Fitter1DLikeProfile, 1 ); // 1DLikeProfile Class	
    };

    // Free operators
    Fitter1DLikeProfile operator+(const Fitter1DLikeProfile& lhs, const Fitter1DLikeProfile& rhs);
    Fitter1DLikeProfile operator-(const Fitter1DLikeProfile& lhs, const Fitter1DLikeProfile& rhs);

    // Unary minus (negate a plot)
    //Fitter1DLikeProfile operator-(const Fitter1DLikeProfile& vec);

}

#endif // FAST_FITTER_1DLIKEPROFILE

