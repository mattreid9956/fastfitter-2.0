#ifndef FAST_FITTER_1DLIKEPROFILEFFTCONV
#define FAST_FITTER_1DLIKEPROFILEFFTCONV 

/***************************
 * Fitter1DLikeProfileFFTConv       *
 *                         *
 * © Matthew M Reid        *
 ***************************/

// STL include
#include <iosfwd>

// ROOT 
#include "TROOT.h"
#include "TGraph.h"
#include "Rtypes.h"

// RooFit
#include "RooDataSet.h"
#include "RooRealVar.h"

// local
#include "RooDataPdf.hpp"
#include "ConvolutionBase.hpp"
#include "Fitter1DLikeProfile.hpp"

/** @class Fitter Fitter1DLikeProfileFFTConv.hpp Fitter/Fitter1DLikeProfileFFTConv.hpp

  Class to apply a convolution of a likelihood profile with a Gaussian distributed 
  with N~(0,\sigma_{syst}), where \sigma_{syst} is the systematic uncertainty, 

  @author Matthew M Reid
  @date   2014-01-08
  */

namespace Fast {

    class Fitter1DLikeProfileFFTConv : public ConvolutionBase {

        public:
            /// Constructor to store a delta log(likelihood) graph.
            explicit Fitter1DLikeProfileFFTConv( const Fitter1DLikeProfile& profile, 
                    Int_t fftBins = 100000 ); 

            // Copy constructor
            //Fitter1DLikeProfileFFTConv(const Fitter1DLikeProfileFFTConv& other);

            virtual ~Fitter1DLikeProfileFFTConv();


            /// getter functions.
            /// getFFTBins returns the number of FFT bin cache being used.
            virtual Int_t getFFTBins() const;

            /// getNormalisation retun the normalisation constant.
            virtual Double_t getNormalisation() const;

            /** setFFTBins
             @param fftBins the number of cached bins to use for the fourier transform.
             */
            virtual void setFFTBins( Int_t fftbins );

            /** setNormalisation
             @param norm the normalisation used to plot the convolved pdf.
             */
            virtual void setNormalisation( Double_t norm );

            /** convolution - derived implementation. The specified x position.
             @param syst_err the smearing systematic error to apply to the 
             likelihood function.
             */
            virtual TGraph convolution( Double_t syst_err ) const ;

        protected:

        private:
            RooRealVar m_x, m_y;
            RooDataSet m_data;
            mutable RooDataPdf m_datapdf;
            Int_t m_fftBins ;
            Double_t m_normalisation;

            ClassDef( Fitter1DLikeProfileFFTConv, 1 ); // 1DLikeProfile Class	
    };
}

#endif // FAST_FITTER_1DLIKEPROFILEFFTCONV


