/******************************
 * Fitter1DLikeProfileFFTConv *
 *****************************/

// STL includes
#include <algorithm>    // std::min_element, std::max_element

// ROOT
#include "TCanvas.h"
#include "TSystemFile.h"
#include "TFile.h"

// RooFit
#include "RooFFTConvPdf.h"
#include "RooGaussian.h"
#include "RooPlot.h"
#include "RooCurve.h"
#include "RooArgSet.h"

// local include
#include "Fitter1DLikeProfileFFTConv.hpp"
#include "Exceptions.hpp"

using namespace Fast;
using Fast::GeneralException;


//-----------------------------------------------------------------------------
Fitter1DLikeProfileFFTConv::Fitter1DLikeProfileFFTConv(
        const Fitter1DLikeProfile& profile, Int_t fftBins )
    : m_x("x", "", 0 ), 
    m_y("y", "", 0 ), 
    m_data( "xy_data", "", RooArgSet( m_x, m_y ) ),
    m_fftBins( fftBins ),
    m_normalisation(0.)
{
    // set the fit range 
    m_x.setMin( profile.graph().GetX()[0] );
    m_x.setMax( profile.graph().GetX()[ profile.npoints()-1 ] );

    //RooFit::StoreError( RooArgSet( x, y ) ) );
    //RooFit::StoreAsymError( RooArgSet( y ) ),

    // Fill the dataset
    for ( int i = 0; i < profile.npoints(); ++i ) {
        m_x.setVal( profile.graph().GetX()[i] );
        //x.setError( 1.0 );
        m_y.setVal( profile.graph().GetY()[i] );
        //y.setError( TMath::Sqrt( y.getVal() ) );
        //y.setAsymError( - profile.graph().GetEYlow()[i], profile.graph().GetEYhigh()[i] );
        m_data.add( RooArgSet( m_x, m_y ), 1, 0 );
    }

    m_datapdf = RooDataPdf( "datapdf", "datapdf", m_x, profile );

    // the integral under the likelihood.
    m_normalisation = profile.integrate();

    m_x.setBins( m_fftBins, "cache" );
}


//-----------------------------------------------------------------------------
Fitter1DLikeProfileFFTConv::~Fitter1DLikeProfileFFTConv() 
{}


//-----------------------------------------------------------------------------
Int_t Fitter1DLikeProfileFFTConv::getFFTBins() const
{
    return m_fftBins;
}


//-----------------------------------------------------------------------------
Double_t Fitter1DLikeProfileFFTConv::getNormalisation() const
{
    return m_normalisation;
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfileFFTConv::setFFTBins( Int_t fftBins )
{
    m_fftBins = fftBins;
    m_x.setBins( m_fftBins, "cache" );
}


//-----------------------------------------------------------------------------
void Fitter1DLikeProfileFFTConv::setNormalisation( Double_t norm )
{
    m_normalisation = norm;
}


//-----------------------------------------------------------------------------
TGraph Fitter1DLikeProfileFFTConv::convolution( Double_t syst_err ) const
{
    RooRealVar x = m_x;
    // create the smearing Gaussian.
    RooRealVar mean( "mean", "", 0.0 );
    RooRealVar sigma( "sigma","", syst_err );
    RooGaussian gaussPDF( "gaussPDF", "", x, mean, sigma );

    // Create the convolution pdf.
    RooFFTConvPdf convPDF( "convPDF", "", x, m_datapdf, gaussPDF );
    convPDF.setBufferStrategy( RooFFTConvPdf::Extend );
    convPDF.setBufferFraction( 0.25 );
    // Annoyingly we need to open a file to
    TString dummyFileName = "dummyfile_121.root";
    TFile *f = TFile::Open( dummyFileName, "RECREATE" );
    if ( !f || f->IsZombie() ) {
        throw IOFailure("Fitter1DLikeProfileFFTConv::convolution", 
                dummyFileName.Data(), "READ" );
    }
    f->cd();

    TCanvas* canvas = 0;
    try {
        canvas = new TCanvas( "Graph", "Graph" );
    }
    catch ( std::bad_alloc& e ) {
        throw GeneralException( "Fitter1DLikeProfile::plotGraph",
                "Got std::bad_alloc when creating new TCanvas" );
    }

    RooPlot* frame = m_x.frame( RooFit::Title("") );

    // For normalisation purposes.
    m_data.plotOnXY( frame, RooFit::YVar( m_y ), 
            RooFit::Name("data"), RooFit::Invisible() );

    // Plot the convolved pdf at the relative normalisation.
    convPDF.plotOn( frame, RooFit::LineColor( kBlue ),
            RooFit::Normalization( m_normalisation ), 
            RooFit::Name( "convolved_pdf" ) 
            );

    frame->Draw();
    canvas->Draw();

    // get the new curve
    RooCurve* curve = frame->getCurve("convolved_pdf");

    Int_t points = m_data.sumEntries();

    // create the temp graph.
    TGraph convGraph( points );
    
    double xval(0.);
    for (Int_t i(0); i < points; ++i ) {
        const RooArgSet* row = m_data.get( i );
        xval = row->getRealValue("x");
        convGraph.SetPoint( i, xval, curve->Eval( xval ) ); 
    }

    // clean up afterwards.
    delete frame; frame = 0;
   // delete curve; curve = 0;
    
    // clean up
    delete canvas; canvas=0;
    f->Close();
    delete f; f=0; 
    
    // remove the temp datafile permanently.
    TSystemFile sysFile( dummyFileName, "." );
    sysFile.Delete();

    return convGraph;
}
