
/***************************
 * FitterPulls                *
 *                         *
 * � 2004 Thomas Latham    *
 *    and Nicole Chevalier *
 ***************************/

// STL includes
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>    // std::min_element, std::max_element

// ROOT
#include "TCanvas.h"
#include "TArrow.h"

// RooFit
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooGaussian.h"

// local include
#include "FitterPulls.hpp"


using namespace Fast;

FitterPulls::FitterPulls() : 
    m_nBins(20),
    m_nVars(0),
    m_dataRes(0),
    m_dataNll(0)
{ 
}

FitterPulls::FitterPulls(const RooArgList& floatList) : 
    m_nBins(20),
    m_nVars(0),
    m_dataRes(0),
    m_dataNll(0)
{ 
    this->createVectors(floatList);
}

FitterPulls::~FitterPulls()
{
    //if (m_dataRes) {
    //    delete m_dataRes; m_dataRes=0;
    //}
    std::vector<const RooFitResult*>::iterator iter = m_results.begin();
    const std::vector<const RooFitResult*>::const_iterator end = m_results.end();
    for (; iter != end; ++iter) {
        if ( *iter ){
            delete (*iter); *iter = 0;
        }
    }
}

void FitterPulls::createVectors(const RooArgList& floatList) 
{
    m_nVars = floatList.getSize();

    // get names of floating parameters
    for (Int_t i=0; i<m_nVars; ++i) {
        m_paramName.push_back( floatList[i].GetName() );
        std::vector<Double_t> dummyVect;
        m_pulls.push_back(dummyVect);
        m_values.push_back(dummyVect);
        m_residuals.push_back(dummyVect);
        m_SystI.push_back(dummyVect);
        m_SystII.push_back(dummyVect);
    }
}

void FitterPulls::fillVectors(const RooFitResult* fitres) 
{  
    const RooArgList& initList  = fitres->floatParsInit();
    const RooArgList& floatList = fitres->floatParsFinal();

    Int_t size = floatList.getSize();

    if (size==0) {
        std::cerr << "ERROR in FitterPulls::fillVectors : No floating parameters in fit." << std::endl;
        return;
    }

    if (m_nVars == 0) {
        this->createVectors(floatList);
    }

    if (m_nVars != size) {
        std::cerr << "ERROR in FitterPulls::fillVectors : nVars_ of unexpected size: " << m_nVars << "." << std::endl;
        return;
    }

    for (Int_t j=0; j<size; ++j) {

        RooRealVar& var( dynamic_cast<RooRealVar&>(floatList[j]) );
        RooRealVar& varInit( dynamic_cast<RooRealVar&>(initList[j]) );

        m_values[j].push_back( var.getVal() );
        // calculate the residual
        Double_t residual = var.getVal()-varInit.getVal();
        m_residuals[j].push_back( residual );

        // Add systematics type I values
        m_SystI[j].push_back( residual );

        // calculate the pull
        if (var.getAsymErrorHi() && var.getAsymErrorLo()){
            if (residual < 0) {
                m_pulls[j].push_back(residual/var.getAsymErrorHi());
            }  
            else {
                m_pulls[j].push_back(-residual/var.getAsymErrorLo());
            }
        }
        else if (var.getError())
        {
            m_pulls[j].push_back(residual/var.getError());
        }
        else {
            std::cerr << "ERROR in FitterPulls::fillVectors : All errors zero - cannot calculate pull." << std::endl;
            m_pulls[j].push_back(0);
        }
    }

    // also store the nll value,
    m_nlls.push_back(fitres->minNll());
    // the estimated distance to minimum,
    m_edm.push_back( fitres->edm() );
    // the fit status,
    m_status.push_back( fitres->status() );
    // and the MINUIT quality code of covariance matrix.
    m_covQual.push_back( fitres->covQual() );

}


void FitterPulls::fillVectors(const RooArgSet* genParams, 
        const RooFitResult* toyFitRes) 
{  
    const RooArgList& initList( *genParams );
    const RooArgList& floatList = toyFitRes->floatParsFinal();

    Int_t size = floatList.getSize();

    if (size==0) {
        std::cerr << "ERROR in FitterPulls::fillVectors : No floating parameters in fit."
            << std::endl;
        return;
    }

    if (m_nVars == 0) {
        this->createVectors(floatList);
    }

    if (m_nVars != size) {
        std::cerr << "ERROR in FitterPulls::fillVectors : nVars_ of unexpected size: "
            << m_nVars << "." << std::endl;
        return;
    }

    for (Int_t j=0; j<size; ++j) {

        RooRealVar& var( dynamic_cast<RooRealVar&>(floatList[j]) );
        RooRealVar& varInit( dynamic_cast<RooRealVar&>(initList[j]) );

        m_values[j].push_back( var.getVal() );
        // calculate the residual
        Double_t residual = var.getVal()-varInit.getVal();
        m_residuals[j].push_back( residual );

        // Add systematics type I values
        m_SystI[j].push_back( residual );

        // calculate the pull
        if (var.getAsymErrorHi() && var.getAsymErrorLo()){
            if (residual < 0) {
                m_pulls[j].push_back(residual/var.getAsymErrorHi());
            }  
            else {
                m_pulls[j].push_back(-residual/var.getAsymErrorLo());
            }
        }
        else if (var.getError())
        {
            m_pulls[j].push_back(residual/var.getError());
        }
        else {
            std::cerr << "ERROR in FitterPulls::fillVectors : All errors zero - cannot calculate pull." << std::endl;
            m_pulls[j].push_back(0);
        }
    }

    // also store the nll value,
    m_nlls.push_back( toyFitRes->minNll() );
    // the estimated distance to minimum,
    m_edm.push_back( toyFitRes->edm() );
    // the fit status,
    m_status.push_back( toyFitRes->status() );
    // and the MINUIT quality code of covariance matrix.
    m_covQual.push_back( toyFitRes->covQual() );

}


//==================================================//
//     Fill vectors for Systematics studies II      //
//==================================================//
void FitterPulls::createVectors_Syst(const RooArgList& floatList) 
{
    m_nVars = floatList.getSize();

    // get names of floating parameters
    for (Int_t i=0; i<m_nVars; ++i) {
        m_paramName.push_back( floatList[i].GetName() );
        std::vector<Double_t> dummyVect;
        m_SystII.push_back(dummyVect);
    }
}

void FitterPulls::fillVectors_Syst(const RooFitResult* fitres_official, const RooFitResult* fitres_toyMC) 
{  

    // Benchmark fit init/results
    //const RooArgList& initList_i  = fitres_official->floatParsInit();
    const RooArgList& floatList_i = fitres_official->floatParsFinal();
    // Toy MC fit init/results
    //const RooArgList& initList_f  = fitres_toyMC->floatParsInit();
    const RooArgList& floatList_f = fitres_toyMC->floatParsFinal();

    Int_t size = floatList_f.getSize();

    if (size==0) {
        std::cerr << "ERROR in FitterPulls::fillVectors : No floating parameters in fit." << std::endl;
        return;
    }

    if (m_nVars == 0) {
        this->createVectors_Syst(floatList_f);
    }

    if (m_nVars != size) {
        std::cerr << "ERROR in FitterPulls::fillVectors : m_nVars of unexpected size: " << m_nVars << "." << std::endl;
        return;
    }

    for (Int_t j=0; j<size; ++j) {

        RooRealVar& varFit_i( dynamic_cast<RooRealVar&>(floatList_i[j]) );
        RooRealVar& varFit_f( dynamic_cast<RooRealVar&>(floatList_f[j]) );

        // calculate the difference between fit results and Toy MC one
        Double_t diff_results = varFit_i.getVal()-varFit_f.getVal();
        // Add systematics type II values
        m_SystII[j].push_back( diff_results );

    }
}

void FitterPulls::writeVectors(TString fileName) const
{
    std::cout << "Writing pull results to " << fileName << "..." << std::flush;

    // create output stream
    std::ofstream outFile(fileName, std::ios::out);
    if (!outFile) {
        std::cerr << "ERROR in FitterPulls::writeVectors : Cannot open file \"" << fileName << "\"." << std::endl;
        std::exit(1);
    }

    for (Int_t i=0; i<m_nVars; ++i) {
        // write the variables name
        TString name = m_paramName[i];
        outFile << name << " ";

        // iterate through the pulls and write them to the stream
        std::vector<Double_t>::const_iterator iter = m_pulls[i].begin();
        const std::vector<Double_t>::const_iterator end = m_pulls[i].end();

        for (; iter != end; ++iter) {
            outFile << (*iter) << " ";
        }
        outFile << std::endl;
    }
    outFile.close();

    std::cout << " done." << std::endl;

}


void FitterPulls::plotPulls(TString filename, Int_t nCores, Double_t plotLimit, Double_t fitLimit ) const
{
    TCanvas* c1 = new TCanvas("c1","Pull Plots" );

    for (Int_t i=0; i<m_nVars; ++i) {

        std::cout << m_paramName[i] << std::endl;

        // create dataset to store the pulls
        TString title(m_paramName[i]);
        title += " Pull";
        RooRealVar pull( "pull", title, -fitLimit, fitLimit );
        RooDataSet pulls( "pulls", title, pull );

        // iterate through the pulls and add them into the dataset
        std::vector<Double_t>::const_iterator iter = m_pulls[i].begin();
        const std::vector<Double_t>::const_iterator end = m_pulls[i].end();
        for (; iter != end; ++iter) {
            if ((*iter) >= -fitLimit && (*iter) <= fitLimit) {
                pull.setVal(*iter);
                pulls.add(pull);
            }
        }

        // create a gaussian pdf
        RooRealVar mean("mean","Pull Gaussian Mean", 0.0, -fitLimit, fitLimit );
        RooRealVar sigma("sigma","Pull Gaussian Sigma",1.0,0.0,2.0*fitLimit );
        RooGaussian gauss("gauss","Pull Gaussian", pull, mean, sigma );

        // fit it to the pull data
        gauss.fitTo( pulls, RooFit::NumCPU( nCores ) );

        // make an empty plot
        RooPlot* plot = pull.frame(-plotLimit,plotLimit);
        plot->SetTitle(title);

        // plot the pulls and the gaussian
        pulls.plotOn( plot, RooFit::Binning(m_nBins) );
        gauss.plotOn( plot );
        gauss.paramOn( plot, RooFit::Parameters( RooArgSet(mean, sigma) ),
                RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.92 ) );

        // draw the plot
        c1->cd();
        plot->Draw();
        plot->SetMinimum(1e-1);

        // save canvas to file
        TString fileName( filename );
        fileName += "_";  
        fileName += m_paramName[i];
        c1->SaveAs( fileName + ".eps" );
        c1->SaveAs( fileName + ".pdf");
        c1->SaveAs( fileName + ".png");
        c1->SaveAs( fileName + ".C");
    }

    delete c1; c1=0;
}

void FitterPulls::plotNlls(TString filename, Int_t nCores) const
{
    // create a canvas
    TCanvas* canvas = new TCanvas("nlls","Minimum Negative Log Likelihood" );

    // check that there are nlls to plot
    if (!m_nlls.size())
    {
        std::cerr << "ERROR in FitterPulls::plotNlls : Vectors not yet filled." << std::endl;
        return;
    }

    // make the title
    TString title = "Minimum Negative Log Likelihood";

    // create the variable setting the extreme values.
    RooRealVar nll("nll", title, *std::min_element( m_nlls.begin(), m_nlls.end() ), *std::max_element( m_nlls.begin(), m_nlls.end() ) );

    // now create the empty dataset
    RooDataSet nlls("nlls", title, RooArgSet( nll ) );

    // iterate again through the nlls and add them into the dataset
    std::vector< Double_t >::const_iterator iter = m_nlls.begin();
    const std::vector< Double_t >::const_iterator enditer = m_nlls.end();
    for (; iter != enditer; ++iter) {
        nll.setVal( (*iter) );
        nlls.add( RooArgSet( nll ) );
    }

    // create a gaussian pdf
    RooRealVar mean( "mean", "Nll Gaussian Mean", nlls.meanVar( nll )->getVal(), nll.getMin(), nll.getMax() );
    RooRealVar sigma( "sigma", "Nll Gaussian Sigma", nlls.rmsVar( nll )->getVal(), 0.0, nll.getMax()-nll.getMin() );
    RooGaussian gauss( "gauss", "Nll Gaussian", nll, mean, sigma );

    // fit it to the nll data
    gauss.fitTo( nlls, RooFit::NumCPU( nCores ) );

    // make an empty plot
    RooPlot* plot = nll.frame();
    //plot->SetTitle("");
    // plot the nlls and the gaussian
    nlls.plotOn( plot, RooFit::Binning(m_nBins) );
    RooHist* hist = dynamic_cast<RooHist*>( plot->getObject(0) );
    Double_t arrowTop = 0.4 * hist->getYAxisMax();
    gauss.plotOn( plot );
    gauss.paramOn( plot, RooFit::Parameters( RooArgSet(mean, sigma) ),
            RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.92 ) );

    // add an arrow for the data value
    // (DON'T DELETE the arrow because the RooPlot destructor does it for you)
    TArrow *arrow(0);
    if (m_dataNll)
    {
        arrow = new TArrow( m_dataNll, arrowTop, m_dataNll, 0, 0.05, ">" ) ;
        arrow->SetFillColor(2);
        arrow->SetLineColor(2);
        arrow->SetLineWidth(2);
        arrow->SetFillStyle(1001);
        plot->addObject(arrow);
    }

    // draw the plot
    plot->Draw();
    plot->SetMinimum(1e-1);

    // save canvas to file
    canvas->SaveAs(filename + ".eps");
    canvas->SaveAs(filename + ".pdf");
    canvas->SaveAs(filename + ".png");
    canvas->SaveAs(filename + ".C");

    delete canvas; canvas = 0;
    if(arrow)
        delete arrow; 
    arrow = 0;
}


void FitterPulls::plotValues(TString filename, Int_t nCores) const
{
    // create a canvas
    TCanvas* canvas = new TCanvas( "values", "Minimum Negative Log Likelihood" );

    for (Int_t i=0; i<m_nVars; ++i) {

        std::cout << m_paramName[i] << std::endl;

        // create dataset to store the pulls
        TString title(m_paramName[i]);
        title += " Value";

        // check that there are nlls to plot
        if (!m_values.size())
        {
            std::cerr << "ERROR in FitterPulls::plotValues : Vectors not yet filled." << std::endl;
            return;
        }

        // make the title

        // create the variable setting the extreme values.
        RooRealVar value("value", title, *std::min_element( m_values[i].begin(), m_values[i].end() ), *std::max_element( m_values[i].begin(), m_values[i].end() ) );

        // now create the empty dataset
        RooDataSet values("values", title, RooArgSet( value ) );

        // iterate again through the nlls and add them into the dataset
        std::vector< Double_t >::const_iterator iter = m_values[i].begin();
        const std::vector< Double_t >::const_iterator enditer = m_values[i].end();
        for (; iter != enditer; ++iter) {
            value.setVal( (*iter) );
            values.add( RooArgSet( value ) );
        }

        // create a gaussian pdf
        RooRealVar mean( "mean", "Value Gaussian Mean", values.meanVar( value )->getVal(), value.getMin(), value.getMax() );
        RooRealVar sigma( "sigma", "Value Gaussian Sigma", values.rmsVar( value )->getVal(), 0.0, value.getMax() - value.getMin());
        RooGaussian gauss( "gauss", "Value Gaussian", value, mean, sigma );

        // fit it to the nll data
        gauss.fitTo( values, RooFit::NumCPU( nCores ) );

        // make an empty plot
        RooPlot* plot = value.frame();
        //plot->SetTitle("");
        // plot the nlls and the gaussian
        values.plotOn( plot, RooFit::Binning(m_nBins) );
        RooHist* hist = dynamic_cast<RooHist*>( plot->getObject(0) );
        Double_t arrowTop = 0.4 * hist->getYAxisMax();
        gauss.plotOn( plot );

        // add an arrow for the data value
        // (DON'T DELETE the arrow because the RooPlot destructor does it for you)
        TArrow *arrow(0);
        RooRealVar* var(0);
        //RooRealVar* measured(0);
        if (m_dataRes)
        {
            double v(0.);
            RooFIter paramIter = m_dataRes->floatParsFinal().fwdIterator();
            while ( (var = dynamic_cast<RooRealVar*>( paramIter.next() ) ) ) {
                std::string name = var->GetName();
                std::string classname = var->ClassName();
                if (classname!="RooRealVar") {
                    continue; 
                }
                if( TString( var->GetName() ) == m_paramName[i] ) {
                    v = var->getVal();
                    //measured = dynamic_cast<RooRealVar*>( var->Clone("meas") );
                    break;
                }
            }
            arrow = new TArrow( v, arrowTop, v, 0, 0.05, ">" ) ;
            arrow->SetFillColor(2);
            arrow->SetLineColor(2);
            arrow->SetLineWidth(2);
            arrow->SetFillStyle(1001);
            plot->addObject(arrow);
        }

        gauss.paramOn( plot, RooFit::Parameters( RooArgSet(  mean, sigma ) ),
                RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.92 ) );

        // It would be cool if we could just add the actual measured value numbers so central and error,
        // but I really cant be bothered so this measured variable is actually pointless right now :(
        //delete measured; measured = 0;
        // draw the plot
        canvas->cd();
        plot->Draw();
        plot->SetMinimum(1e-1);

        // save canvas to file
        TString fileName( filename );
        fileName += "_";  
        fileName += m_paramName[i];
        canvas->SaveAs( fileName + ".eps" );
        canvas->SaveAs( fileName + ".pdf");
        canvas->SaveAs( fileName + ".png");
        canvas->SaveAs( fileName + ".C");

        if(arrow)
            delete arrow; 
        arrow = 0;
        // draw the plot
    }

    delete canvas; canvas = 0;

}

void FitterPulls::plotResiduals( TString filename, Int_t nCores ) const
{
    // create a canvas
    TCanvas* canvas = new TCanvas( "residuals", "Minimum Negative Log Likelihood" );

    for (Int_t i=0; i<m_nVars; ++i) {

        std::cout << m_paramName[i] << std::endl;

        // create dataset to store the pulls
        TString title(m_paramName[i]);
        title += " Value";

        // check that there are nlls to plot
        if (!m_residuals.size())
        {
            std::cerr << "ERROR in FitterPulls::plotValues : Vectors not yet filled." << std::endl;
            return;
        }

        // make the title

        // create the variable setting the extreme values.
        RooRealVar value("value", title, *std::min_element( m_residuals[i].begin(), m_residuals[i].end() ), *std::max_element( m_residuals[i].begin(), m_residuals[i].end() ) );

        // now create the empty dataset
        RooDataSet values("values", title, RooArgSet( value ) );

        // iterate again through the nlls and add them into the dataset
        std::vector< Double_t >::const_iterator iter = m_residuals[i].begin();
        const std::vector< Double_t >::const_iterator enditer = m_residuals[i].end();
        for (; iter != enditer; ++iter) {
            value.setVal( (*iter) );
            values.add( RooArgSet( value ) );
        }

        // create a gaussian pdf
        RooRealVar mean( "mean", "Value Gaussian Mean", values.meanVar( value )->getVal(), value.getMin(), value.getMax() );
        RooRealVar sigma( "sigma", "Value Gaussian Sigma", values.rmsVar( value )->getVal(), 0.0, value.getMax() - value.getMin());
        RooGaussian gauss( "gauss", "Value Gaussian", value, mean, sigma );

        // fit it to the nll data
        gauss.fitTo( values, RooFit::NumCPU( nCores ) );

        // make an empty plot
        RooPlot* plot = value.frame();
        //plot->SetTitle("");
        // plot the nlls and the gaussian
        values.plotOn( plot, RooFit::Binning(m_nBins) );
        RooHist* hist = dynamic_cast<RooHist*>( plot->getObject(0) );
        Double_t arrowTop = 0.4 * hist->getYAxisMax();
        gauss.plotOn( plot );

        // add an arrow for the data value
        // (DON'T DELETE the arrow because the RooPlot destructor does it for you)
        TArrow *arrow(0);
        RooRealVar* var(0);
        //RooRealVar* measured(0);
        if (m_dataRes)
        {
            double v(0.);
            RooFIter paramIter = m_dataRes->floatParsFinal().fwdIterator();
            while ( (var = dynamic_cast<RooRealVar*>( paramIter.next() ) ) ) {
                std::string name = var->GetName();
                std::string classname = var->ClassName();
                if (classname!="RooRealVar") {
                    continue; 
                }
                if( TString( var->GetName() ) == m_paramName[i] ) {
                    v = var->getVal();
                    //measured = dynamic_cast<RooRealVar*>( var->Clone("meas") );
                    break;
                }
            }
            arrow = new TArrow( v, arrowTop, v, 0, 0.05, ">" ) ;
            arrow->SetFillColor(2);
            arrow->SetLineColor(2);
            arrow->SetLineWidth(2);
            arrow->SetFillStyle(1001);
            plot->addObject(arrow);
        }

        gauss.paramOn( plot, RooFit::Parameters( RooArgSet(  mean, sigma ) ),
                RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.92 ) );

        // It would be cool if we could just add the actual measured value numbers so central and error,
        // but I really cant be bothered so this measured variable is actually pointless right now :(
        //delete measured; measured = 0;
        // draw the plot
        canvas->cd();
        plot->Draw();
        plot->SetMinimum(1e-1);

        // save canvas to file
        TString fileName( m_paramName[i] );
        fileName += "_";  fileName += filename;
        canvas->SaveAs( fileName + ".eps" );
        canvas->SaveAs( fileName + ".pdf");
        canvas->SaveAs( fileName + ".png");
        canvas->SaveAs( fileName + ".C");

        if(arrow)
            delete arrow; 
        arrow = 0;
        // draw the plot
    }

    delete canvas; canvas = 0;

}


// plot information about the Systematics studies Type I
void FitterPulls::plotSystI(TString dir, std::string paramName, Int_t nCores) const
{
    // create a canvas
    TCanvas* canvas = new TCanvas( "values", "Systematics Type I" );

    for (Int_t i=0; i<m_nVars; ++i) {

        std::string listPar = (m_paramName[i]).Data();
        if (listPar.find("Yield")!=std::string::npos){
            std::cout << m_paramName[i] << std::endl;

            // create dataset to store the pulls
            TString title(m_paramName[i]);
            title += " Residual";

            // create the variable setting the extreme values.
            RooRealVar value("value", title, -0.5, 0.5);
            RooDataSet values("values", title, RooArgSet( value ) );

            // iterate again through the nlls and add them into the dataset
            std::vector< Double_t >::const_iterator iter = m_SystI[i].begin();
            const std::vector< Double_t >::const_iterator enditer = m_SystI[i].end();
            for (; iter != enditer; ++iter) {
                value.setVal( (*iter) );
                values.add( RooArgSet( value ) );
            }

            // create a gaussian pdf
            RooRealVar mean( "mean", "Value Gaussian Mean", 0.0, -0.5, 0.5);//values.meanVar( value )->getVal(), value.getMin(), value.getMax() );
            RooRealVar sigma( "sigma", "Value Gaussian Sigma", 1.0, 0.0, 2.0);//values.rmsVar( value )->getVal(), 0.0, value.getMax() - value.getMin());
            RooGaussian gauss( "gauss", "Value Gaussian", value, mean, sigma );

            // fit it to the nll data
            gauss.fitTo( values, RooFit::NumCPU( nCores ), RooFit::PrintLevel(-1), RooFit::PrintEvalErrors(-1) );

            // make an empty plot
            RooPlot* plot = value.frame();

            plot->SetTitleOffset(1.0);
            //plot->SetTitle("");
            // plot the nlls and the gaussian
            values.plotOn( plot, RooFit::Binning(m_nBins) );
            RooHist* hist = dynamic_cast<RooHist*>( plot->getObject(0) );
            Double_t arrowTop = 0.4 * hist->getYAxisMax();
            gauss.plotOn( plot );

            // add an arrow for the data value
            // (DON'T DELETE the arrow because the RooPlot destructor does it for you)
            TArrow *arrow(0);
            RooRealVar* var(0);
            //RooRealVar* measured(0);
            if (m_dataRes)
            {
                double v(0.);
                RooFIter paramIter = m_dataRes->floatParsFinal().fwdIterator();
                while ( (var = dynamic_cast<RooRealVar*>( paramIter.next() ) ) ) {
                    std::string name = var->GetName();
                    std::string classname = var->ClassName();
                    if (classname!="RooRealVar") {
                        continue; 
                    }
                    if( TString( var->GetName() ) == m_paramName[i] ) {
                        v = var->getVal();
                        //measured = dynamic_cast<RooRealVar*>( var->Clone("meas") );
                        break;
                    }
                }
                arrow = new TArrow( v, arrowTop, v, 0, 0.05, ">" ) ;
                arrow->SetFillColor(2);
                arrow->SetLineColor(2);
                arrow->SetLineWidth(2);
                arrow->SetFillStyle(1001);
                plot->addObject(arrow);
            }

            gauss.paramOn( plot, RooFit::Parameters( RooArgSet(  mean, sigma ) ),
                    RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.82 ) );

            // It would be cool if we could just add the actual measured value numbers so central and error,
            // but I really cant be bothered so this measured variable is actually pointless right now :(
            //delete measured; measured = 0;
            // draw the plot
            canvas->cd();
            plot->Draw();
            plot->SetMinimum(1e-1);

            // save canvas to file
            TString fileName( dir );
            fileName +=  m_paramName[i] ;
            fileName += "_";  
            fileName += "SystI";  
            fileName += "_";  
            fileName += paramName;  
            canvas->SaveAs( fileName + ".eps" );
            canvas->SaveAs( fileName + ".pdf");
            canvas->SaveAs( fileName + ".C");
            canvas->SaveAs( fileName + ".png");

            //if(arrow)
            //		delete arrow; 
            //arrow = 0;
            // draw the plot	

        }
    }

    delete canvas; canvas = 0;

}

// plot information about the Systematics studies Type II
void FitterPulls::plotSystII(TString dir, Int_t nCores) const
{
    // create a canvas
    TCanvas* canvas = new TCanvas( "values", "Systematics Type II" );

    for (Int_t i=0; i<m_nVars; ++i) {

        std::string listPar = (m_paramName[i]).Data();
        if (listPar.find("Yield")!=std::string::npos){
            std::cout << m_paramName[i] << std::endl;

            // create dataset to store the pulls
            TString title(m_paramName[i]);
            title += " Diff";

            // create the variable setting the extreme values.
            RooRealVar value("value", title, -20, 20);
            RooDataSet values("values", title, RooArgSet( value ) );

            // iterate again through the nlls and add them into the dataset
            std::vector< Double_t >::const_iterator iter = m_SystII[i].begin();
            const std::vector< Double_t >::const_iterator enditer = m_SystII[i].end();
            for (; iter != enditer; ++iter) {
                value.setVal( (*iter) );
                values.add( RooArgSet( value ) );
            }

            // create a gaussian pdf
            RooRealVar mean( "mean", "Value Gaussian Mean", 0.0, -10, 10);
            RooRealVar sigma( "sigma", "Value Gaussian Sigma", 1.0, 0.0, 10.0);
            RooGaussian gauss( "gauss", "Value Gaussian", value, mean, sigma );

            // fit it to the nll data
            gauss.fitTo( values, RooFit::NumCPU( nCores ), RooFit::PrintLevel(-1), RooFit::PrintEvalErrors(-1) );

            // make an empty plot
            RooPlot* plot = value.frame();

            plot->SetTitleOffset(1.0);
            //plot->SetTitle("");
            // plot the nlls and the gaussian
            values.plotOn( plot, RooFit::Binning(m_nBins) );
            RooHist* hist = dynamic_cast<RooHist*>( plot->getObject(0) );
            Double_t arrowTop = 0.4 * hist->getYAxisMax();
            gauss.plotOn( plot );

            // add an arrow for the data value
            // (DON'T DELETE the arrow because the RooPlot destructor does it for you)
            TArrow *arrow(0);
            RooRealVar* var(0);
            //RooRealVar* measured(0);
            if (m_dataRes)
            {
                double v(0.);
                RooFIter paramIter = m_dataRes->floatParsFinal().fwdIterator();
                while ( (var = dynamic_cast<RooRealVar*>( paramIter.next() ) ) ) {
                    std::string name = var->GetName();
                    std::string classname = var->ClassName();
                    if (classname!="RooRealVar") {
                        continue; 
                    }
                    if( TString( var->GetName() ) == m_paramName[i] ) {
                        v = var->getVal();
                        //measured = dynamic_cast<RooRealVar*>( var->Clone("meas") );
                        break;
                    }
                }
                arrow = new TArrow( v, arrowTop, v, 0, 0.05, ">" ) ;
                arrow->SetFillColor(2);
                arrow->SetLineColor(2);
                arrow->SetLineWidth(2);
                arrow->SetFillStyle(1001);
                plot->addObject(arrow);
            }

            gauss.paramOn( plot, RooFit::Parameters( RooArgSet(  mean, sigma ) ),
                    RooFit::Format( "NEU", RooFit::AutoPrecision( 2 ) ), RooFit::Layout( 0.59, 0.92, 0.82 ) );

            // It would be cool if we could just add the actual measured value numbers so central and error,
            // but I really cant be bothered so this measured variable is actually pointless right now :(
            //delete measured; measured = 0;
            // draw the plot
            canvas->cd();
            plot->Draw();
            plot->SetMinimum(1e-1);

            // save canvas to file
            TString fileName( dir );
            fileName +=  m_paramName[i] ;
            fileName += "_";  
            fileName += "SystII";  
            canvas->SaveAs( fileName + ".eps" );
            canvas->SaveAs( fileName + ".pdf");
            canvas->SaveAs( fileName + ".png");
            canvas->SaveAs( fileName + ".C");

            //if(arrow)
            //		delete arrow; 
            //arrow = 0;
            // draw the plot	

        }
    }
    delete canvas; canvas = 0;

}

