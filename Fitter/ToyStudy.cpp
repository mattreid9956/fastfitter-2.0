#include <iterator>     // std::distance


// Local include
#include "ToyStudy.hpp"
#include "string_tools.hpp"


// RooFit 
#include "RooLinkedListIter.h"
#include "RooRealVar.h"
#include "RooAbsReal.h"
#include "RooAbsArg.h"
#include "RooAbsCollection.h"
#include "RooRandom.h"
#include "RooMsgService.h"


// ROOT
#include "TGraph.h"
#include "TFile.h"
#include "TRandom3.h"
#include "TRegexp.h"


// TMVA used for timer
#include "TMVA/Timer.h"

using namespace Fast;


ToyStudy::ToyStudy( Fitter* fitter, Int_t nSamples, Int_t mSystematics, Int_t seed ) :
    m_stringHelper(), m_fitter(fitter),
    m_nSamples( nSamples ), m_mSystematics( mSystematics ),
    m_dataResult(0), m_result(0), m_seed(seed), m_datafitResultName("fitResults"),
    m_toyfitParamsName("toy_params"), m_systematicParamsName("systematic_params") 
{}


ToyStudy::~ToyStudy() 
{
    if( m_result ) {
        //delete m_result; m_result=0;
    }
    if( m_dataResult ) {
        //delete m_dataResult; m_dataResult=0;
    }
}

//=============================================================================
// MIGRAD is able to find the minimum of your parameter space, however it can 
// be subject to localised minima. A cross-check for this is to change the 
// initial values of the fit and perform the fit again. If the true global 
// minimum has been found then the function returns true.
//=============================================================================
bool ToyStudy::localMinimumChecker( const char* fitName, Int_t n,  
        Fitter::RandomMethod method, Double_t edmTol, Bool_t extendedMode, 
        Bool_t useMinos, Bool_t useSumW2Errors, 
        Bool_t printResults) { 

    // need to refit having set the initial parameters to some new values.
    // first need the original fit model
    RooFitResult* theresult = dynamic_cast<RooFitResult*>( m_fitter->getFitResult( fitName ) );
    if (!theresult){
        std::stringstream msg;
        msg << "fit called " << fitName << " has not been performed.";
        throw GeneralException("ToyStudy::localMinimumChecker", msg.str() ); 
    }

    TRandom3 rand(m_seed*m_seed);

    double bestNll = theresult->minNll(), thisnll(bestNll), minnll(thisnll);

    m_fitter->loadSnapshot( fitName );
    std::vector< RooFitResult* > listOfNewMinNllFits;
    bool success(false);

    string_tools stringHelper;

    RooFit::MsgLevel msg = RooMsgService::instance().globalKillBelow();
    m_fitter->setGlobalKillBelowError( RooFit::ERROR ); // Make it quiet

    Int_t fail_counter(0);
    TMVA::Timer timer( n, "ToyStudy::localMinimumChecker" );
    for ( Int_t i(0); i < n; ++i ) {
        // new random seed.
        m_seed += i;

        // load the original fit parameters
        m_fitter->loadSnapshot( fitName );

        // randomise the initial parameters based on their error
        m_fitter->wiggleAllParams( m_seed, method, fitName );
        //randomiseFitParams( method );

        std::string name = "localMinimumChecker_" + stringHelper.to_string<int>(i);
        // do the fit
        m_fitter->performFit( name.c_str() , 
                useMinos, // default is to use minos, this can sometimes lead to improved minima
                // but can also lead to numerical instability.... hmmn user can decide!
                kFALSE, // never verbose, could make it an option...
                kFALSE, // do not save a snapshot
                printResults, //print the fitResult each time.
                useSumW2Errors,
                extendedMode
                ) ;

        // Get the fit result and see if it is better than the nominal fit
        //RooFitResult* res = m_fitter->getFitResult( name.c_str() );

        thisnll = m_fitter->getFitResult( name.c_str() )->minNll();
        success = ( m_fitter->getFitResult( name.c_str() )->covQual()==3 
                && m_fitter->getFitResult( name.c_str() )->edm() < edmTol 
                && m_fitter->getFitResult( name.c_str() )->status() == 0 );
        if(!success) {
            ++fail_counter;
        }
        if (thisnll < minnll) { 
            minnll = thisnll ;
            listOfNewMinNllFits.push_back( m_fitter->getFitResult( name.c_str() ) );
        }

        timer.DrawProgressBar( i );
    }

    std::cout << "INFO: ToyStudy::localMinimumChecker - Requested " << n
        << " fits, where " << fail_counter << " failed to converge."  << std::endl;

    bool status(false);
    // get the size of the 
    if( listOfNewMinNllFits.empty() ) {
        status = true;
    } 
    else {

        RooArgList nominalNllFit( theresult->floatParsFinal() );

        // IT WOULD NE NEAT TO HAVE SOME DETAIL ANALYSIS OF THE VARIABLES AND THE ORIGINAL 
        // FITTED VALUES COMPARED TO THE NOMINAL HERE!!!!
        // Clean up if necessary too might aswell!
        std::cout << "INFO: ToyStudy::localMinimumChecker - Nominal fit returns nll value of " 
            << bestNll << std::endl;
        std::cout << "INFO: ToyStudy::localMinimumChecker - " << listOfNewMinNllFits.size() 
            << " better nll values were found" << std::endl;

        // sort the function
        std::sort( listOfNewMinNllFits.begin(), listOfNewMinNllFits.end(), 
                CompareFitResultFunctor() );

        std::vector< RooFitResult* >::iterator iter = listOfNewMinNllFits.begin();
        const std::vector< RooFitResult* >::const_iterator enditer =  listOfNewMinNllFits.end();
        for(; iter != enditer; ++iter ) {

            size_t position = std::distance( listOfNewMinNllFits.begin(), iter );
            RooArgList newNllFit( (*iter)->floatParsFinal() ) ;

            std::cout << "INFO: ToyStudy::localMinimumChecker - " << position + 1
                << ": found a better nll value by " << bestNll - (*iter)->minNll() << std::endl;
            std::cout << "     Variable      |    Nominal Value    |    New Value    |    Difference  "
                << std::endl;
            std::cout << " -----------------   -------------------   ---------------   --------------" 
                << std::endl;

            RooAbsReal* nomVar = 0;
            RooAbsReal* altVar = 0;

            TIterator* nomIt = nominalNllFit.createIterator();
            TIterator* altIt = newNllFit.createIterator();
            bool ret = true;
            while ( (nomVar = dynamic_cast<RooAbsReal*>( nomIt->Next() ) ) 
                    && ( altVar = dynamic_cast<RooAbsReal*>( altIt->Next() ) ) ) {

                if( TString(nomVar->GetName()) != TString(altVar->GetName()) ) {
                    std::stringstream msg;
                    msg << "Variables " << nomVar->GetName() << " and " << altVar->GetName()
                        << " are not the same, something has gone wrong as variables should be the same.";
                    throw GeneralException("ToyStudy::localMinimumChecker", msg.str() ); 
                }
                
                if ( nomVar->getVal() != altVar->getVal() ) {
                    double diff = nomVar->getVal() - altVar->getVal();
                    std::cout << nomVar->GetName() << "    |    " << nomVar->getVal() << "    |    " << 
                        altVar->getVal() << "    |    " << diff << std::endl;
                }
            }
            
            std::cout << " --------------------------------------------------------------------------"
                << std::endl;

            delete nomIt; nomIt=0;
            delete altIt; altIt=0;

            (*iter)->Print("V");
            std::cout << std::endl;
            std::cout << std::endl;
        }    
    }

    m_fitter->setGlobalKillBelowError( msg ); // Reset the verbosity

    return status;

}


//=============================================================================
// Does a MC study on the pdf. 
// Sets seed for random generator (to enable many small independent toy studies
// to be done in parallel).
// Allows individual yield parameters to be set explicitly
//=============================================================================
void ToyStudy::runToyStudy( const char* fitName, std::string setyields,  
        Fitter::RandomMethod randomiser )
{
   
    m_datafitResultName = fitName;
    std::vector<std::string> args; // Check if we're setting multiple parameters to new values.

    m_stringHelper.replaceAll( setyields, " ", "" ); // removes all whitespace
    if( setyields != "" ) {
        m_stringHelper.split( args, setyields, "," );
    }

    if( args.empty() && TString(setyields).Contains("=") ) {
        args.push_back(setyields);
    }

    // Store the result from the fit to data, useful for comparison
    m_dataResult = m_fitter->getFitResult( m_datafitResultName );
    this->dataMinNll();

    // Store the initial values from the fit to data and save a snapshot
    const RooArgList& initParams = m_dataResult->floatParsInit();
    const RooArgList& finParams = m_dataResult->floatParsFinal();
    RooArgSet finalParams( finParams );
    RooArgSet initialParams( initParams );
    
    //m_fitter->getWS()->saveSnapshot( "initParams", initialParams, kTRUE );

    // set the initial toy parameter values, if any need to be changed
    // will save it under the m_toyfitParamsName name .
    if( !args.empty() ) {
        this->setToyInitialValues( args, finalParams );
    }

    bool goodfit(false);

    RooDataSet* dataMC = 0;
    std::string pdfName = m_fitter->getModelName();
    std::string datasetName = m_fitter->getDataSetName();
    Int_t iExp(0), event(0), control(10), ci(0);
    bool ok(true);

    TMVA::Timer timer( m_nSamples, "ToyStudy::runToyStudy" );
    while ( iExp < m_nSamples ) {
        
        // get a copy of the original truth parameters.
        RooArgSet finalParamscopy = finalParams;

        if( ci == (control) ) {
            ok = false;
            break;
        }

        // load the nominal fit values for generation
        m_fitter->loadSnapshot( m_datafitResultName );

        // If we have set some default parameters, then we can call those here
        // and load that snap shot.
        if( !args.empty() ) {
            m_fitter->loadSnapshot( m_toyfitParamsName );
        }

        // generate the toy data based on and constrained parameter passed in.
        double generatedYields = m_fitter->sumYields();
        dataMC = m_fitter->generate( generatedYields, kTRUE ); // Always use Poisson dist

        // after we generate we need to change the initial parameter values
        m_fitter->wiggleAllParams( m_seed, randomiser, fitName, "iparams" );
//        m_fitter->gaussian_randomiser( &finalParamscopy, m_seed, "iparams" );

        //m_fitter->resetGaussianConstraints();
        const RooArgSet* theConstraints = m_fitter->getWS()->set( m_fitter->getConstraintString() );
        m_result = m_fitter->getWS()->pdf( pdfName.c_str() )->fitTo( *dataMC, 
                RooFit::NumCPU( m_fitter->getnCPU() ),
                RooFit::Extended( kTRUE ),
                RooFit::Save( kTRUE ),
                RooFit::PrintLevel(-1),
                RooFit::PrintEvalErrors(-1),
                RooFit::ExternalConstraints( *theConstraints )
                );

        // increment the total counter.
        ++event;

        // Maybe not so good to hardcode the emd distance here?... But the value is reasonable
        goodfit = m_result->covQual()==3 && m_result->edm()<1e-3 && m_result->status()==0;

        if ( !goodfit ) {   
            ++m_seed;
            ci++;
            // reset the initial parameters.
            delete dataMC; dataMC=0;
            std::cerr << "ToyStudy::runToyStudy -- Bad fit (covQual, edm, status) = (" 
                << m_result->covQual() 
                << ", " << m_result->edm() << ", " << m_result->status()  << ")" << std::endl;
            delete m_result; m_result=0;
            continue;
        }
        ci = 0; // reset the max fitting loop iExp

        fillVectors( &finalParams );
       
        // increment the ok counter.
        ++iExp;
        // reset the initial parameters.
        ++m_seed;
        delete dataMC; dataMC=0;
        delete m_result; m_result=0;
        timer.DrawProgressBar( iExp );
    }

    if(!ok) {
        std::stringstream msg;
        msg << "Failed to find convergent fit after " << ci << " iterations.";
        throw GeneralException("ToyStudy::runToyStudy", msg.str() );
    }

    std::cout << "ToyStudy::runToyStudy - Total number of failed toys: " << event - iExp 
        << " (" << static_cast<Double_t>(event-iExp)/ static_cast<Double_t>(event)
        << "\%)." << std::endl;
}


//=============================================================================
// plot the central value distribution from the toys
//=============================================================================
void ToyStudy::plotValues(TString fileName) const
{
    m_pulls.plotValues(fileName);
}


//=============================================================================
// plot the pulls distribution from the toys
//=============================================================================
void ToyStudy::plotPulls(TString fileName) const
{
    m_pulls.plotPulls(fileName);
}


//=============================================================================
// plot the systematic type I toy study
//=============================================================================
void ToyStudy::plotSystI( TString fileName, std::string paramName ) const
{
    m_pulls.plotSystI(fileName, paramName);
}


//=============================================================================
// plot the systematic type II toy study 
//=============================================================================
void ToyStudy::plotSystII( TString fileName ) const
{
    m_pulls.plotSystII( fileName );
}


//=============================================================================
// write the pull values to a text file
//=============================================================================
void ToyStudy::writePulls( TString fileName ) const
{
    m_pulls.writeVectors(fileName);
}


//=============================================================================
// plot the distribution of the NLL values
//=============================================================================
void ToyStudy::plotNlls( TString fileName ) const
{
    m_pulls.plotNlls(fileName);
}


//=============================================================================
// fill all values based on the fit result
//=============================================================================
void ToyStudy::fillVectors(const RooArgSet* genParams )
{
    // check that there is a result
    if (m_result==0) {
        std::cerr << "ERROR in ToyStudy::fillVectors : No fit result to use." << std::endl;
        return;
    }

    m_pulls.fillVectors( genParams, m_result );
}


//=============================================================================
// fill all values based on the fit result
//=============================================================================
void ToyStudy::fillVectors()
{
    // check that there is a result
    if (m_result==0) {
        std::cerr << "ERROR in ToyStudy::fillVectors : No fit result to use." << std::endl;
        return;
    }

    m_pulls.fillVectors( m_result );
}


//=============================================================================
// fill data results for Systematics Type II studies
//=============================================================================
void ToyStudy::fillVectors_SystII( RooFitResult* res_official, 
        RooFitResult* res_toyMC)
{
    // check that there is a result
    if ( (res_official==0) || (res_toyMC==0) ){
        std::cerr << "ERROR in ToyStudy::fillVectors : No fit result to use."
            << std::endl;
        return;
    }

    m_pulls.fillVectors_Syst( res_official, res_toyMC );
}


//=============================================================================
// set the data fit result
//=============================================================================
void ToyStudy::dataMinNll()
{
    // check that there is a result
    if (m_dataResult==0) {
        std::cerr << "ERROR in ToyStudy::dataMinNll : No fit result to use."
            << std::endl;
        return;
    }

    m_pulls.dataFitResult( m_dataResult );
}


//=============================================================================
// set the seed of the toy study
//=============================================================================
void ToyStudy::setSeed( Int_t seed ) {
    RooRandom::randomGenerator()->SetSeed( seed );
    m_seed = seed;
}


/*void ToyStudy::setSystematicParams( std::vector<std::string> args ) {

//put dependents names in a vector
RooWorkspace* rws = dynamic_cast<RooWorkspace*>( m_fitter->getWS() );

//checking we have all the dependents
RooAbsReal* var = 0;
std::vector<std::string>::const_iterator iter = args.begin();
const std::vector<std::string>::const_iterator end = args.end();
for (; iter != end; ++iter ) {
var = dynamic_cast<RooAbsReal*>( rws->obj( iter->c_str() ) );
if(!var){
throw WSRetrievalFailure("ToyStudy::setSystematicParams",
 *rws, iter->c_str(), "RooAbsReal");
 }    
 m_wiggleCorrelation.add(*var);
 }

 }*/


//=============================================================================
// To be run after all parameters are set to values after fit.
// Then the yields for example can be modified here only.
//=============================================================================
void ToyStudy::setToyInitialValues( const std::vector<std::string>& args,
        RooArgSet& params ) 
{
    if( args.empty() ) { return; }

    std::vector<std::string>::const_iterator iter = args.begin();
    const std::vector<std::string>::const_iterator enditer = args.end();
    for ( ; iter != enditer; ++iter ) {

        // now need to split by the varable and value,
        std::vector<std::string> vartovalue;
        m_stringHelper.split( vartovalue, *iter, "=" );

        if( vartovalue.empty() || vartovalue.size() != 2 ) {
            throw GeneralException("ToyStudy::setToyInitialValues",
                    "Unable to parse " + *iter );
            break;
        }

        // get the parameter name
        TString parameterName = vartovalue.front();

        // now get required value of the parameter
        double value = m_stringHelper.to_number<double>( vartovalue.back() );

        // check if parameter name is in fact a regexp
        if( parameterName.MaybeRegexp() ) {
            // define the regular expression.
            TRegexp re( parameterName, kTRUE );
            Bool_t ok(false);
            // now need to loop over the params and see if name can be matched.
            RooRealVar* var = 0;
            TIterator* varIt = params.createIterator();
            while ( ( var = dynamic_cast<RooRealVar*>( varIt->Next() ) ) ) {
                if ( var->IsA()->InheritsFrom( RooRealVar::Class() ) ) {
                    TString name = var->GetName();

                    // see if we can match the regex.
                    if ( name.Index(re) == kNPOS ) { continue; }
                    ok = true;
                    std::cout <<"INFO: ToyStudy::setToyInitialValues - setting " 
                        << parameterName << " (Regex) value to " << value << std::endl; 
                    var->setVal( value );

                }
            }
            delete varIt; varIt=0;
            if( !ok ) {
                std::stringstream msg;
                msg << "Specified regex string " << parameterName << ", did not do anything, please check it.";
                throw GeneralException("ToyStudy::setToyInitialValues", msg.str() );
            }

        } else {

            // make sure the variable exists in the fitted parameter space
            RooRealVar* var = dynamic_cast<RooRealVar*>( params.find( parameterName ) );
            if ( !var ) {
                throw WSRetrievalFailure("ToyStudy::setToyInitialValues",
                        *m_fitter->getWS(), parameterName.Data(), "RooRealVar" );
            }

            var->setVal( value );
            std::cout <<"INFO: ToyStudy::setToyInitialValues - setting " 
                << parameterName << " value to " << value << std::endl;
        }
    }

    // save the snapshot
    m_fitter->getWS()->saveSnapshot( m_toyfitParamsName, params, kTRUE );

    // load it ready for use
    if ( !m_fitter->getWS()->loadSnapshot( m_toyfitParamsName ) )
    {
        throw WSRetrievalFailure("Fitter::setToyInitialValues",
                *m_fitter->getWS(), m_toyfitParamsName, " toy parameters snapshot");
    }
}

//=============================================================================
// set systematic parameters to the values listed in args, used for systematic
// studies such as varying a parameter that was fixed due to MC.
//=============================================================================
/*void ToyStudy::setSystematicValues( std::vector<std::string> args ) {

  RooArgSet variables;
  RooWorkspace* m_rws = dynamic_cast<RooWorkspace*>( m_fitter->getWS() );
  std::vector<std::string>::const_iterator iter = args.begin();
  const std::vector<std::string>::const_iterator enditer = args.end();
  for (; iter != enditer; ++iter) {

// now need to split by the varable and value,
std::vector<std::string> vartovalue;
m_stringHelper.split( vartovalue, *iter, "=" );

if( vartovalue.empty() || vartovalue.size() != 2 ) {
throw GeneralException("ToyStudy::setToyInitialValues",
"Unable to parse " + *iter );
break;
}

std::string parameterName = vartovalue.front();
double value = m_stringHelper.to_number<double>( vartovalue.back() );
m_fitter->setParameterValue( parameterName.c_str(), value );
variables.add( *m_fitter->getVar( parameterName.c_str() ) );
std::cout <<"INFO: ToyStudy::setToyInitialValues - setting " << parameterName << " value to " << value << std::endl;

}
m_fitter->wiggleParams( &m_wiggleCorrelation, model, rds, m_seed, m_systematicParamsName );

}
*/

//=============================================================================
// Perform parameter scans to determined systematics effects due to the use of
// fixed parameters in the fit to data -- aka Systematics I   
//=============================================================================
void ToyStudy::runSystI(  const char* fitName, Fitter* fitterMC, std::string setparameters,  
        Fitter::RandomMethod randomiser ) 
{
    m_datafitResultName = fitName; // Store the fit to data result name
    std::vector<std::string> args; // Parameters to be changed

    m_stringHelper.replaceAll( setparameters, " ", "" ); // removes all whitespace
    if( setparameters != "" ) {
        m_stringHelper.split( args, setparameters, "," ); // passes the list of parameters
    }

    // Store the result from the fit to data
    m_dataResult = m_fitter->getFitResult( m_datafitResultName );
    dataMinNll();

    // Store the initial values from the fit to data and save a snapshot
    const RooArgList initParams = m_dataResult->floatParsInit();
    RooArgSet initialParams( initParams );
    m_fitter->getWS()->saveSnapshot( "initial_params", initialParams, kTRUE );

    // Get information from MC fit
    RooFitResult* MCResult = dynamic_cast<RooFitResult*>( fitterMC->getWS()->obj( "rfres_fitResults" ) );

    // Final name of the fit model
    RooRealVar* nameModelScheme = m_fitter->getWS()->var("ModelName");
    TString nameModel = nameModelScheme->GetTitle();
    std::string pdfName = nameModel.Data();
    // Additional informations and data
    std::string datasetName = m_fitter->getDataSetName();
    RooDataSet* data = m_fitter->getDataSet((datasetName).c_str());

    std::string paramWiggleName("");
    Int_t iExp(0);
    bool goodfit(false);

    TMVA::Timer timer( m_nSamples, "ToyStudy::runSystI" );
    // Loop over all the parameters 
    for(std::vector<std::string>::const_iterator it_par = args.begin(); it_par != args.end(); ++it_par){

        std::cout << "ToyStudy::runSystI - Initialising systematics for parameter: " << (*it_par) << std::endl;

        // Loop over the number of experiments
        while ( iExp < m_nSamples ) {

            // Load data initial parameters parameters
            m_fitter->loadSnapshot( "initial_params" );

            // Vary given parameter within its correlation matrix
            paramWiggleName = (*it_par);
            m_fitter->wiggleParamsFixed( paramWiggleName, randomiser, MCResult, m_datafitResultName);

            // Ommiting warning messages
            RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
            gErrorIgnoreLevel = kWarning;

            // Perform the fit to the data again with a different parameter value
            m_result = m_fitter->getWS()->pdf( pdfName.c_str() )->fitTo( *data, 
                    RooFit::NumCPU( m_fitter->getnCPU() ),
                    RooFit::Extended( kTRUE ),
                    RooFit::Save( kTRUE ),
                    RooFit::PrintLevel(-1),
                    RooFit::PrintEvalErrors(-1),
                    RooFit::ExternalConstraints( *( m_fitter->getWS()->set(  m_fitter->getConstraintString() ) ) )
                    );

            goodfit = m_result->covQual()==3 && m_result->edm()<1e-3 && m_result->status()==0;

            if ( !goodfit ) {   
                // reset the initial parameters.
                delete m_result; m_result=0;
                continue;
            }

            fillVectors();
            ++iExp;
            delete m_result; m_result=0;
            timer.DrawProgressBar( iExp );
        }
    }    
}

//=============================================================================
// Generate ToyMC samples from a given PDF model and fitting with the benchmark
// PDF shape. -- aka Systematics Type II   
//=============================================================================
void ToyStudy::runSystII(  const char* fitName, Fitter* fitter_official,   
        Fitter::RandomMethod randomiser ) 
{

    m_datafitResultName = fitName; // Store the fit to data result name for alternative mode
    const char* m_datafitResutName_benchmark = fitName; // Kept the same name in both for simplification	

    // Store the result from the fit to data
    m_dataResult = m_fitter->getFitResult( m_datafitResultName );
    dataMinNll();

    // Store the initial values from the benchmark fit to data and save a snapshot
    RooFitResult* dataResult_benchmark = fitter_official->getFitResult( m_datafitResutName_benchmark );
    const RooArgList initParams = dataResult_benchmark->floatParsInit();
    const RooArgList fitParams  = dataResult_benchmark->floatParsFinal();

    RooArgSet initialParams( initParams );
    RooArgSet finalParams( fitParams );
    //fitter_official->getWS()->saveSnapshot( "initial_params", initialParams, kTRUE );
    fitter_official->getWS()->saveSnapshot( "fitResults", finalParams, kTRUE );

    // Final name of the benchmark fit model pdf
    RooRealVar* nameModelScheme = fitter_official->getWS()->var("ModelName");
    TString nameModel = nameModelScheme->GetTitle();
    std::string pdfName = nameModel.Data();

    // Toy MC sample 
    RooDataSet* dataToyMC = 0;

    Int_t iExp(0);
    bool goodfit(false);

    TMVA::Timer timer( m_nSamples, "ToyStudy::runSystII" );

    std::cout << "ToyStudy::runSystI - Initialising systematics studies Type II " << std::endl;

    // Loop over the number of experiments
    while ( iExp < m_nSamples ) {

        // Load snapshot of the fit results of the alternative model
        m_fitter->loadSnapshot( m_datafitResultName );

        // Generate ToyMC sample from the alternative model
        dataToyMC = m_fitter->generate( m_fitter->sumYields() , kTRUE ); // Allways use Poisson dist

        // Load initial values of benchmark model
        //fitter_official->loadSnapshot( "initial_params" );
        fitter_official->loadSnapshot( "fitResults" );

        // Ommiting warning messages
        RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
        gErrorIgnoreLevel = kWarning;

        // Perform the fit to the ToyMC samples with the benchmark model
        m_result = fitter_official->getWS()->pdf( pdfName.c_str() )->fitTo( *dataToyMC, 
                RooFit::NumCPU( 4 ),
                RooFit::Extended( kTRUE ),
                RooFit::Save( kTRUE ),
                //RooFit::Minos( kTRUE),
                RooFit::PrintLevel(-1),
                RooFit::PrintEvalErrors(-1),
                RooFit::ExternalConstraints( *( fitter_official->getWS()->set(  fitter_official->getConstraintString() ) ) )
                );
        //m_result->Print("V"); 
        goodfit = m_result->covQual()==3 && m_result->edm()<1e-3 && m_result->status()==0;

        if ( !goodfit ) {   
            // reset the initial parameters.
            delete m_result; m_result=0;
            continue;
        }

        fillVectors_SystII(dataResult_benchmark, m_result);
        ++iExp;
        delete m_result; m_result=0;
        timer.DrawProgressBar( iExp );
    }    
}
