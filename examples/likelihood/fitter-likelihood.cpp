#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

//ROOT
#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TMultiGraph.h"

// RooFit

// FastFitter custom package
#include "SimultaneousFitter.hpp"
#include "ToyStudy.hpp"
#include "LHCbStyle.hpp"
#include "Fitter1DLikeProfile.hpp"
#include "FitterUpperLimit.hpp"

// Boost
//#include <boost/program_options.hpp>
//#include <boost/algorithm/string/split.hpp>
//#include <boost/algorithm/string.hpp>

// Local include

TGraph createLLGraph() ;
TGraph createDDGraph() ;

int main( int argc, const char* argv[] ) {

    //================================================================================
    Fast::LHCbStyle lhcbStyle();
    //================================================================================

	///////////////////////////////////
	Double_t syst_error   = 8.897e-8; // correlated
	Double_t syst_errorDD = 3.4732e-7; // (6623375272%)
	Double_t syst_errorLL = 4.2549e-7;

	TString xaxis_title = "B( #Xi_{b}#rightarrow K_{S}^{0}p#pi )";
	TString xaxis_DDtitle = "B( #Xi_{b}#rightarrow K_{S}^{0}(DD)p#pi )";
	TString xaxis_LLtitle = "B( #Xi_{b}#rightarrow K_{S}^{0}(LL)p#pi )";

    //TString curDir = "/home/matthew/C++/FitterCode/fitter/";
//    TString curDir = getcwd(NULL,0);
//    TString saveDir = curDir + "/";
    TString saveDir = "./";
	///////////////////////////////////


    //================================================================================
    // create LL graph
    //================================================================================
    TGraph hNLL_LL = createLLGraph();
    Fast::Fitter1DLikeProfile profileLL( "NLL_LL_Xib2KSppi", "dll" );
    profileLL.setGlobalKillBelowError();
    profileLL.setGraph( hNLL_LL );
    
    // save the delta log likelihood profile
    profileLL.save( saveDir+"nllPlots.root", "", false ); // update false for first instance

    //profileLL.plotGraph( saveDir, xaxis_LLtitle );
	std::cout << "Minimum NLL LL : " << profileLL.minimum() << std::endl;

    // save the likelihood transform
    profileLL.convertToLikelihood();
    profileLL.save( saveDir+"nllPlots.root", "", true ); // update true
    //profileLL.plotGraph( saveDir, xaxis_LLtitle );

    // do the systematic convolution LL uncorrelated first.
    profileLL.systConvolution( syst_errorLL, "FFT" );
    profileLL.save( saveDir+"nllPlots.root", "conv", true ); // update true
    //profileLL.plotGraph( saveDir+"conv", xaxis_LLtitle );

    profileLL.convertToLogLikelihood();
    //================================================================================


    //================================================================================
    // create DD graph
    //================================================================================
    TGraph hNLL_DD = createDDGraph();
    Fast::Fitter1DLikeProfile profileDD( "NLL_DD_Xib2KSppi", "dll" );
    profileDD.setGlobalKillBelowError();
    profileDD.setGraph( hNLL_DD );
    
    // save the delta log likelihood profile
    profileDD.save( saveDir+"nllPlots.root", "", true ); // update false for first instance
    //profileDD.plotGraph( saveDir, xaxis_DDtitle );
	std::cout << "Minimum NLL DD : " << profileDD.minimum() << std::endl;

    // save the likelihood transfrom
    profileDD.convertToLikelihood();
    profileDD.save( saveDir+"nllPlots.root", "", true ); // update true
    //profileDD.plotGraph( saveDir, xaxis_DDtitle );

    // do the systematic convolution LL uncorrelated first.
    profileDD.systConvolution( syst_errorDD, "FFT" );
    profileDD.save( saveDir+"nllPlots.root", "conv", true ); // update true
    //profileDD.plotGraph( saveDir+"conv", xaxis_DDtitle );

    profileDD.convertToLogLikelihood();
    //================================================================================


    //================================================================================
    // The statistical significances are thus
    //================================================================================
    std::cout << "\nStatistical only" << std::endl;
    std::cout << "\t-Delta ln L(0) LL = " << profileLL.significance() << std::endl;
    std::cout << "\nStatistical only" << std::endl;
    std::cout << "\t-Delta ln L(0) DD = " << profileDD.significance() << std::endl;
    //================================================================================


    //================================================================================
    // Now combine the two profiles and do the final convolution for correlated
    // systematics. Both profiles have to be in LogLikelihood mode to add
    //================================================================================
    std::cout << "Combining the DD and LL mode likelihoods.\n";
    Fast::Fitter1DLikeProfile combined = profileLL + profileDD;
    combined.SetName( "NLL_Xib2KSppi" );
    combined.save( saveDir+"nllPlots.root", "merged", true ); // update true
    //combined.plotGraph( saveDir+"merged", xaxis_title ); 

    // save the merged likelihood profiles
    combined.convertToLikelihood();
    combined.save( saveDir+"nllPlots.root", "merged", true ); // update true
    //combined.plotGraph( saveDir+"merged", xaxis_title ); 

    // Finally do the convolution.
    combined.systConvolution( syst_error, "FFT" );
    combined.save( saveDir+"nllPlots.root", "merged_convolution", true ); // update true
    //combined.plotGraph( saveDir+"merged_convolution", xaxis_title ); 

    combined.convertToLogLikelihood();
    combined.save( saveDir+"nllPlots.root", "merged_convolution", true ); // update true
    //combined.plotGraph( saveDir+"merged_convolution", xaxis_title ); 

    std::cout << "Branching Fraction = " << combined.minimum() << " ^{+" << combined.getErrorHi() << "}_{" << combined.getErrorLow() << "}\n";
    std::cout << "Errors calculated at the " << combined.getConfidenceLevelError()*100. << "\% confidence level." << std::endl;
    //================================================================================
    
    
    //================================================================================
    // Now plot the 
    //================================================================================
    Fast::FitterUpperLimit limit( combined );
    limit.calculate( 0.90 );
    limit.addConfidenceAttributes( 0.90, kMagenta, 3335 );
    double upperlimit90 = limit.getUpperLimit( 0.90 );
    std::cout << "The CL(90%) confidence has value: " << upperlimit90 << std::endl;
  
    limit.calculate( 0.95 );
    limit.addConfidenceAttributes( 0.95, kAzure+2, 3353 );
    double upperlimit95 = limit.getUpperLimit( 0.95 );
    std::cout << "The CL(95%) confidence has value: " << upperlimit95 << std::endl;
  
    limit.calculate( 0.99 );
    limit.addConfidenceAttributes( 0.99, kRed, 3335 );
    double upperlimit99 = limit.getUpperLimit( 0.99 );
    std::cout << "The CL(99%) confidence has value: " << upperlimit99 << std::endl;

    limit.plotLimits( saveDir+"limits", xaxis_title );

    return 0;
}




TGraph createDDGraph() {
    TGraph hNLL_DD( 89 );
    hNLL_DD.SetPoint(0,-4.81737e-06,6.88861);
    hNLL_DD.SetPoint(1,-4.69012e-06,6.54044);
    hNLL_DD.SetPoint(2,-4.56287e-06,6.20153);
    hNLL_DD.SetPoint(3,-4.43561e-06,5.87347);
    hNLL_DD.SetPoint(4,-4.30836e-06,5.55492);
    hNLL_DD.SetPoint(5,-4.1811e-06,5.24648);
    hNLL_DD.SetPoint(6,-4.05385e-06,4.94795);
    hNLL_DD.SetPoint(7,-3.9266e-06,4.6593);
    hNLL_DD.SetPoint(8,-3.79934e-06,4.38006);
    hNLL_DD.SetPoint(9,-3.67209e-06,4.11048);
    hNLL_DD.SetPoint(10,-3.54483e-06,3.85037);
    hNLL_DD.SetPoint(11,-3.41758e-06,3.59969);
    hNLL_DD.SetPoint(12,-3.29033e-06,3.35828);
    hNLL_DD.SetPoint(13,-3.16307e-06,3.12608);
    hNLL_DD.SetPoint(14,-3.03582e-06,2.90299);
    hNLL_DD.SetPoint(15,-2.90856e-06,2.68893);
    hNLL_DD.SetPoint(16,-2.78131e-06,2.48381);
    hNLL_DD.SetPoint(17,-2.65406e-06,2.28751);
    hNLL_DD.SetPoint(18,-2.5268e-06,2.09997);
    hNLL_DD.SetPoint(19,-2.39955e-06,1.92109);
    hNLL_DD.SetPoint(20,-2.27229e-06,1.75078);
    hNLL_DD.SetPoint(21,-2.14504e-06,1.58895);
    hNLL_DD.SetPoint(22,-2.01779e-06,1.43554);
    hNLL_DD.SetPoint(23,-1.89053e-06,1.29045);
    hNLL_DD.SetPoint(24,-1.76328e-06,1.15356);
    hNLL_DD.SetPoint(25,-1.63602e-06,1.02481);
    hNLL_DD.SetPoint(26,-1.50877e-06,0.904084);
    hNLL_DD.SetPoint(27,-1.38152e-06,0.791352);
    hNLL_DD.SetPoint(28,-1.25426e-06,0.686494);
    hNLL_DD.SetPoint(29,-1.12701e-06,0.589435);
    hNLL_DD.SetPoint(30,-9.99755e-07,0.500084);
    hNLL_DD.SetPoint(31,-8.72501e-07,0.418348);
    hNLL_DD.SetPoint(32,-7.45247e-07,0.344159);
    hNLL_DD.SetPoint(33,-6.17993e-07,0.277456);
    hNLL_DD.SetPoint(34,-4.90739e-07,0.218133);
    hNLL_DD.SetPoint(35,-3.63485e-07,0.1661);
    hNLL_DD.SetPoint(36,-2.36231e-07,0.121297);
    hNLL_DD.SetPoint(37,-1.08978e-07,0.0836425);
    hNLL_DD.SetPoint(38,1.82764e-08,0.0530563);
    hNLL_DD.SetPoint(39,1.4553e-07,0.0294662);
    hNLL_DD.SetPoint(40,2.72784e-07,0.0127941);
    hNLL_DD.SetPoint(41,4.00038e-07,0.00295924);
    hNLL_DD.SetPoint(42,5.27292e-07,-0.000105139);
    hNLL_DD.SetPoint(43,6.54546e-07,0.00352432);
    hNLL_DD.SetPoint(44,7.818e-07,0.013789);
    hNLL_DD.SetPoint(45,9.09054e-07,0.030589);
    hNLL_DD.SetPoint(46,1.03631e-06,0.0538674);
    hNLL_DD.SetPoint(47,1.16356e-06,0.0835541);
    hNLL_DD.SetPoint(48,1.29082e-06,0.11958);
    hNLL_DD.SetPoint(49,1.41807e-06,0.161876);
    hNLL_DD.SetPoint(50,1.54532e-06,0.210375);
    hNLL_DD.SetPoint(51,1.67258e-06,0.265018);
    hNLL_DD.SetPoint(52,1.79983e-06,0.325724);
    hNLL_DD.SetPoint(53,1.92709e-06,0.392435);
    hNLL_DD.SetPoint(54,2.05434e-06,0.465086);
    hNLL_DD.SetPoint(55,2.18159e-06,0.543614);
    hNLL_DD.SetPoint(56,2.30885e-06,0.627957);
    hNLL_DD.SetPoint(57,2.4361e-06,0.718051);
    hNLL_DD.SetPoint(58,2.56336e-06,0.813836);
    hNLL_DD.SetPoint(59,2.69061e-06,0.915253);
    hNLL_DD.SetPoint(60,2.81786e-06,1.02224);
    hNLL_DD.SetPoint(61,2.94512e-06,1.13474);
    hNLL_DD.SetPoint(62,3.07237e-06,1.2527);
    hNLL_DD.SetPoint(63,3.19962e-06,1.37605);
    hNLL_DD.SetPoint(64,3.32688e-06,1.50474);
    hNLL_DD.SetPoint(65,3.45413e-06,1.63872);
    hNLL_DD.SetPoint(66,3.58139e-06,1.77793);
    hNLL_DD.SetPoint(67,3.70864e-06,1.92233);
    hNLL_DD.SetPoint(68,3.83589e-06,2.07183);
    hNLL_DD.SetPoint(69,3.96315e-06,2.22641);
    hNLL_DD.SetPoint(70,4.0904e-06,2.38601);
    hNLL_DD.SetPoint(71,4.21766e-06,2.55057);
    hNLL_DD.SetPoint(72,4.34491e-06,2.72004);
    hNLL_DD.SetPoint(73,4.47216e-06,2.89438);
    hNLL_DD.SetPoint(74,4.59942e-06,3.07354);
    hNLL_DD.SetPoint(75,4.72667e-06,3.25746);
    hNLL_DD.SetPoint(76,4.85393e-06,3.44611);
    hNLL_DD.SetPoint(77,4.98118e-06,3.63942);
    hNLL_DD.SetPoint(78,5.10843e-06,3.83735);
    hNLL_DD.SetPoint(79,5.23569e-06,4.03986);
    hNLL_DD.SetPoint(80,5.36294e-06,4.24691);
    hNLL_DD.SetPoint(81,5.4902e-06,4.45844);
    hNLL_DD.SetPoint(82,5.61745e-06,4.67442);
    hNLL_DD.SetPoint(83,5.7447e-06,4.89479);
    hNLL_DD.SetPoint(84,5.87196e-06,5.11952);
    hNLL_DD.SetPoint(85,5.99921e-06,5.34856);
    hNLL_DD.SetPoint(86,6.12647e-06,5.58188);
    hNLL_DD.SetPoint(87,6.25372e-06,5.81942);
    hNLL_DD.SetPoint(88,6.38097e-06,6.06115);
    return hNLL_DD;
}


TGraph createLLGraph(  ) {
    TGraph hNLL_LL( 89 );
    hNLL_LL.SetPoint(0,-4.81737e-06,3.23674);
    hNLL_LL.SetPoint(1,-4.69012e-06,3.07606);
    hNLL_LL.SetPoint(2,-4.56287e-06,2.92005);
    hNLL_LL.SetPoint(3,-4.43561e-06,2.76863);
    hNLL_LL.SetPoint(4,-4.30836e-06,2.62178);
    hNLL_LL.SetPoint(5,-4.1811e-06,2.47944);
    hNLL_LL.SetPoint(6,-4.05385e-06,2.34157);
    hNLL_LL.SetPoint(7,-3.9266e-06,2.20813);
    hNLL_LL.SetPoint(8,-3.79934e-06,2.07907);
    hNLL_LL.SetPoint(9,-3.67209e-06,1.95436);
    hNLL_LL.SetPoint(10,-3.54483e-06,1.83395);
    hNLL_LL.SetPoint(11,-3.41758e-06,1.71778);
    hNLL_LL.SetPoint(12,-3.29033e-06,1.60583);
    hNLL_LL.SetPoint(13,-3.16307e-06,1.49804);
    hNLL_LL.SetPoint(14,-3.03582e-06,1.39438);
    hNLL_LL.SetPoint(15,-2.90856e-06,1.29481);
    hNLL_LL.SetPoint(16,-2.78131e-06,1.19927);
    hNLL_LL.SetPoint(17,-2.65406e-06,1.10773);
    hNLL_LL.SetPoint(18,-2.5268e-06,1.02014);
    hNLL_LL.SetPoint(19,-2.39955e-06,0.936468);
    hNLL_LL.SetPoint(20,-2.27229e-06,0.856666);
    hNLL_LL.SetPoint(21,-2.14504e-06,0.780694);
    hNLL_LL.SetPoint(22,-2.01779e-06,0.708414);
    hNLL_LL.SetPoint(23,-1.89053e-06,0.63997);
    hNLL_LL.SetPoint(24,-1.76328e-06,0.575375);
    hNLL_LL.SetPoint(25,-1.63602e-06,0.514316);
    hNLL_LL.SetPoint(26,-1.50877e-06,0.457008);
    hNLL_LL.SetPoint(27,-1.38152e-06,0.403176);
    hNLL_LL.SetPoint(28,-1.25426e-06,0.352899);
    hNLL_LL.SetPoint(29,-1.12701e-06,0.306136);
    hNLL_LL.SetPoint(30,-9.99755e-07,0.262851);
    hNLL_LL.SetPoint(31,-8.72501e-07,0.223006);
    hNLL_LL.SetPoint(32,-7.45247e-07,0.186564);
    hNLL_LL.SetPoint(33,-6.17993e-07,0.153489);
    hNLL_LL.SetPoint(34,-4.90739e-07,0.123744);
    hNLL_LL.SetPoint(35,-3.63485e-07,0.0972938);
    hNLL_LL.SetPoint(36,-2.36231e-07,0.0741027);
    hNLL_LL.SetPoint(37,-1.08978e-07,0.0541358);
    hNLL_LL.SetPoint(38,1.82764e-08,0.0373586);
    hNLL_LL.SetPoint(39,1.4553e-07,0.0237363);
    hNLL_LL.SetPoint(40,2.72784e-07,0.0132363);
    hNLL_LL.SetPoint(41,4.00038e-07,0.00582457);
    hNLL_LL.SetPoint(42,5.27292e-07,0.00146826);
    hNLL_LL.SetPoint(43,6.54546e-07,0.000134797);
    hNLL_LL.SetPoint(44,7.818e-07,0.00179201);
    hNLL_LL.SetPoint(45,9.09054e-07,0.00640815);
    hNLL_LL.SetPoint(46,1.03631e-06,0.0139519);
    hNLL_LL.SetPoint(47,1.16356e-06,0.0243922);
    hNLL_LL.SetPoint(48,1.29082e-06,0.0376985);
    hNLL_LL.SetPoint(49,1.41807e-06,0.0538406);
    hNLL_LL.SetPoint(50,1.54532e-06,0.0727888);
    hNLL_LL.SetPoint(51,1.67258e-06,0.0945134);
    hNLL_LL.SetPoint(52,1.79983e-06,0.118986);
    hNLL_LL.SetPoint(53,1.92709e-06,0.146177);
    hNLL_LL.SetPoint(54,2.05434e-06,0.176059);
    hNLL_LL.SetPoint(55,2.18159e-06,0.208603);
    hNLL_LL.SetPoint(56,2.30885e-06,0.243786);
    hNLL_LL.SetPoint(57,2.4361e-06,0.281574);
    hNLL_LL.SetPoint(58,2.56336e-06,0.321942);
    hNLL_LL.SetPoint(59,2.69061e-06,0.364866);
    hNLL_LL.SetPoint(60,2.81786e-06,0.410318);
    hNLL_LL.SetPoint(61,2.94512e-06,0.458272);
    hNLL_LL.SetPoint(62,3.07237e-06,0.508704);
    hNLL_LL.SetPoint(63,3.19962e-06,0.561587);
    hNLL_LL.SetPoint(64,3.32688e-06,0.616898);
    hNLL_LL.SetPoint(65,3.45413e-06,0.674612);
    hNLL_LL.SetPoint(66,3.58139e-06,0.734705);
    hNLL_LL.SetPoint(67,3.70864e-06,0.797152);
    hNLL_LL.SetPoint(68,3.83589e-06,0.861931);
    hNLL_LL.SetPoint(69,3.96315e-06,0.929019);
    hNLL_LL.SetPoint(70,4.0904e-06,0.998391);
    hNLL_LL.SetPoint(71,4.21766e-06,1.06996);
    hNLL_LL.SetPoint(72,4.34491e-06,1.14385);
    hNLL_LL.SetPoint(73,4.47216e-06,1.21995);
    hNLL_LL.SetPoint(74,4.59942e-06,1.29825);
    hNLL_LL.SetPoint(75,4.72667e-06,1.37872);
    hNLL_LL.SetPoint(76,4.85393e-06,1.46134);
    hNLL_LL.SetPoint(77,4.98118e-06,1.5461);
    hNLL_LL.SetPoint(78,5.10843e-06,1.63296);
    hNLL_LL.SetPoint(79,5.23569e-06,1.72192);
    hNLL_LL.SetPoint(80,5.36294e-06,1.81295);
    hNLL_LL.SetPoint(81,5.4902e-06,1.90604);
    hNLL_LL.SetPoint(82,5.61745e-06,2.00116);
    hNLL_LL.SetPoint(83,5.7447e-06,2.09829);
    hNLL_LL.SetPoint(84,5.87196e-06,2.19742);
    hNLL_LL.SetPoint(85,5.99921e-06,2.29854);
    hNLL_LL.SetPoint(86,6.12647e-06,2.40161);
    hNLL_LL.SetPoint(87,6.25372e-06,2.50662);
    hNLL_LL.SetPoint(88,6.38097e-06,2.61356);

    return hNLL_LL;
}
