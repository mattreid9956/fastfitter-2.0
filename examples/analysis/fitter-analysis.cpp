//C++
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

//ROOT
#include "TChain.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"
#include "TROOT.h"
#include "TH2D.h"
#include "TFile.h"

//RooFit
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooFitResult.h"
#include "RooRandom.h"
#include "RooKeysPdf.h"
#include "RooAbsCollection.h"

//Custom
#include "SimultaneousFitter.hpp"
#include "ToyStudy.hpp"
#include "Exceptions.hpp"

using Fast::GeneralException;
using Fast::WSRetrievalFailure;

void usage()
{
    std::cout << "./model.exe <v1PIDCut> <v2PIDCut>" << std::endl;
    std::cout << "<v1PIDCut> = cut threshold on KstarK/PhiKPlus" << std::endl;
    std::cout << "<v2PIDCut> = cut threshold on KstarPi/PhiKMinus" << std::endl;
    exit(EXIT_FAILURE);
}

void plotPrettyMassAndPull( TCanvas* cBmass, std::string save, Fast::SimultaneousFitter * fitter, std::string name
        , std::string slicename, std::string fitName ); 


int main(int argc, char* argv[])
{
    //int nargs = argc-1;
    //if (nargs!=2) usage();
    //std::string v1PIDCut = argv[1], v2PIDCut = argv[2];

    //================================================================================
    // Create instance of the fitter and set the mass variable
    //================================================================================
    Fast::SimultaneousFitter fitter( "B2JpsiKShhFit", "B0 --> Jpsi KS h' h Fit" );
    fitter.loadWS( "MyWorkspace.root", "workspace", false );

    //fitter.getWS()->Print("V");
    //return 0;
    fitter.setModelName("pdf");
    fitter.setDataSetName("data");
    fitter.setCategoryName("mode");
    fitter.setConstraintString("setConstraints");

    // Since the fit was done not using the fast fitter package we must grab the fit result explicitly.
    RooFitResult* fitResult = dynamic_cast<RooFitResult*>( fitter.getWS()->obj( "fitResult_pdf" ) );
    fitResult->Print("V");

    std::cout << fitResult->status() << " " << fitResult->edm() << std::endl;
    const RooArgList initParams = fitResult->floatParsInit();
    const RooArgList fitParams = fitResult->floatParsFinal();

    RooArgSet initialParams( initParams );
    RooArgSet fittedParams( fitParams );

    fitter.getWS()->saveSnapshot( "initial_params", initialParams, kTRUE );
    fitter.getWS()->saveSnapshot( "fitResults", fittedParams, kTRUE );
    fitter.getWS()->import( *fitResult, "rfres_fitResults", kTRUE );


    // some monitoring and setup
    //fitter.setGlobalKillBelowError( RooFit::ERROR );
    fitter.setGlobalKillBelowError( RooFit::FATAL );
    fitter.setnCPU( 4 ); // sets the number of CPUs to run the minimisation in parallel

    TString massvar("B_M_PVKSJpsi"); // Setup the name of fit axix.
    Float_t xmin( 5180.0 ), xmax( 5500.0 ); // set the range of fit axis

    fitter.set1DFitVarName( massvar );
    //fitter.make1DFitVar( xmin, xmax, "MeV/c^{2}", "B^{0}_{(s)} --> J/#psi K^{0}_{S} #pi^{+} #pi^{-}" );
    fitter.setPlotBins( massvar, 80 );
    //================================================================================
    
    //RooAbsCollection* consts =  (RooAbsCollection*)fitter.getWS()->allVars().selectByAttrib("Constant", kTRUE);
    //consts->Print("V");
    //consts =  (RooAbsCollection*)fitter.getWS()->allVars().selectByAttrib("Constant", kFALSE);
    //consts->Print("V");

    //return 0;

    //fitter.getWS()->allCats().Print("V");


    RooArgSet listPdfs = fitter.getWS()->allPdfs();
    TIterator* cIter = listPdfs.createIterator();
    RooAbsPdf* pdf = 0;
    while( ( pdf = (RooAbsPdf*)cIter->Next() ) ) {
        
        if( TString(pdf->GetName()).Contains("constraint_") )  {

            //check if there is a set of constraints already in the workspace
            const RooArgSet* set = fitter.getWS()->set( fitter.getConstraintString() );
            if (!set){
                //if there isn't, make one.
                fitter.getWS()->defineSet( fitter.getConstraintString(), pdf->GetName() );
            }
            else{
                //if there is, add this constraint to the set
                fitter.getWS()->extendSet( fitter.getConstraintString(), pdf->GetName() );
            }
        }

    }
    delete cIter; cIter=0;
    fitter.getWS()->Print("V");

    
    //fitter.performFit();
    /*RooFitResult* result = fitter.getWS()->pdf( fitter.getModelName() )->fitTo( *fitter.getWS()->data( fitter.getDataSetName() ),
          RooFit::Extended( kTRUE ),
            RooFit::Timer( kTRUE ),
            RooFit::Save( kTRUE ),
            RooFit::NumCPU( fitter.getnCPU() ),
            RooFit::ExternalConstraints( *( fitter.getWS()->set(  fitter.getConstraintString() ) ) )
            //RooFit::ExternalConstraints( constraints )
            ); 
*/
  //  result->Print("V");

    //return 0;
    //================================================================================
    // Create PDFs and then add by csv the pdfs that should be used for the mass 
    // distribution imported above.
    //================================================================================

    //================================================================================
    // Set parameter constraints and and anything that is constant or has Gaussian 
    // constraints. Can also set blinding of variables here too.
    //================================================================================    
    // signal means are separated by their pdg difference
    //================================================================================
    // Finally perform the fit to the data and the plot some results.
    //================================================================================  

    //return 0;
    fitter.loadSnapshot( "initial_params" );
    //((RooRealVar*)fitter.getWS()->var("mBd"))->setConstant(kFALSE);
    //fitter.performFit( "new" );
    
    /*
    fitter.plotPrettyMassAndPull( "mass_JpsiKSpipi_LL", "M( J/#psi K_{S}^{0}(LL) #pi^{#pm}#pi^{#mp} )  [ MeV/c^{2} ]", "JpsiKSpipi_LL", "new" ) ;
    fitter.plotPrettyMassAndPull( "mass_JpsiKSpipi_DD", "M( J/#psi K_{S}^{0}(DD) #pi^{#pm}#pi^{#mp} )  [ MeV/c^{2} ]", "JpsiKSpipi_DD", "new" ) ;
    fitter.plotPrettyMassAndPull( "mass_JpsiKSKpi_LL", "M( J/#psi K_{S}^{0}(LL) K^{#pm}#pi^{#mp} )  [ MeV/c^{2} ]", "JpsiKSKpi_LL", "new" ) ;
    fitter.plotPrettyMassAndPull( "mass_JpsiKSKpi_DD", "M( J/#psi K_{S}^{0}(DD) K^{#pm}#pi^{#mp} )  [ MeV/c^{2} ]", "JpsiKSKpi_DD", "new" ) ;
    fitter.plotPrettyMassAndPull( "mass_JpsiKSKK_LL", "M( J/#psi K_{S}^{0}(LL) K^{#pm}K^{#mp} )  [ MeV/c^{2} ]", "JpsiKSKK_LL", "new" ) ;
    fitter.plotPrettyMassAndPull( "mass_JpsiKSKK_DD", "M( J/#psi K_{S}^{0}(DD) K^{#pm}K^{#mp} )  [ MeV/c^{2} ]", "JpsiKSKK_DD", "new" ) ;
    */
   
    //RooFitResult res1( *fitter.getFitResult( "newFit" ) );
    //res1.Print("V");
    double ntot = fitter.sumYields();
    std::cout << "Yields: " << ntot << std::endl;
    
    //RooKeysPdf* keys = dynamic_cast<RooKeysPdf*>(fitter.getWS()->pdf("BdJpsiKSKKAsJpsiKSKpi"));
    //keys->Print("V");
    
    /*
    std::vector<std::string> listOfYields;
    listOfYields.push_back("nBdJpsiKSpipi_LL");
    listOfYields.push_back("nBdJpsiKSpipi_DD");
    listOfYields.push_back("nBdJpsiKSKpi_LL");
    listOfYields.push_back("nBdJpsiKSKpi_DD");
    listOfYields.push_back("nBdJpsiKSKK_LL");
    listOfYields.push_back("nBdJpsiKSKK_DD");

    listOfYields.push_back("nBsJpsiKSpipi_LL");
    listOfYields.push_back("nBsJpsiKSpipi_DD");
    listOfYields.push_back("nBsJpsiKSKpi_LL");
    listOfYields.push_back("nBsJpsiKSKpi_DD");
    listOfYields.push_back("nBsJpsiKSKK_LL");
    listOfYields.push_back("nBsJpsiKSKK_DD");
 
    std::vector<std::string>::const_iterator iter = listOfYields.begin();
    const std::vector<std::string>::const_iterator enditer = listOfYields.end();
    TCanvas *c = new TCanvas("SimultaneousFitB","Simultaneous Fit B", 900.0, 900.0/1.641);
    TFile output("MyScans.root","RECREATE");
    for(; iter != enditer; ++iter ) {
        c->cd();    
        std::string range = *iter + "_NLL_range";
        fitter.loadSnapshot("fitResults");
        fitter.setParameterRange( iter->c_str(), range.c_str(), -10., 1100.);
        // Plot the scan of one parameter of the fit
        RooPlot* testplot = fitter.plotLikelihoodScan( iter->c_str(), "fitResults", (range).c_str()  );
        testplot->Draw();
        testplot->SetName( iter->c_str() );
        testplot->Write();

        std::string saveName =  "nLLScan_" + *iter + "_Yield";
        c->Print( (saveName+".pdf").c_str() );
        c->Print( (saveName+".C").c_str() );
        c->Clear();
    } 
    output.Close();
    delete c;c=0;

    return 0;
    */

    fitter.loadSnapshot("fitResults");
    fitter.addGaussianConstraint( "kalpha1JpsiKSKpi", 1.03, 0.2 );
    fitter.addGaussianConstraint( "kn1JpsiKSKpi", 1.03, 0.48 );
    //fitter.addGaussianConstraint( "kalpha1JpsiKSKK", 1.23, 0.22 );
    //fitter.addGaussianConstraint( "kn1JpsiKSKK", 1.98, 0.41 );
    fitter.addGaussianConstraint( "alpha1", 1.03, 0.2 );
    fitter.addGaussianConstraint( "kalpha2", -1.57, 0.43 );
    fitter.addGaussianConstraint( "n1", 3.0, 1.0 );
    fitter.addGaussianConstraint( "kn2", 1.86, 0.77 );
    fitter.addGaussianConstraint( "frac2", 0.714, 0.089 );


    fitter.performFit( "gaussianConstrained_MCFitted_params" );
    RooFitResult res2(*fitter.getFitResult( "gaussianConstrained_MCFitted_params" ));
    res2.Print("V");

    return 0;



    //    fitter.setGaussianConstraint("", )

    /*    Fast::ToyStudy toy( &fitter, 5 );
          toy.runToyStudy( "fitResults", "Bd_signal_JpsiKSpipiLL_Yield=0,Bd_signal_JpsiKSpipiDD_Yield=0,Bs_signal_JpsiKSKpiLL_Yield=0,Bs_signal_JpsiKSKpiDD_Yield=0" );
          toy.plotNlls("toystudy_nlls");
          toy.plotPulls("toystudy_pulls");
     */
    /* 1  Bd_signal_JpsiKSKKDD_Yield   
       2  Bd_signal_JpsiKSKKLL_Yield   
       3  Bd_signal_JpsiKSKpiDD_Yield  
       4  Bd_signal_JpsiKSKpiLL_Yield  
       5  Bd_signal_JpsiKSpipiDD_Yield 
       6  Bd_signal_JpsiKSpipiLL_Yield
     */
    return 0;
}

void plotPrettyMassAndPull( TCanvas* cBmass, std::string save, Fast::SimultaneousFitter* fitter, std::string name
        , std::string slicename, std::string fitName ) {

    RooPlot* hresidual_match = fitter->plotFitPulls( name.c_str(), slicename.c_str(), fitName.c_str());
    RooPlot* mBframe = fitter->plotFitResults( name.c_str(), slicename.c_str(), fitName.c_str() );

    if( !mBframe ) {
        throw GeneralException("plotPrettyMassAndPull()",
                "Cannot plot the mass disribution.");        
    }
    if( !hresidual_match ) {
        throw GeneralException("plotPrettyMassAndPull()",
                "Cannot plot the pull disribution.");
    }

    double r = 0.2;
    double sr = 1. / r;

    TAxis* xAxis = hresidual_match->GetXaxis();
    xAxis->SetTickLength ( sr * xAxis->GetTickLength()  );
    xAxis->SetLabelSize  ( sr * xAxis->GetLabelSize()   );
    xAxis->SetTitleSize  ( 0                            );
    xAxis->SetLabelOffset( sr * xAxis->GetLabelOffset() );

    TAxis* yAxis = hresidual_match->GetYaxis();
    yAxis->SetNdivisions ( 504                          );
    yAxis->SetLabelSize  ( sr * yAxis->GetLabelSize()   );

    cBmass->Divide( 1, 2, .1, .1 );
    mBframe->GetXaxis()->SetLabelSize(0);
    mBframe->GetXaxis()->SetTitleOffset(0.8);

    r  = .25;
    double sl = 1. / ( 1. - r );
    mBframe->GetYaxis()->SetLabelSize(sl * mBframe->GetYaxis()->GetLabelSize());
    //  double labelSize = 0.2;
    hresidual_match->GetXaxis()->SetLabelSize((1./(1.+r)) * hresidual_match->GetXaxis()->GetLabelSize());
    hresidual_match->GetYaxis()->SetLabelSize((1./(1.+r)) * hresidual_match->GetYaxis()->GetLabelSize());
    TPad* padHisto_match = (TPad*) cBmass->cd(1);
    TPad* padResid_match = (TPad*) cBmass->cd(2);
    double smmatch = 0.1;
    padHisto_match->SetPad( 0., r , 1., 1. );
    padHisto_match->SetBottomMargin( smmatch );
    padResid_match->SetPad( 0., 0., 1., r  );
    padResid_match->SetBottomMargin( 0.3  );
    padResid_match->SetTopMargin   ( smmatch );
    padHisto_match->cd();
    mBframe->Draw();
    //if (mFitOptions->isConstant("plotLegend")) plotLegend(mBframe,catname);
    padResid_match->cd();
    hresidual_match->Draw();
    cBmass->Print( save.c_str() );
    cBmass->Clear();

    delete hresidual_match; hresidual_match = 0;
    delete mBframe; mBframe = 0;
}
