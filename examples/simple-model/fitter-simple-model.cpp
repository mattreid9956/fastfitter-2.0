#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

//ROOT
#include "TChain.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"
#include "TROOT.h"
#include "TH2D.h"
#include "TFile.h"

// RooFit
#include "RooPlot.h"

//Custom
#include "SimultaneousFitter.hpp"
#include "ToyStudy.hpp"
#include "DataLocationConfig.h"


int main() {

    bool Bd_blind(false), Bs_blind(true);
    //================================================================================
    // Create instance of the fitter and set the mass variable
    //================================================================================
    Fast::SimultaneousFitter fitter( "B2JpsiKShhFit", "B0 --> Jpsi KS h' h Fit" );
    fitter.createWS( "Desktop", "Desktop" );

    // some monitoring and setup
    fitter.setGlobalKillBelowError( RooFit::FATAL );
    fitter.setnCPU( 4 ); // sets the number of CPUs to run the minimisation in parallel

    TString massvar("B_Mass_Constr"); // Setup the name of fit axix.
    Float_t xmin( 5180.0 ), xmax( 5500.0 ); // set the range of fit axis

    fitter.set1DFitVarName( massvar );
    fitter.make1DFitVar( xmin, xmax, "MeV/c^{2}", "B^{0}_{(s)} --> J/#psi K^{0}_{S} #pi^{+} #pi^{-}" );
    fitter.setPlotBins( massvar, 80 );
    //================================================================================


    //================================================================================
    // Make all the pdfs I need  
    //================================================================================
    // Bd signal LL model
    fitter.makeSingleGauss("Bd_signalLL", 5280, 5250, 5300,//mean
            8, 0, 20//sigma_core
            );
    // Bd signal DD model
    fitter.makeSingleGauss("Bd_signalDD", 5280, 5250, 5300,//mean
            10, 0, 20//sigma_core
            );

    // Bs signal LL model
    fitter.makeSingleGauss("Bs_signalLL", 5366, 5350, 5375,//mean
            10, 0, 20//sigma_core
            ); 
    // Bs signal DD model
    fitter.makeSingleGauss("Bs_signalDD", 5366, 5350, 5375,//mean
            13, 0, 20//sigma_core
            );  

    fitter.makeChebychev( "combinatoric_jpsiKSpipiLL", -0.0045, -1.0, 1.0 );
    fitter.makeChebychev( "combinatoric_jpsiKSpipiDD", -0.0045, -1.0, 1.0 );

    //================================================================================
    // Import the data that you need 
    //================================================================================
    TString inputFileNameLL = (FITTER_EXAMPLE_DATA+"/ExampleDataLL.root");
    TFile *fileLL = TFile::Open( inputFileNameLL, "READ" );
    if (!fileLL || fileLL->IsZombie()) {
        std::cout << "ERROR: Failed to open candidate file " << inputFileNameLL 
            << " for reading" << std::endl;
        return EXIT_FAILURE;
    }

    TString llpipiTupleName = "treeLL";
    TTree* llpipiTree = dynamic_cast<TTree*>( fileLL->Get( llpipiTupleName ) );
    if (!llpipiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << llpipiTupleName
            << " from file" << inputFileNameLL << std::endl;
        return EXIT_FAILURE;
    }
    fitter.makeMassDataSet( llpipiTree, massvar, "JpsiKSpipiLL", "JpsiKSpipiLL" ) ; //, place cuts here("D0K_PIDK>0 && D0Pi_PIDK<4 && KstarK_PIDK>"+v1PIDCut+" && KstarPi_PIDK<"+v2PIDCut).c_str());
    fileLL->Close();
    
    TString inputFileNameDD = (FITTER_EXAMPLE_DATA+"/ExampleDataDD.root");
    TFile *fileDD = TFile::Open( inputFileNameDD, "READ" );
    if (!fileDD || fileDD->IsZombie()) {
        std::cout << "ERROR: Failed to open candidate file " << inputFileNameDD
            << " for reading" << std::endl;
        return EXIT_FAILURE;
    }

    TString ddpipiTupleName = "treeDD";
    TTree* ddpipiTree = dynamic_cast<TTree*>( fileDD->Get( ddpipiTupleName ) );
    if (!ddpipiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << ddpipiTupleName
            << " from file" << inputFileNameDD << std::endl;
        return EXIT_FAILURE;
    }

    // Follows MakeMassDataSet( tree, massName, name, title, cuts );
    fitter.makeMassDataSet( ddpipiTree, massvar, "JpsiKSpipiDD", "JpsiKSpipiDD" ); 
    fileDD->Close();  // close the file no longer needing it!
 
    fitter.combineDataSets();
    //================================================================================

    //================================================================================
    // Create PDFs and then add by csv the pdfs that should be used for the mass 
    // distribution imported above.
    //================================================================================
    fitter.createPDFSets();
    fitter.addPDFs( "JpsiKSpipiLL", "Bd_signalLL, Bs_signalLL, combinatoric_jpsiKSpipiLL" );
    fitter.addPDFs( "JpsiKSpipiDD", "Bd_signalDD, Bs_signalDD, combinatoric_jpsiKSpipiDD" );

    fitter.addYields( "JpsiKSpipiLL" ) ;
    fitter.addYields( "JpsiKSpipiDD" ) ;

    fitter.buildAddPdfs();
    fitter.buildModel();
    //================================================================================


    //================================================================================
    // Set parameter constraints and and anything that is constant or has Gaussian 
    // constraints. Can also set blinding of variables here too.
    //================================================================================    
    // signal means are separated by their pdg difference
    fitter.addConstraint("Bs_signalLL_mu","@0 + 87.35","Bd_signalLL_mu");
    fitter.addConstraint("Bs_signalDD_mu","@0 + 87.35","Bd_signalDD_mu");
    // constrain the common means between DD and LL modes
    fitter.addConstraint("Bd_signalDD_mu","@0","Bd_signalLL_mu");

    // add a new parameter to relate the LL and DD modes
    fitter.addParameter( "LLoverDD", 0.0, 4.0 );
    fitter.addConstraint("Bd_signalLL_sigma0","@0*@1","Bd_signalDD_sigma0,LLoverDD");
    fitter.addConstraint("Bs_signalLL_sigma0","@0*@1","Bs_signalDD_sigma0,LLoverDD");
//    fitter.addConstraint("Bs_signalDD_sigma0", "@0*1.1", "Bs_signalDD_sigma0" );
 
    // width ratio of double gaussian signal is constant at 2.1 and equal for
    // Bs and Bd
    fitter.addGaussianConstraint("Bs_signalDD_sigma0", 13, 3);

    // core fraction of double gaussian signal is constant at 0.87 and equal for
    // Bs and Bd
    //fitter.setParameterConstant("Bd_signal_coreFrac", 0.87 );

    //blind the Bs --> JpsiKSpipi(LL/DD) yield
    if(Bd_blind){
        fitter.blindParameter( "Bd_signalLL_JpsiKSpipiLL_Yield", 30 );
        fitter.blindParameter( "Bd_signalDD_JpsiKSpipiDD_Yield", 30 );
    }
    if(Bs_blind){
        fitter.blindParameter( "Bs_signalLL_JpsiKSpipiLL_Yield", 30 );
        fitter.blindParameter( "Bs_signalDD_JpsiKSpipiDD_Yield", 30 );
    }
    //================================================================================    

    // example setting all Yields with signal in their name to 0.0 and constant.
    //fitter.setRegexParameterValues( "*signal+*Yield", 0.0, true );
   
    //================================================================================
    // Finally perform the fit to the data and the plot some results.
    //================================================================================  
    fitter.performFit( "fitResults" );
    RooFitResult* res1 = fitter.getFitResult( "fitResults" );
    double ntot = fitter.sumYields();
    std::cout << "Yields: " << ntot << std::endl;
    //================================================================================  

    //return 0;
    //================================================================================
    // Finally perform the fit to the data and the plot 
    //================================================================================  
/*    // Store the correlation plot
    TCanvas *c1 = new TCanvas("c1","Simultaneous Fit B", 900, 900/1.641);
    c1->SetLeftMargin(0.33);
    c1->SetBottomMargin(0.33);
    c1->cd();
    gPad->SetRightMargin(.14);
    TH2* hcorr = fitter.plotCorrelationHist(); 
    hcorr->Draw("colz");
    gStyle->SetPaintTextFormat("g");
    c1->Print( "correlationHist.eps");
    c1->Print( "correlationHist.pdf");
    delete c1;c1=0;
*/    //================================================================================  


    //================================================================================
    // Get the significance of a signal mode by setting any of the related parameters
    // specifiec to zero and getting the liklihood ratio. sqrt( -2 [ nll( B ) - nll( S + B ) ] )
    //================================================================================  
    double significance_Bd2JpsiKSpipi = fitter.likelihoodRatio( "Bd_signalLL_JpsiKSpipiLL_Yield", "fitResults"  );
    //double significance_Bs2JpsiKSpipi = fitter.likelihoodRatio( "Bs_signalDD_JpsiKSpipiLL_Yield,Bs_signalDD_JpsiKSpipiDD_Yield", "fitResults"  );
    //================================================================================  


    //================================================================================
    // Do some nice plotting of the invariant mass spectra, this will do residual and
    //================================================================================  
    fitter.setPlotBins( massvar, 80 ); // change binning
    fitter.plotPrettyMassAndPull( "pipillplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(LL) #pi^{#pm}#pi^{#mp}  [ MeV/c^{2} ]", "JpsiKSpipiLL", "fitResults" ) ;
    // can even get the probability of this fit or the chi2/ndof, up to you...
    std::cout << "Probability of fit to JpsiKSpipiLL spectrum: " << fitter.probFitMeasure( massvar, "JpsiKSpipiLL", "fitResults" ) << std::endl;
    fitter.setPlotBins( massvar, 120 );
    fitter.plotPrettyMassAndPull( "pipiddplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(DD) #pi^{#pm}#pi^{#mp}  [ MeV/c^{2} ]", "JpsiKSpipiDD", "fitResults" ) ;
    std::cout << "Probability of fit to JpsiKSpipiDD spectrum: " << fitter.probFitMeasure( massvar, "JpsiKSpipiDD", "fitResults" ) << std::endl;
    //================================================================================    


    //================================================================================
    // Perform a set of nll scans and save them
    //================================================================================  
    /*
    TCanvas *c = new TCanvas("SimultaneousFitB","Simultaneous Fit B", 900, 900/1.641);
    c->cd();    
    // Plot the scan of one parameter of the fit
    RooPlot* testplot = fitter.plotLikelihoodScan( "Bs_signalLL_JpsiKSpipiLL_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bs_signal_JpsiKSpipiLL_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;

    testplot = fitter.plotLikelihoodScan( "Bs_signalDD_JpsiKSpipiDD_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bs_signal_JpsiKSpipiDD_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;

    testplot = fitter.plotLikelihoodScan( "Bd_signalLL_JpsiKSpipiLL_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bd_signal_JpsiKSpipiLL_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;

    testplot = fitter.plotLikelihoodScan( "Bd_signalDD_JpsiKSpipiDD_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bd_signal_JpsiKSpipiDD_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;
    delete c; c=0;
    */
    //================================================================================    

    
    //================================================================================
    // Carry out some toy studies on the dataset, is it stable?
    //================================================================================  
    Fast::ToyStudy toy( &fitter, 100 );
    

    bool status = toy.localMinimumChecker( "fitResults", 
            100, 
            Fast::Fitter::CORRELATION, 
            1.e-3,
            true,
            false,
            false,
            false );
    //                    Double_t edmTol = 1.e-3, Bool_t extendedMode = kTRUE, Bool_t useMinos = kTRUE, Bool_t useSumW2Errors = kFALSE, 
    std::cout << status << std::endl;


    toy.runToyStudy( "fitResults" );
    toy.plotNlls("toystudy_nlls");
    toy.plotValues("toystudy_values");
    toy.plotPulls("toystudy_pulls");
    //================================================================================
    // Save everything in the workspace!
    //================================================================================  
    fitter.saveWS( "MyDataTestFit.root" );

    return 0;
}
