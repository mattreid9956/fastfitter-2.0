//C++
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

//ROOT
#include "TChain.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"
#include "TROOT.h"
#include "TH2D.h"
#include "TFile.h"

//RooFit
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooRandom.h"

//Custom
#include "SimultaneousFitter.hpp"
#include "ToyStudy.hpp"
#include "Exceptions.hpp"

using Fast::GeneralException;
using Fast::WSRetrievalFailure;

void usage()
{
    std::cout << "./model.exe <v1PIDCut> <v2PIDCut>" << std::endl;
    std::cout << "<v1PIDCut> = cut threshold on KstarK/PhiKPlus" << std::endl;
    std::cout << "<v2PIDCut> = cut threshold on KstarPi/PhiKMinus" << std::endl;
    exit(EXIT_FAILURE);
}

void plotPrettyMassAndPull( TCanvas* cBmass, std::string save, Fast::SimultaneousFitter * fitter, std::string name
        , std::string slicename, std::string fitName ); 


int main(int argc, char* argv[])
{
    //int nargs = argc-1;
    //if (nargs!=2) usage();
    //std::string v1PIDCut = argv[1], v2PIDCut = argv[2];

    //================================================================================
    // Create instance of the fitter and set the mass variable
    //================================================================================
    Fast::SimultaneousFitter fitter( "B2JpsiKShhFit", "B0 --> Jpsi KS h' h Fit" );
    fitter.createWS( "Desktop", "Desktop" );

    // some monitoring and setup
    fitter.setGlobalKillBelowError( RooFit::FATAL );
    fitter.setnCPU( 4 ); // sets the number of CPUs to run the minimisation in parallel

    TString massvar("B_M_PVKSJpsi"); // Setup the name of fit axix.
    Float_t xmin( 5180.0 ), xmax( 5500.0 ); // set the range of fit axis

    fitter.set1DFitVarName( massvar );
    fitter.make1DFitVar( xmin, xmax, "MeV/c^{2}", "B^{0}_{(s)} --> J/#psi K^{0}_{S} #pi^{+} #pi^{-}" );
    fitter.setPlotBins( massvar, 80 );
    //================================================================================


    //================================================================================
    // Make all the pdfs I need  
    //================================================================================
    // Bd signal model
    fitter.makeDoubleGauss("Bd_signal", 5280, 5250, 5300,//mean
            10, 0, 20,//sigma_core
            2, 1, 3, //width_ratio
            0.5//core fraction
            );

    fitter.makeDoubleGauss("Bs_signal", 5366, 5350, 5375,//mean
            10, 0, 20,//sigma_core
            2, 1, 3, //width_ratio
            0.5//core fraction
            );    

    /* We can also import pdfs already fitted and saved to a file.
       fitter.importPDFFromFile("~/BdDKstar_analysis_2012/FittingMacro/LowMass/LowMassPdf_Bd_REFITTED_M.root",
       "B2DstKst_BKG",
       "mass_pdf_Bd_ALL",
       "Bd_2_DstKst_bkg");
     */

    fitter.makeChebychev( "combinatoric_jpsiKSpipiDD", -0.0045, -1.0, 1.0 );
    fitter.makeChebychev( "combinatoric_jpsiKSpipiLL", -0.0045, -1.0, 1.0 );
    fitter.makeChebychev( "combinatoric_jpsiKSKpiDD", -0.0045, -1.0, 1.0 );
    fitter.makeChebychev( "combinatoric_jpsiKSKpiLL", -0.0045, -1.0, 1.0 );
    //fitter.makeChebychev( "combinatoric_jpsiKSKKDD", -0.0045, -1.0, 1.0 );
    //fitter.makeChebychev( "combinatoric_jpsiKSKKLL", -0.0045, -1.0, 1.0 );
    //================================================================================


    //================================================================================
    // Import the data that you need 
    //================================================================================
    TString inputFileName = "/home/matt/B2JpsiKShh-Collision11-Merged-Stripping17b-doca_cropDataOneCand.root";
    TFile *file = TFile::Open( inputFileName, "READ" );
    if (!file || file->IsZombie()) {
        std::cout << "ERROR: Failed to open candidate file " << inputFileName 
            << " for reading" << std::endl;
        return EXIT_FAILURE;
    }

    TString llpipiTupleName = "tupleB2JpsiKSpipiLL";
    TTree* llpipiTree = dynamic_cast<TTree*>( file->Get( llpipiTupleName ) );
    if (!llpipiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << llpipiTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }

    TString ddpipiTupleName = "tupleB2JpsiKSpipiDD";
    TTree* ddpipiTree = dynamic_cast<TTree*>( file->Get( ddpipiTupleName ) );
    if (!ddpipiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << ddpipiTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }
    
    TString llKpiTupleName = "tupleB2JpsiKSKpiLL";
    TTree* llKpiTree = dynamic_cast<TTree*>( file->Get( llKpiTupleName ) );
    if (!llKpiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << llKpiTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }

    TString ddKpiTupleName = "tupleB2JpsiKSKpiDD";
    TTree* ddKpiTree = dynamic_cast<TTree*>( file->Get( ddKpiTupleName ) );
    if (!ddKpiTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << ddKpiTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }
    
    /*
    TString llKKTupleName = "tupleB2JpsiKSKKLL";
    TTree* llKKTree = dynamic_cast<TTree*>( file->Get( llKKTupleName ) );
    if (!llKKTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << llKKTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }

    TString ddKKTupleName = "tupleB2JpsiKSKKDD";
    TTree* ddKKTree = dynamic_cast<TTree*>( file->Get( ddKKTupleName ) );
    if (!ddKKTree) {
        std::cout << "ERROR: Failed to retrieve candidate nTuple " << ddKKTupleName
            << " from file" << inputFileName << std::endl;
        return EXIT_FAILURE;
    }
    */

    // Follows MakeMassDataSet( tree, massName, name, title, cuts );
    fitter.makeMassDataSet( llpipiTree, massvar, "JpsiKSpipiLL", "JpsiKSpipiLL" ) ; //, place cuts here("D0K_PIDK>0 && D0Pi_PIDK<4 && KstarK_PIDK>"+v1PIDCut+" && KstarPi_PIDK<"+v2PIDCut).c_str());
    fitter.makeMassDataSet( ddpipiTree, massvar, "JpsiKSpipiDD", "JpsiKSpipiDD" ); //,("D0K_PIDK>0 && D0Pi_PIDK<4 && PhiKPlus_PIDK>"+v1PIDCut+" && PhiKMinus_PIDK>"+v2PIDCut).c_str());
    // Follows MakeMassDataSet( tree, massName, name, title, cuts );
    fitter.makeMassDataSet( llKpiTree, massvar, "JpsiKSKpiLL", "JpsiKSKpiLL" ) ; //, place cuts here("D0K_PIDK>0 && D0Pi_PIDK<4 && KstarK_PIDK>"+v1PIDCut+" && KstarPi_PIDK<"+v2PIDCut).c_str());
    fitter.makeMassDataSet( ddKpiTree, massvar, "JpsiKSKpiDD", "JpsiKSKpiDD" ); //,("D0K_PIDK>0 && D0Pi_PIDK<4 && PhiKPlus_PIDK>"+v1PIDCut+" && PhiKMinus_PIDK>"+v2PIDCut).c_str());
    // Follows MakeMassDataSet( tree, massName, name, title, cuts );
    //fitter.makeMassDataSet( llKKTree, massvar, "JpsiKSKKLL", "JpsiKSKKLL" ) ; //, place cuts here("D0K_PIDK>0 && D0Pi_PIDK<4 && KstarK_PIDK>"+v1PIDCut+" && KstarPi_PIDK<"+v2PIDCut).c_str());
    //fitter.makeMassDataSet( ddKKTree, massvar, "JpsiKSKKDD", "JpsiKSKKDD" ); //,("D0K_PIDK>0 && D0Pi_PIDK<4 && PhiKPlus_PIDK>"+v1PIDCut+" && PhiKMinus_PIDK>"+v2PIDCut).c_str());
    
    fitter.combineDataSets();
    file->Close();  // close the file no longer needing it!
    //================================================================================


    //================================================================================
    // Create PDFs and then add by csv the pdfs that should be used for the mass 
    // distribution imported above.
    //================================================================================
    fitter.createPDFSets();
    fitter.addPDFs( "JpsiKSpipiLL", "Bd_signal,combinatoric_jpsiKSpipiLL" );
    fitter.addPDFs( "JpsiKSpipiDD", "Bd_signal,combinatoric_jpsiKSpipiDD" );
    fitter.addPDFs( "JpsiKSKpiLL", "Bs_signal,combinatoric_jpsiKSKpiLL" );
    fitter.addPDFs( "JpsiKSKpiDD", "Bs_signal,combinatoric_jpsiKSKpiDD" );
    //fitter.addPDFs( "JpsiKSKKLL", "Bd_signal,Bs_signal,combinatoric_jpsiKSKKLL" );
    //fitter.addPDFs( "JpsiKSKKDD", "Bd_signal,Bs_signal,combinatoric_jpsiKSKKDD" );

    fitter.addYields("JpsiKSpipiLL");
    fitter.addYields("JpsiKSpipiDD");
    fitter.addYields("JpsiKSKpiLL");
    fitter.addYields("JpsiKSKpiDD");
    //fitter.addYields("JpsiKSKKLL");
    //fitter.addYields("JpsiKSKKDD");

    fitter.buildAddPdfs();
    fitter.buildModel();
    //================================================================================


    //================================================================================
    // Set parameter constraints and and anything that is constant or has Gaussian 
    // constraints. Can also set blinding of variables here too.
    //================================================================================    
    // signal means are separated by their pdg difference
    fitter.addConstraint("Bs_signal_mu","@0 + 87.35","Bd_signal_mu");
    // Bd and Bs widths are equal
    fitter.addConstraint("Bs_signal_sigma0","@0","Bd_signal_sigma0");

    // width ratio of double gaussian signal is constant at 2.1 and equal for
    // Bs and Bd
    fitter.addConstraint("Bs_signal_s1oS0","@0","Bd_signal_s1oS0");
    //fitter.addGaussianConstraint("Bs_signal_sigma1", 2.1, 0.4);
    fitter.setParameterConstant("Bd_signal_s1oS0",2.1);

    // core fraction of double gaussian signal is constant at 0.87 and equal for
    // Bs and Bd
    fitter.addConstraint("Bs_signal_coreFrac","@0","Bd_signal_coreFrac");
    fitter.setParameterConstant("Bd_signal_coreFrac",0.87);

    //blind the Bs --> JpsiKSpipi(LL/DD) yield
    //    fitter.blindParameter("Bs_signal_JpsiKSpipiLL_Yield", 30, 30);
    //    fitter.blindParameter("Bs_signal_JpsiKSpipiDD_Yield", 30, 30);
    //================================================================================    


    //================================================================================
    // Finally perform the fit to the data and the plot some results.
    //================================================================================  
    fitter.performFit();
    RooFitResult res1(*fitter.getFitResult( "fitResults" ));
    double ntot = fitter.sumYields();
    std::cout << "Yields: " << ntot << std::endl;

    Fast::ToyStudy toy( &fitter, 5 );
    
    bool status = toy.localMinimumChecker( "fitResults", 
            100, 
            Fast::Fitter::CORRELATION, 
            1.e-3,
            true,
            false,
            false,
            false );
    //                    Double_t edmTol = 1.e-3, Bool_t extendedMode = kTRUE, Bool_t useMinos = kTRUE, Bool_t useSumW2Errors = kFALSE, 
    std::cout << status << std::endl;
    //toy.runToyStudy( "fitResults", "Bd_signal_JpsiKSpipiLL_Yield=0,Bd_signal_JpsiKSpipiDD_Yield=0,Bs_signal_JpsiKSKpiLL_Yield=0,Bs_signal_JpsiKSKpiDD_Yield=0" );
    //toy.plotNlls("toystudy_nlls");
    //toy.plotPulls("toystudy_pulls");

/* 1  Bd_signal_JpsiKSKKDD_Yield   
   2  Bd_signal_JpsiKSKKLL_Yield   
   3  Bd_signal_JpsiKSKpiDD_Yield  
   4  Bd_signal_JpsiKSKpiLL_Yield  
   5  Bd_signal_JpsiKSpipiDD_Yield 
   6  Bd_signal_JpsiKSpipiLL_Yield
*/
    return 0;
    
    //fitter.importDataSet( data );
    //fitter.performFit();
    //RooFitResult res3(*fitter.getFitResult( "fitResults" ) );
    //ntot = fitter.sumYields();
    //std::cout << "Yields: " << ntot << std::endl;

    res1.Print("V");
    //res3.Print("V");


    return 0;
    //check how well the fits did, recall this will be dependent on binning
    fitter.probFitMeasure( massvar, "JpsiKSpipiLL", "fitResults" );
    fitter.plotPrettyMassAndPull( "pipillplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(LL) #pi^{#pm}#pi^{mp}  [ MeV/c^{2} ]", "JpsiKSpipiLL", "fitResults" ) ;

    fitter.setPlotBins( massvar, 90 );
    fitter.probFitMeasure( massvar, "JpsiKSpipiDD", "fitResults" );
    fitter.plotPrettyMassAndPull( "pipiddplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(DD) #pi^{#pm}#pi^{mp}  [ MeV/c^{2} ]", "JpsiKSpipiDD", "fitResults" ) ;

    fitter.setPlotBins( massvar, 70 );
    // plot the mass distibution with pdfs and pulls in a nice format
    fitter.plotPrettyMassAndPull( "Kpillplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(LL) K^{#pm}#pi^{mp}  [ MeV/c^{2} ]", "JpsiKSKpiLL", "fitResults" ) ;
    fitter.plotPrettyMassAndPull( "Kpiddplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(DD) K^{#pm}#pi^{mp}  [ MeV/c^{2} ]", "JpsiKSKpiDD", "fitResults" ) ;
    //fitter.plotPrettyMassAndPull( "KKllplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(LL) K^{#pm}K^{mp}  [ MeV/c^{2} ]", "JpsiKSKKLL", "fitResults" ) ;
    //fitter.plotPrettyMassAndPull( "KKddplot", "B_{(s)}^{0} #rightarrow J/#psi K_{S}^{0}(DD) K^{#pm}K^{mp}  [ MeV/c^{2} ]", "JpsiKSKKDD", "fitResults" ) ;

    fitter.probFitMeasure( massvar, "JpsiKSKpiLL", "fitResults" );
    fitter.probFitMeasure( massvar, "JpsiKSKpiDD", "fitResults" );
    //fitter.probFitMeasure( massvar, "JpsiKSKKLL", "fitResults" );
    //fitter.probFitMeasure( massvar, "JpsiKSKKDD", "fitResults" );

    fitter.getWS()->allVars().Print("V");

    fitter.wiggleAllParams( 0 );
    std::cout << "Loading in the new parameters.\n";
    std::cout << ".\n";
    std::cout << ".\n";

    //fitter.getWS()->allVars().Print("V");

    std::cout << "Yields: " << fitter.sumYields() << std::endl;


    //fitter.floatAllParams();

    return 0;
    // Can even grab a given fit result if we want...
    //RooFitResult* res = fitter.getFitResult( "fitResults" );
    //res->Print();

    TCanvas *c = new TCanvas("Simultaneous Fit B","Simultaneous Fit B", 900, 900/1.641);
    c->cd();    


    // Plot the scan of one parameter of the fit
    RooPlot* testplot = fitter.plotLikelihoodScan( "Bs_signal_JpsiKSKpiLL_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bs_signal_JpsiKSKpiLL_Yield.eps" );
    c->Clear();

    fitter.loadSnapshot("fitResults");

    delete testplot; testplot = 0;
    
    /*testplot = fitter.plotLikelihoodScan( "Bs_signal_JpsiKSKpiDD_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bs_signal_JpsiKSKpiDD_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;

    testplot = fitter.plotLikelihoodScan( "Bd_signal_JpsiKSKKLL_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bd_signal_JpsiKSKKLL_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;
    
    testplot = fitter.plotLikelihoodScan( "Bd_signal_JpsiKSpipiLL_Yield" );
    testplot->Draw();
    c->Print( "nLLScan_Bd_signal_JpsiKSpipiLL_Yield.eps" );
    c->Clear();
    delete testplot; testplot = 0;*/

    // Get yield information
    double bd_yield_ll = fitter.getYield( "Bd_signal", "Bd_signal_JpsiKSpipiLL_Yield", 5200.0, 5320.0 );
    double bd_yield_dd = fitter.getYield( "Bd_signal", "Bd_signal_JpsiKSpipiDD_Yield", 5200.0, 5320.0 );
    std::cout << "N(B0 --> Jpsi KS(LL) pi pi) = " << bd_yield_ll << std::endl;
    std::cout << "N(B0 --> Jpsi KS(DD) pi pi) = " << bd_yield_dd << std::endl;
    fitter.likelihoodRatio( "Bd_signal_JpsiKSpipiDD_Yield,Bd_signal_JpsiKSpipiLL_Yield", "fitResults"  );
    fitter.likelihoodRatio( "Bs_signal_JpsiKSKpiDD_Yield,Bs_signal_JpsiKSKpiLL_Yield", "fitResults"  );
    fitter.likelihoodRatio( "Bd_signal_JpsiKSKKDD_Yield,Bd_signal_JpsiKSKKLL_Yield", "fitResults"  );

    
    // Store the correlation plot
    c->SetLeftMargin(0.33);
    c->SetBottomMargin(0.33);
    c->cd();
    gPad->SetRightMargin(.14);
    TH2* hcorr = fitter.plotCorrelationHist(); 
    hcorr->Draw("colz");
    gStyle->SetPaintTextFormat("g");
    c->Print( "correlationHist.eps");
    
    fitter.saveWS( "MyDataTestFit.root" );

    //fitter.doRooMCStudy( 100, "ToyStudy.root", 0, 1.0, kFALSE );
    //doMCStudy( 100, "ToyStudy.root", "Bs_signal_JpsiKSpipiDD_Yield=700,Bs_signal_JpsiKSpipiLL_Yield=100");
    //================================================================================    


    // clean up
    //delete fitter; fitter = 0;
    delete c; c=0;

    return 0;
}

void plotPrettyMassAndPull( TCanvas* cBmass, std::string save, Fast::SimultaneousFitter* fitter, std::string name
        , std::string slicename, std::string fitName ) {

    RooPlot* hresidual_match = fitter->plotFitPulls( name.c_str(), slicename.c_str(), fitName.c_str());
    RooPlot* mBframe = fitter->plotFitResults( name.c_str(), slicename.c_str(), fitName.c_str() );

    if( !mBframe ) {
        throw GeneralException("plotPrettyMassAndPull()",
                "Cannot plot the mass disribution.");        
    }
    if( !hresidual_match ) {
        throw GeneralException("plotPrettyMassAndPull()",
                "Cannot plot the pull disribution.");
    }
    
    double r = 0.2;
    double sr = 1. / r;
    
    TAxis* xAxis = hresidual_match->GetXaxis();
    xAxis->SetTickLength ( sr * xAxis->GetTickLength()  );
    xAxis->SetLabelSize  ( sr * xAxis->GetLabelSize()   );
    xAxis->SetTitleSize  ( 0                            );
    xAxis->SetLabelOffset( sr * xAxis->GetLabelOffset() );

    TAxis* yAxis = hresidual_match->GetYaxis();
    yAxis->SetNdivisions ( 504                          );
    yAxis->SetLabelSize  ( sr * yAxis->GetLabelSize()   );

    cBmass->Divide( 1, 2, .1, .1 );
    mBframe->GetXaxis()->SetLabelSize(0);
    mBframe->GetXaxis()->SetTitleOffset(0.8);
    
    r  = .25;
    double sl = 1. / ( 1. - r );
    mBframe->GetYaxis()->SetLabelSize(sl * mBframe->GetYaxis()->GetLabelSize());
    //  double labelSize = 0.2;
    hresidual_match->GetXaxis()->SetLabelSize((1./(1.+r)) * hresidual_match->GetXaxis()->GetLabelSize());
    hresidual_match->GetYaxis()->SetLabelSize((1./(1.+r)) * hresidual_match->GetYaxis()->GetLabelSize());
    TPad* padHisto_match = (TPad*) cBmass->cd(1);
    TPad* padResid_match = (TPad*) cBmass->cd(2);
    double smmatch = 0.1;
    padHisto_match->SetPad( 0., r , 1., 1. );
    padHisto_match->SetBottomMargin( smmatch );
    padResid_match->SetPad( 0., 0., 1., r  );
    padResid_match->SetBottomMargin( 0.3  );
    padResid_match->SetTopMargin   ( smmatch );
    padHisto_match->cd();
    mBframe->Draw();
    //if (mFitOptions->isConstant("plotLegend")) plotLegend(mBframe,catname);
    padResid_match->cd();
    hresidual_match->Draw();
    cBmass->Print( save.c_str() );
    cBmass->Clear();

    delete hresidual_match; hresidual_match = 0;
    delete mBframe; mBframe = 0;
}
